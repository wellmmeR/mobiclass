package com.mobiclass.entity.enumerator;

public enum MetodosMgrAtividade {
	addAreaAtividade("addAreaAtividade"),
	addAtividadeSlides("addAtividadeSlides"),
	addPergunta("addPergunta"),
	addRespostaPergunta("addRespostaPergunta"),
	addAtividade("addAtividade"),
	addPontuacao("addPontuacao"),
	removeAtividade("removeAtividade"),
	getPagina("getPagina"),
	getAtividades("getAtividades"),
	getAtividadeDisponivel("getAtividadeDisponivel"),
	getQuestaoCategoria("getQuestaoCategoria"),
	getQuestao("getQuestao"),
	getJogosForca("getJogosForca"),
	marcarInicioAtividadeExecutada("marcarInicioAtividadeExecutada"),
	marcarFimAtividadeExecutada("marcarFimAtividadeExecutada"),
	getPontuacaoAluno("getPontuacaoAluno"),
	getAreaAtividade("getAreaAtividade"),
	getAreasAtividade("getAreasAtividade"),
	setPagina("setPagina"),
	setOnAtividade("setOnAtividade"),
	setOffAtividade("setOffAtividade"),
	setStartAtividade("setStartAtividade"),
	setStopAtividade("setStopAtividade"),
	getAtividadeExecutada("getAtividadeExecutada"),
	getNomeAtividade("getNomeAtividade"),
	loginAluno("loginAluno"),
	loginProfessor("loginProfessor"),
	logoffAluno("logoffAluno"),
	logoffProfessor("logoffProfessor");
	
	private String nome;
	
	private MetodosMgrAtividade(String nome) {
		this.nome = nome;
	}
	
	public String getNome(){
		return nome;
	}
}
