package com.mobiclass.entity;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Pessoa implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private long RA;
	private String nome;
	private String senha;
	private String email;
	private int logado;
	private Date dataNasc;
	private String sexo;
	
	public Pessoa(){
		
	}
	
	
	public Pessoa(long rA, String nome, String senha, String email,
			Date dataNasc, String sexo) {
		super();
		RA = rA;
		this.nome = nome;
		this.senha = senha;
		this.email = email;
		this.dataNasc = dataNasc;
		this.sexo = sexo;
	}


	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public void setLogado(int logado){
		this.logado = logado;
	}
	public int getLogado(){
		return logado;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public Date getDataNasc() {
		return dataNasc;
	}
	public void setDataNasc(String dataNasc) {
		this.dataNasc = converteData(dataNasc);
	}
	public void setDataNasc(Date dataNasc) {
		this.dataNasc = dataNasc;
	}
	public long getRA() {
		return RA;
	}
	public void setRA(long rA) {
		RA = rA;
	}	
	private Date converteData(String data) {  
	    Date date = null;  
	    try {  
	        if(data!=null && !data.isEmpty()){
	        	SimpleDateFormat dtOutput = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);  
		        date = dtOutput.parse(data);
	        }
	    }
	    catch (ParseException e) {  
	        e.printStackTrace();  
	    }  
	    return date;  
	}
}
