package com.mobiclass.entity;

import android.annotation.SuppressLint;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class AtividadeExecutada implements Serializable{
	private static final long serialVersionUID = 1L;
	private AtividadeExecutadaID id;
	private Date dataInicio;	
	private Date dataFim;
	private int isFinalizado;

	
	@SuppressLint("SimpleDateFormat")
	public AtividadeExecutada(String codTurma, int anoTurma, long codAluno, String codAtividade, String codDisciplina, long areaAtividade) {
		
		Date hj = new Date();
		SimpleDateFormat formatData = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		setId(new AtividadeExecutadaID(codAluno, codAtividade, codDisciplina, codTurma, anoTurma, areaAtividade));
		setDataInicio(converteData(formatData.format(hj)));
		setIsFinalizado(0);
	}
	

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public int getIsFinalizado() {
		return isFinalizado;
	}

	public void setIsFinalizado(int isFinalizado) {
		this.isFinalizado = isFinalizado;
	}
	
	@SuppressLint("SimpleDateFormat")
	private Date converteData(String data) {  
	    Date date = null;  
	    try {  
	        SimpleDateFormat dtOutput = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
	        date = dtOutput.parse(data);
	    }
	    catch (ParseException e) {  
	        e.printStackTrace();  
	    }  
	    return date;  
	}
	
	public AtividadeExecutada() {
		setId(new AtividadeExecutadaID());
	}

	public AtividadeExecutadaID getId() {
		return id;
	}

	public void setId(AtividadeExecutadaID id) {
		this.id = id;
	}
	
}
