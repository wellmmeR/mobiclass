package com.mobiclass.entity;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class Pontuacao implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private PontuacaoID id;
	private Date dataTarefa;
	private String descricao;
	private double pontos;
	private String codLancamento;
		
	
	public String getCodLancamento() {
		return codLancamento;
	}
	public void setCodLancamento(String codLancamento) {
		this.codLancamento = codLancamento;
	}

	public Date getDataTarefa() {
		return dataTarefa;
	}

	public void setDataTarefa(Date dataTarefa) {
		this.dataTarefa = dataTarefa;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getPontos() {
		return pontos;
	}

	public void setPontos(double pontos) {
		this.pontos = pontos;
	}
	
	public PontuacaoID getId() {
		return id;
	}
	public void setId(PontuacaoID id) {
		this.id = id;
	}
	
	public String toString(){
		return"||"+
		id.getRaAluno()+'|'+
		id.getCodAtividade()+'|'+
		id.getAreaAtividade()+'|'+
		id.getCodDisciplina()+'|'+
		getDataTarefa()+'|'+
		getDescricao()+'|'+
		getPontos()+"||"; 
	}
	
	public Pontuacao() {
		setId(new PontuacaoID());
	}
	
	
	public Pontuacao(long ra, String codAtividade, String codDisciplina, String codTurma, 
		int anoTurma, String descricao, double pontos, String codLancamento, long areaAtividade){
		Date hj = new Date();
		SimpleDateFormat formatData = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);
		
		setId(new PontuacaoID(ra, codAtividade, codDisciplina, anoTurma, codTurma, areaAtividade));
		
		setDataTarefa(converteData(formatData.format(hj)));
		setDescricao(descricao);
		setPontos(pontos);
		setCodLancamento(codLancamento);
	
	}
	private Date converteData(String data) {  
	    Date date = null;  
	    try {  
	        SimpleDateFormat dtOutput = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);  
	        date = dtOutput.parse(data);
	    }
	    catch (ParseException e) {  
	        e.printStackTrace();  
	    }  
	    return date;  
	}
}
