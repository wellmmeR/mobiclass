package com.mobiclass.entity;

import java.io.Serializable;


public class AreaAtividade implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String areaAtividade;

	public AreaAtividade() {

	}

	public AreaAtividade(String AreaAtividade) {
		this.setAreaAtividade(AreaAtividade);
	}

	public String toString() {
		return "||" + getId() + '|' + getAreaAtividade() + "||";
	}

	public String getAreaAtividade() {
		return areaAtividade;
	}

	public void setAreaAtividade(String areaAtividade) {
		this.areaAtividade = areaAtividade;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
