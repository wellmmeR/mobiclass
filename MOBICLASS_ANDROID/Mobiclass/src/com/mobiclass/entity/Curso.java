package com.mobiclass.entity;

import java.io.Serializable;

public class Curso implements Serializable {

	private static final long serialVersionUID = 1L;

	private String codCurso;
	private String nome;
	private int isDisponivel;
	
	public int getIsDisponivel() {
		return isDisponivel;
	}
	public void setIsDisponivel(int isDisponivel) {
		this.isDisponivel = isDisponivel;
	}
	public Curso() {
		
	}
	public Curso(String codigoCurso, String nomeCurso, int isDisponivel) {

		setCodCurso(codigoCurso);
		setNome(nomeCurso);
		setIsDisponivel(isDisponivel);
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}	
	
	public String toString(){
		return "||"+getCodCurso()+"|"+getNome()+"|"+getIsDisponivel()+"||";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codCurso == null) ? 0 : codCurso.hashCode());
		result = prime * result + isDisponivel;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Curso other = (Curso) obj;
		if (codCurso == null) {
			if (other.codCurso != null)
				return false;
		} else if (!codCurso.equals(other.codCurso))
			return false;
		if (isDisponivel != other.isDisponivel)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}
	
	
}
