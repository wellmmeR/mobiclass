package com.mobiclass.entity;

import java.io.Serializable;

public class AtividadeID implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String codigoAtividade;
	private String codigoDisciplina;
	private long areaAtividade;
	
	public AtividadeID() {
	
	}
	
	public AtividadeID(String codigoAtividade, String codigoDisciplina, long areaAtividade){
		this.codigoAtividade = codigoAtividade;
		this.codigoDisciplina = codigoDisciplina;
		this.areaAtividade = areaAtividade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ (int) (areaAtividade ^ (areaAtividade >>> 32));
		result = prime * result
				+ ((codigoAtividade == null) ? 0 : codigoAtividade.hashCode());
		result = prime
				* result
				+ ((codigoDisciplina == null) ? 0 : codigoDisciplina.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtividadeID other = (AtividadeID) obj;
		if (areaAtividade != other.areaAtividade)
			return false;
		if (codigoAtividade == null) {
			if (other.codigoAtividade != null)
				return false;
		} else if (!codigoAtividade.equals(other.codigoAtividade))
			return false;
		if (codigoDisciplina == null) {
			if (other.codigoDisciplina != null)
				return false;
		} else if (!codigoDisciplina.equals(other.codigoDisciplina))
			return false;
		return true;
	}
	
	public String getCodigoAtividade() {
		return codigoAtividade;
	}

	public void setCodigoAtividade(String codigoAtividade) {
		this.codigoAtividade = codigoAtividade;
	}

	public String getCodigoDisciplina() {
		return codigoDisciplina;
	}

	public void setCodigoDisciplina(String codigoDisciplina) {
		this.codigoDisciplina = codigoDisciplina;
	}

	public long getAreaAtividade() {
		return areaAtividade;
	}

	public void setAreaAtividade(long areaAtividade) {
		this.areaAtividade = areaAtividade;
	}

}
