package com.mobiclass;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.mobiclass.adapter.AtividadeAdapter;
import com.mobiclass.adapter.item.AtividadeListItem;
import com.mobiclass.entity.Aluno;
import com.mobiclass.entity.AtividadeDisponivel;
import com.mobiclass.entity.AtividadeDisponivelID;
import com.mobiclass.entity.AtividadeExecutada;
import com.mobiclass.entity.Disciplina;
import com.mobiclass.entity.Turma;
import com.mobiclass.service.Facilitador;
import com.mobiclass.service.Service;

public class AtividadesActivity extends ListActivity {

	private List<AtividadeListItem> lista;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		lista = new ArrayList<AtividadeListItem>();
		new TransacaoAtividadesDisponiveis(this).execute();
			
	}

	private int getTipoDeAtividade() {
		return R.drawable.jogodaforca;
	}

	private int getIMGConcluido(AtividadeExecutada executada) {
		if(executada.getIsFinalizado() == 1)
			return R.drawable.certo;
		return R.drawable.interrogacao;
	}

	private String getNomeAtividade(long ra, String senha,AtividadeDisponivelID id) {
		return Service.getNomeAtividade(ra, senha, id.getCodAtividade(), id.getAreaAtividade(), id.getCodDisciplina());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mobiclass_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		AtividadeExecutada at = lista.get(position).getAtividadeExecutada();
		if(at!=null){
			if(at.getIsFinalizado()==0)
			{
				Intent i = new Intent(this, JogoForcaActivity.class);
				Bundle params = new Bundle();
				params.putSerializable("atividade", lista.get(position).getAtividadeDisponivel());
				i.putExtras(params);
				startActivity(i);
				finish();
			}
			else
			{
				new TransacaoPontuacao(this).execute(at);
			}
		}
	}
	
	public boolean deslogar(MenuItem v) {
	    App app = (App) getApplication();
	    app.deslogar();
	    return true;
	}
	
	@Override
	public void onBackPressed() {
		startActivity(new Intent(this, DisciplinasActivity.class));
		finish();
	}
	
	class TransacaoAtividadesDisponiveis extends AsyncTask<Void, Void, Void>{
		
		private Context c;
		private String mensagem;
		private ProgressDialog dialog;
		public TransacaoAtividadesDisponiveis(Context c) {
			this.c = c;
			dialog = new ProgressDialog(c);
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage("Buscando Atividades Disponiveis...");
			dialog.show();
		}
		@Override
		protected Void doInBackground(Void... params) {
			try{
				App a = (App) getApplication();
				Disciplina d = (Disciplina) getIntent().getExtras().get("disciplina");
				Turma t = (Turma) getIntent().getExtras().get("turma");
				
				for(AtividadeDisponivel atividade : Facilitador.obterAtividadesDisponiveis(a.getUsuario(), d, t)){
					AtividadeListItem item = new AtividadeListItem();
					AtividadeExecutada atExecutada = a.getMapaAtividadeExecutada().get(atividade);
					item.setAtividadeExecutada(atExecutada);
					item.setAtividadeDisponivel(atividade);
					item.setNome(getNomeAtividade(a.getUsuario().getRA(), a.getUsuario().getSenha(), atividade.getId()));
					item.setImg(getIMGConcluido(atExecutada));
					item.setTipo(getTipoDeAtividade());
					lista.add(item);
				}
				mensagem = "Concluido";
			}
			catch(Exception ex){
				mensagem = "Erro ao listar atividades";
			}
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			dialog.hide();
			setListAdapter(new AtividadeAdapter(c, lista));
			Toast.makeText(c, mensagem, Toast.LENGTH_SHORT).show();
		}
	}
	class TransacaoPontuacao extends AsyncTask<AtividadeExecutada, Void, Void>{
		
		private Context c;
		private String mensagem;
		private ProgressDialog dialog;
		public TransacaoPontuacao(Context c) {
			this.c = c;
			dialog = new ProgressDialog(c);
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage("Buscando Pontua��o...");
			dialog.show();
		}
		@Override
		protected Void doInBackground(AtividadeExecutada... params) {
			try{
				Aluno user = ((App)getApplication()).getUsuario();
				AtividadeExecutada at = params[0];
				double pontuacao = Service.getPontuacaoAluno(user.getRA(), user.getSenha(), user.getRA(), 
						at.getId().getCodDisciplina(), at.getId().getCodTurma(), at.getId().getAnoTurma(), 
						at.getId().getCodAtividade(), at.getId().getAreaAtividade());
				mensagem = "Nota: " + pontuacao;
			}
			catch(Exception ex){
				mensagem = "Erro ao listar atividades";
			}
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			dialog.hide();
			setListAdapter(new AtividadeAdapter(c, lista));
			Toast.makeText(c, mensagem, Toast.LENGTH_SHORT).show();
		}
	}
}
