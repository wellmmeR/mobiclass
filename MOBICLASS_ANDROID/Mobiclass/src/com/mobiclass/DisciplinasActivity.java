package com.mobiclass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobiclass.adapter.DisciplinaAdapter;
import com.mobiclass.adapter.item.DisciplinaListItem;
import com.mobiclass.entity.AtividadeDisponivel;
import com.mobiclass.entity.Disciplina;
import com.mobiclass.entity.Turma;
import com.mobiclass.service.Facilitador;

public class DisciplinasActivity extends ListActivity {

	
	private Map<Disciplina, Turma> mapaDisciplinaTurma;
	private ArrayAdapter<String> materias;
	private List<DisciplinaListItem> itens;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mapaDisciplinaTurma= new HashMap<Disciplina, Turma>();
		itens = new ArrayList<DisciplinaListItem>();
		materias = new ArrayAdapter<String>(this, R.layout.activity_disciplinas, R.id.txtNomeDisciplina);
		new TransacaoBuscarDadosAluno(this).execute();
	}
	
	@SuppressLint("InflateParams")
	@SuppressWarnings("unused")
	private void configurarMenu() {
		App a = (App) getApplication();
		TextView titulo_centralizado = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_title_disciplinas, null);
		ActionBar.LayoutParams params = new ActionBar.LayoutParams(
			     ActionBar.LayoutParams.MATCH_PARENT, 
			     ActionBar.LayoutParams.MATCH_PARENT, Gravity.CENTER );
		titulo_centralizado.setText(a.getUsuario().getNome());
	}

	public boolean deslogar(MenuItem v) {
	    App app = (App) getApplication();
	    app.deslogar();
	    return true;
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Bundle bundle = new Bundle();
		DisciplinaListItem item = (DisciplinaListItem) l.getItemAtPosition(position);
		Disciplina d = item.getDisciplina();
		bundle.putSerializable("disciplina", d);
		bundle.putSerializable("turma", mapaDisciplinaTurma.get(d));
		Intent i = new Intent(this, AtividadesActivity.class);
		i.putExtras(bundle);
		startActivity(i);
		finish();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.mobiclass_menu, menu);
		return true;
	}
	
	@Override
	public void onBackPressed() {
		App app = (App) getApplication();
	    app.deslogar();
	}
	
	class TransacaoBuscarDadosAluno extends AsyncTask<Void, Void, Void>{
		private Context c;
		private String mensagem;
		private ProgressDialog dialog;
		
		public TransacaoBuscarDadosAluno(Context c) {
			this.c = c;
			dialog = new ProgressDialog(c);
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage("Buscando Disciplinas e Atividades...");
			dialog.show();
		}
		@Override
		protected Void doInBackground(Void... params) {
			try{
				App app = (App) getApplication();
				List<AtividadeDisponivel> todasAtividades = new ArrayList<AtividadeDisponivel>();
				for(Turma turma : app.getTurmasDoAluno()){
					for(Disciplina disciplina : Facilitador.obterDisciplinas(app.getUsuario(), turma)){
						int contadorDeAtividadesAFazer = 0;
						for(AtividadeDisponivel atividade : Facilitador.obterAtividadesDisponiveis(app.getUsuario(), disciplina, turma)){
							todasAtividades.add(atividade);
							app.getMapaAtividadeExecutada().put(atividade, Facilitador.obterAtividadeDoAluno(app.getUsuario(), atividade.getId()));
							if(app.getMapaAtividadeExecutada().get(atividade).getIsFinalizado() == 0)
								contadorDeAtividadesAFazer++;
							
						}
						materias.add(turma.getCodCurso() + " - " + turma.getId().getCodigoTurma() + " - " + disciplina.getNome() + " - " + contadorDeAtividadesAFazer);
						mapaDisciplinaTurma.put(disciplina, turma);
						itens.add(new DisciplinaListItem(turma.getId().getCodigoTurma(), turma.getId().getAnoTurma(), turma.getCodCurso(), disciplina));
					}
				}
				app.setAtividadesDisponiveis(todasAtividades);
				mensagem = "Concluido";
				//configurarMenu();
			}
			catch(Exception ex){
				//Toast.makeText(c, "Erro ao buscar disciplinas", Toast.LENGTH_SHORT).show();
				mensagem = "Erro ao buscar disciplinas";
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (c != null){
				Toast.makeText(c, mensagem, Toast.LENGTH_SHORT).show();
			dialog.dismiss();
			setListAdapter(new DisciplinaAdapter(c, itens));
			}
		}
		
	}
}
