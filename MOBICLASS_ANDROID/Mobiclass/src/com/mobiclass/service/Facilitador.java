package com.mobiclass.service;

import java.util.List;

import com.mobiclass.entity.AtividadeDisponivel;
import com.mobiclass.entity.AtividadeDisponivelID;
import com.mobiclass.entity.AtividadeExecutada;
import com.mobiclass.entity.Disciplina;
import com.mobiclass.entity.Pessoa;
import com.mobiclass.entity.Turma;

public class Facilitador {

	
	public static List<Disciplina> obterDisciplinas(Pessoa p, Turma t) throws Exception{
		return Service.getDisciplinasDoALuno(p.getRA(), p.getSenha(), t.getId().getCodigoTurma(), p.getRA(), 1);
	}
	
	public static List<AtividadeDisponivel> obterAtividadesDisponiveis(Pessoa p, Disciplina d, Turma t) throws Exception{
		return Service.getAtividadesDisponiveis(p.getRA(), p.getSenha(), d.getCodigoDisciplina(), 0, t.getId().getCodigoTurma(), t.getId().getAnoTurma());
	}
	
	public static AtividadeExecutada  obterAtividadeDoAluno(Pessoa p, AtividadeDisponivelID id) throws Exception{
		return Service.getAtividadeParaOAluno(p.getRA(), p.getSenha(), id.getCodAtividade(), id.getCodDisciplina(), id.getAreaAtividade(), id.getCodTurma(), id.getAnoTurma(), p.getRA());
	}
	
}
