package com.mobiclass.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobiclass.R;
import com.mobiclass.adapter.item.AtividadeListItem;

public class AtividadeAdapter extends BaseAdapter{
	
	private Context context;
	private List<AtividadeListItem> lista;
	
	public AtividadeAdapter(Context context, List<AtividadeListItem> lista) {
		this.context = context;
		if(lista.size()==0)
			lista.add(itemListaVazia());
		this.lista = lista;
	}
	
	private AtividadeListItem itemListaVazia(){
		AtividadeListItem itemVazio = new AtividadeListItem();
		itemVazio.setNome("Nenhuma Atividade");
		return itemVazio;
	}
	

	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint({ "ViewHolder", "InflateParams" })
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		AtividadeListItem item = lista.get(position);
		LayoutInflater inflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.listview_item_atividade, null);
		
		((ImageView)view.findViewById(R.id.imageTipo)).setImageResource(item.getTipo());
		((TextView)view.findViewById(R.id.txtNomeAtividade)).setText(item.getNome());
		((ImageView)view.findViewById(R.id.imageConcluido)).setImageResource(item.getImg());
		
		return view;
	}

}
