package com.mobiclass.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mobiclass.R;
import com.mobiclass.adapter.item.DisciplinaListItem;

public class DisciplinaAdapter extends BaseAdapter{
	
	private List<DisciplinaListItem> itens;
	private Context context;
	
	public DisciplinaAdapter() {
	}
	
	public DisciplinaAdapter(Context context, List<DisciplinaListItem> lista) {
		this.context = context;
		itens = lista;
	}
	
	@Override
	public int getCount() {
		return itens.size();
	}

	@Override
	public Object getItem(int position) {
		return itens.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint({ "ViewHolder", "InflateParams" })
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DisciplinaListItem item = itens.get(position);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.listview_item_disciplina, null);
		TextView disciplina =(TextView) view.findViewById(R.id.txtNomeDisciplina);
		TextView curso =(TextView) view.findViewById(R.id.txtNomeCurso);
		TextView turma =(TextView) view.findViewById(R.id.txtNomeTurmaAno);
		
		disciplina.setText(item.getDisciplina().getNome());
		curso.setText(DisciplinaListItem.NOME_CURSO + item.getNomeCurso());
		turma.setText(DisciplinaListItem.NOME_TURMA + item.getNomeTurma() + " " + item.getAnoTurma());
		return view;
	}

}
