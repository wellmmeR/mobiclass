package com.mobiclass;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobiclass.entity.AtividadeDisponivel;
import com.mobiclass.entity.JogoForca;
import com.mobiclass.service.Service;

public class JogoForcaActivity extends Activity {

	private static final int NAO_INICIADO = -1;
	private LinearLayout layoutUnder;
	private EditText input;
	int tamanho;
	
	private TextView lblNuMTentativa;
	private TextView lblLetrasPressionadas;
	private TextView lblPergunta;
	private TextView[] textsView;
	private ImageButton btnExibirDica;
	private ImageButton btnExibirTeclado;
	private String  mensagem;
	private List<Double> notas;
	
	private List<JogoForca> listaJogos;
	private int posJogoAtual = NAO_INICIADO;
	private boolean semJogo;
	private JogoForca jogoForca;
	private int numChances;
	private Activity activity = this;
	private AtividadeDisponivel atividade;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {  
		try{
			super.onCreate(savedInstanceState);
		    new TransacaoJogoForca(this).execute();
		}
		catch(Exception ex){
			Toast.makeText(this, "Erro ao tentar iniciar o jogo da forca: " + ex.getMessage(), Toast.LENGTH_LONG).show();
			startActivity(new Intent(this, DisciplinasActivity.class));
		}
	}
	
	private void proximoJogo(){
		posJogoAtual++;
		if(listaJogos.size() > posJogoAtual)
			jogoForca = listaJogos.get(posJogoAtual);
		else
			semJogo = true;
		
	}

	private void marcarFim(double nota) {
		App a = (App) getApplication();
		Service.marcarFimAtividade(a.getUsuario().getRA(), a.getUsuario().getSenha(), atividade.getId().getCodAtividade(), 
				atividade.getId().getCodDisciplina(), atividade.getId().getAreaAtividade(), atividade.getId().getCodTurma(), 
				atividade.getId().getAnoTurma(), a.getUsuario().getRA(), nota);
	}
	private void buscarJogosForca() throws Exception{

		atividade = (AtividadeDisponivel) getIntent().getExtras().getSerializable("atividade");
		App a = (App) getApplication();
		listaJogos = 
				Service.getJogosForca(a.getUsuario().getRA(), a.getUsuario().getSenha(), atividade.getId().getCodDisciplina(),
						atividade.getId().getCodAtividade(), atividade.getId().getAreaAtividade());
		Service.marcarInicioAtividade(a.getUsuario().getRA(), a.getUsuario().getSenha(), atividade.getId().getCodAtividade(), 
				atividade.getId().getCodDisciplina(), atividade.getId().getAreaAtividade(), atividade.getId().getCodTurma(), 
				atividade.getId().getAnoTurma(), a.getUsuario().getRA());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void obterComponentes() {
		layoutUnder = (LinearLayout) findViewById(R.id.layoutUnder);
		lblNuMTentativa = (TextView) findViewById(R.id.lblNumTentativas);
		lblLetrasPressionadas = (TextView) findViewById(R.id.letrasPressionadas);
		lblPergunta = (TextView) findViewById(R.id.lblPergunta);
		btnExibirDica= (ImageButton) findViewById(R.id.imgDica);
		btnExibirDica.setOnClickListener(new ActionShowDica());
		btnExibirTeclado = (ImageButton) findViewById(R.id.imgBtnExibirTeclado);
		btnExibirTeclado.setOnClickListener(new ActionExibirTeclado());
		input = (EditText) findViewById(R.id.inputLetra);
		input.addTextChangedListener(new EntradaDados());
		input.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
		notas = new ArrayList<Double>();
	}

	private void montarMapa() {
		String[] caracteres = dividirString(jogoForca.getPalavra());
		if(textsView != null)
			removerTextos();
		
		textsView = new TextView[caracteres.length];
		for(int i=0;i<textsView.length;i++){
			textsView[i] = new TextView(this);
			textsView[i].setTextSize(60);
			if(caracteres[i].equals(" "))
				textsView[i].setText(" ");
			else
				textsView[i].setText("_");
			layoutUnder.addView(textsView[i]);
		}
		
	}

	private void removerTextos() {
		for(int i=0; i < textsView.length; i++){
			ViewGroup layout = (ViewGroup) textsView[i].getParent();
			if(layout != null)
				layout.removeView(textsView[i]);
		}
	}

	private void montarJogo() {
		jogoForca.setPalavra(jogoForca.getPalavra().toLowerCase(Locale.US));
		numChances = jogoForca.getNumTentativas();
		lblNuMTentativa.setText(String.valueOf(numChances));
		lblLetrasPressionadas.setText("");
		lblPergunta.setText(jogoForca.getPergunta());
		montarMapa();
	}
	
	private void jogar(String letra){
		int index = 0;
		
		if(!verificarSeFoiPressionado(letra))
		{
			adicionarLetraPressionada(letra);
			if(jogoForca.getPalavra().contains(letra))
			{
				for(String letra2 : dividirString(jogoForca.getPalavra())){
					if(letra2.equals(letra)){
						textsView[index].setText(letra);
					}
					index++;
				}
			}
			else 
			{
				numChances--;
				lblNuMTentativa.setText(String.valueOf(numChances));
			}
		}
		
		if(terminouJogoAtual())
		{
			double nota = ((atividade.getValorAtividade() / listaJogos.size()) * numChances) / jogoForca.getNumTentativas();
			notas.add(nota);
			mensagem = "Nota: " + formatarDouble(nota);
			mostrarMensagem(new ActionDialogProximoJogo());
		}
	}
	
	private boolean verificarSeFoiPressionado(String letra){
		boolean retorno = false;
		for(String t :lblLetrasPressionadas.getText().toString().split(" ")){
			if(t.equals(letra)){
				retorno = true;
				break;
			}
		}
		return retorno;
	}
	
	private boolean acertouPalavra() {
		if(nenhumUnderline())
			return true;
		else 
			return false;
	}

	private boolean terminouJogoAtual() {
		if(numChances <= 0)
			return true;
		if(acertouPalavra())
			return true;
		return false;
	}

	private void adicionarLetraPressionada(String letra) {
		String texto = lblLetrasPressionadas.getText().toString();
		lblLetrasPressionadas.setText(texto + " " + letra);
		
	}

	private void mostrarMensagem(OnClickListener action){
		TextView title = new TextView(this);
		String numeroJogo = semJogo ? "": (posJogoAtual+1)+"";
		String jogoOuAtividade = semJogo? " da Atividade " : " do jogo ";
		title.setText("FIM"+ jogoOuAtividade +  numeroJogo);
		title.setPadding(10, 10, 10, 10);
		title.setGravity(Gravity.CENTER);
		title.setTextSize(23);
		
		TextView msg = new TextView(this);
		msg.setText(mensagem);
		msg.setPadding(10, 10, 10, 10);
		msg.setGravity(Gravity.CENTER);
		msg.setTextSize(18);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setView(msg);
		builder.setCustomTitle(title);
		builder.setPositiveButton(semJogo ? "FIM":"Pr�ximo", action);
		builder.create().show();
	}

	private boolean nenhumUnderline() {
		for(TextView t : textsView){
			if("_".equals(t.getText().toString()))
				return false;
		}
		return true;
	}
	
	private double somarNotas(){
		double total = 0;
		for(double d : notas)
			total+= d;
		return total;
	}
	
	private String formatarDouble(double nota){
		DecimalFormat format = new DecimalFormat();
		format.setMaximumFractionDigits(2);
		return format.format(nota);
	}
	
	@Override
	public void onBackPressed() {
	}
	
	
	class ActionDialogProximoJogo implements DialogInterface.OnClickListener{

		@Override
		public void onClick(DialogInterface dialog, int which) {
			proximoJogo();
			if(semJogo)
			{
				double nota = somarNotas();
				mensagem = "Nota: " + formatarDouble(nota);
				mostrarMensagem(new ActionDialogFimJogo());
			}
			else
			{
				montarJogo();
			}
			
		}
		
	}
	
	class ActionDialogFimJogo implements DialogInterface.OnClickListener{
		@Override
		public void onClick(DialogInterface dialog, int which) {
			new TransacaoGravarJogoForca().execute();
		}
	}
	
	class ActionShowDica implements android.view.View.OnClickListener{
		@Override
		public void onClick(View v) {
			Toast.makeText(activity, "Dica: " + jogoForca.getDica(), Toast.LENGTH_LONG).show();
		}
	}
	
	class ActionExibirTeclado implements android.view.View.OnClickListener{
		@SuppressLint("NewApi")
		@Override
		public void onClick(View v) {
			if(input!=null)
				input.requestFocus();
			final InputMethodManager inputMethodManager = 
					(InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					inputMethodManager.showSoftInput(input, InputMethodManager.SHOW_IMPLICIT);
		}
	}
	
	class EntradaDados implements TextWatcher{

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,int after) {
			tamanho = s.toString().length();	
		}
		@Override
		public void afterTextChanged(Editable s) {
			int qt = s.length();
			String valor = s.toString();

			if(s.length() > 0 && !isBackSpace(qt)){
				String letra = valor.substring(valor.length()-1);
				if(deAcordoComARegex(letra))
					jogar(letra);
			}
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			
		}
		
	}
	
	private boolean deAcordoComARegex(String letra){
		String regex = "[abcdefghijklmnopqrstuvwxyz1234567890������������������]";
		if(letra.matches(regex))
			return true;
		return false;
	}
	private boolean isBackSpace(int qt){
		boolean boleano = false;
		if(qt <= tamanho)
			boleano = true;
		return boleano;
	}
	
	private String[] dividirString(String texto){
		String[] aux = texto.split("");
		if(aux.length == 0)
			return aux;
		
		String[] array = new String[aux.length-1];
		for(int i=0; i<array.length; i++){
			array[i] = aux[i+1];
		}
		return array;
	}
	class TransacaoJogoForca extends AsyncTask<Void, Void, Void>{
		
		private ProgressDialog dialog;
		private String mensagem;
		private Context context;
		
		public TransacaoJogoForca(Context c) {
			context = c;
			dialog = new ProgressDialog(c);
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage("Buscando Dados do Jogo...");
			dialog.show();
		}
		@Override
		protected Void doInBackground(Void... params) {
			try{
				buscarJogosForca();
				mensagem = "Concluido";
			}
			catch(Exception ex){
				mensagem = "Erro ao buscar jogo da forca";
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			setContentView(R.layout.activity_jogo_forca);
			obterComponentes();
			proximoJogo();
		    montarJogo();
			dialog.hide();
			Toast.makeText(context, mensagem, Toast.LENGTH_SHORT).show();
		}
	}
	class TransacaoGravarJogoForca extends AsyncTask<Void, Void, Void>{
		@Override
		protected Void doInBackground(Void... params) {
			marcarFim(somarNotas());
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			startActivity(new Intent(activity, DisciplinasActivity.class));
		}
	}
	
}
