function apenasNumerosDecimais(digito) 
{
	target = digito.currentTarget;
	if(digito.keyCode == 8)/*deletar*/
	{
		deletar(target);
	}
	else
	{
		if(digito.keyCode >= 48 && digito.keyCode <= 57)
		{
			target.possuiPrimeiroDigito = true;
			return true;
		}
		else
		{
			if(digito.keyCode==190 && !target.possuiPonto)
			{
				if(target.possuiPrimeiroDigito)
				{
					target.possuiPonto = true;
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				if(isSetas(digito))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}
	}	
}
		
function deletar(target)
{
	
	var texto = target.value;
	if(texto.length > 0)
	{
		texto = texto.substring(0,texto.length-1);
		if(texto.length==0)
			target.possuiPrimeiroDigito = false;
		
		if(texto.indexOf(".") == -1)
			target.possuiPonto = false;
	}
}

function isSetas(digito)
{
	if(digito.keyCode >= 37 && digito.keyCode <= 40)
		return true;
	return false;
}

function apenasNumero(event)
{
	if(isNumber(event.keyCode))
		return true;
	if(event.keyCode == 8)/*deletar*/
		return true;
	if(isSetas(event))
		return true;
	return false;
}


function isNumber(code)
{
	if(code >=48 && code <=57)
		return true;
	else
		return false;
}
