package web.src.entity;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;

public class NegocioException extends Exception{

	private static final long serialVersionUID = 1L;
	private String titulo;
	private Severity tipo;
	
	public String getTitulo(){
		return titulo;
	}
	
	public NegocioException() {

	}
	
	public NegocioException(String mensagem){
		super(mensagem);
	}
	public NegocioException(String titulo, String mensagem){
		super(mensagem);
		this.titulo = titulo;
	}
	
	public NegocioException(String titulo, String mensagem, Severity tipo){
		super(mensagem);
		this.titulo = titulo;
		this.tipo = tipo; 
	}
	
	public NegocioException(Throwable causa){
		super(causa);
	}
	
	public NegocioException(String mensagem, Throwable causa){
		super(mensagem, causa);
	}

	public FacesMessage.Severity getTipo() {
		return tipo;
	}
}
