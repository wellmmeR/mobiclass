package web.src.entity;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class Util {
	
	public static int TIPO_ALUNO = 1;
	public static int TIPO_PROFESSOR = 2;
	public static int TIPO_ADMIN = 3;
	
	
	public static Integer booleanParaInteiro(Boolean b)
	{
		if(b == null)
			return null;
		else if(b)
			return 1;
		else
			return 0;
	}
	
	public static Boolean inteiroParaBoolean(Integer i)
	{
		if(i == null)
			return null;
		else if(i.equals(1))
			return true;
		else
			return false;
	}
	
	public static void adicionarObjetoNaSessao(SessaoEnum chave, Object o){
		getHttpSession(true).setAttribute(chave.name(), o);
	}
	
	public static Object pegarObjetoDaSessao(SessaoEnum chave){
		return getHttpSession(true).getAttribute(chave.name());
	}
	
	public static Object pegarObjetoDaSessaoERemover(SessaoEnum chave){
		Object objeto = getHttpSession(true).getAttribute(chave.name());
		removerObjetoDaSessao(chave);
		return objeto;
	}
	
	public static void removerObjetoDaSessao(SessaoEnum chave){
		getHttpSession(true).removeAttribute(chave.name());
	}
	
	public static HttpSession getHttpSession(boolean b){
		 return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(b);
	}
	
	public static void exibirMensagemDeErro(String titulo, String mensagem){
		exibirMensagem(titulo,mensagem, FacesMessage.SEVERITY_ERROR);
	}
	
	public static void exibirMensagemDeAviso(String titulo, String mensagem){
		exibirMensagem(titulo,mensagem, FacesMessage.SEVERITY_WARN);
	}
	
	public static void exibirMensagemDeSucesso(String titulo, String mensagem){
		exibirMensagem(titulo,mensagem, FacesMessage.SEVERITY_INFO);
	}
	
	public static void exibirMensagem(String titulo, String mensagem,Severity tipo){
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(tipo, titulo, mensagem));
	}
	
	public static void exibirMensagem(String idComponent,String titulo, String mensagem,Severity tipo){
		FacesContext.getCurrentInstance().addMessage(idComponent, new FacesMessage(tipo, titulo, mensagem));
	}
	
	public static void irPara(String url, String origem){
		@SuppressWarnings("unchecked")
		List<String> urls = (List<String>)pegarObjetoDaSessaoERemover(SessaoEnum.LISTA_PAGINAS);
		if(urls == null)
			urls = new ArrayList<>();
		
		urls.add(origem);
		adicionarObjetoNaSessao(SessaoEnum.LISTA_PAGINAS, urls);
		redirecionar(url);
	}
	
	public static void irPaginaAnterior(){
		@SuppressWarnings("unchecked")
		List<String> urls = (List<String>)pegarObjetoDaSessaoERemover(SessaoEnum.LISTA_PAGINAS);
		if(urls == null || urls.isEmpty())
			redirecionarParaIndex();
		
		String destino = urls.get(urls.size()-1);
		urls.remove(destino);
		adicionarObjetoNaSessao(SessaoEnum.LISTA_PAGINAS, urls);
		redirecionar(destino);
	}
	
	public static void redirecionar(String url){
		try{
			FacesContext.getCurrentInstance().getExternalContext().redirect(url);
		}
		catch(Exception ex){
			ex.printStackTrace();
			redirecionarParaPaginaDeErro();
			System.out.println("ERRO no redirecionamento");
		}
	}
	public static void redirecionarParaPaginaDeErro(){
		redirecionar("paginas/error.xhtml");
	}
	public static void redirecionarParaIndex(){
		redirecionar("paginas/index.xhtml");
	}
	
	public static boolean soTemNumeros(String texto) {  
	   try {  
	      Long.parseLong(texto);  
	      return true;  
	   } catch (NumberFormatException e) {  
	      return false;  
	   }  
	}
	
	public static <T> void removerListaDentroDeLista(List<T> completa, List<T> aDeletar){
		if(completa == null || completa.isEmpty() || aDeletar == null || aDeletar.isEmpty())
			return ;
		for(T objeto : aDeletar){
			completa.remove(objeto);
		}
	}
}
