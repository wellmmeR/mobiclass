package web.src.view;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.academico.Disciplina;
import jpa.academico.Turma;
import web.src.controller.DetalharTurmaController;
import web.src.entity.NegocioException;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;

@ManagedBean
@ViewScoped
public class DetalharTurmaView extends View{

	private static final long serialVersionUID = -2709135797332299581L;
	private static final String NOME_PAGINA = "detalhar-turma.xhtml";
	
	private Turma turmaDetalhada;
	private boolean viewIniciada;
	private DetalharTurmaController controller;

	//DISCIPLINA
	private String codDisciplina;
	private String nomeDisciplina;
	private List<Disciplina> disciplinas;
	private List<Disciplina> disciplinasSelecionadas;
	//DISCIPLINA FIM
	
	public DetalharTurmaView(){
		controller = new DetalharTurmaController();
	}
	
	public void init(){
		if(!viewIniciada){
			obterObjetoDaSessao();
			if(turmaDetalhada != null){
				initDisciplina();
				viewIniciada = true;
			}
			else{
				Util.redirecionar("gestao-turma.xhtml");
			}
		}
	}
	
	public void obterObjetoDaSessao(){
		turmaDetalhada = (Turma) Util.pegarObjetoDaSessaoERemover(SessaoEnum.OBJETO_TURMA);
	}
	
	public void voltar(){
		Util.irPaginaAnterior();
	}
/*----------------------------------------------DISCIPLINA----------------------------------------------------*/
	private void initDisciplina(){
		pesquisarDisciplina();
		
	}
	public void pesquisarDisciplina(){
		try{
			disciplinas = controller.disciplinasDaTurma(codDisciplina, nomeDisciplina, turmaDetalhada.getId().getCodigoTurma(), turmaDetalhada.getId().getAnoTurma());
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("erro na pesquisa da disciplina", nEx.getMessage());
		}
	}
	public void vincularDisciplina(){
		Util.adicionarObjetoNaSessao(SessaoEnum.OBJETO_VIEW, this);
		Util.irPara("pesquisar-disciplina.xhtml", NOME_PAGINA);
	}
	public void desvincularDisciplina(){
		try{
			controller.desvincularDisciplina(disciplinasSelecionadas, turmaDetalhada.getId());
			Util.removerListaDentroDeLista(disciplinas, disciplinasSelecionadas);
			disciplinasSelecionadas = null;
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("erro ao desvincular disciplina", nEx.getMessage());
		}
	}
	public void detalharDisciplina(){
		
	}
	public void limparDisciplina(){
		codDisciplina = null;
		nomeDisciplina = null;
	}
	public boolean nenhumaDisciplinaSelecionada(){
		if(disciplinasSelecionadas == null || disciplinasSelecionadas.isEmpty())
			return true;
		return false;
	}
	
	public boolean umaTurmaSelecionada(){
		if(nenhumaDisciplinaSelecionada() || disciplinasSelecionadas.size() != 1)
			return false;
		return true;
	}
	
	@Override
	public void disciplinasDaPesquisa(List<Disciplina> disciplinas)
			throws NegocioException {
		try{
			controller.vincularDisciplina(disciplinas, turmaDetalhada.getId());
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}
	
	@Override
	public List<Disciplina> pesquisarDisciplinaView(Disciplina d)
			throws NegocioException {
		try{
			List<Disciplina> todas = controller.buscarDisciplinas(d);
			List<Disciplina> disciplinasPossiveis = controller.disciplinasDaTurma(d.getCodigoDisciplina(), d.getNome(), turmaDetalhada.getId().getCodigoTurma(), turmaDetalhada.getId().getAnoTurma());
			Util.removerListaDentroDeLista(todas, disciplinasPossiveis);
			return todas;
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}
/*----------------------------------------------DISCIPLINA FIM----------------------------------------------------*/

	public Turma getTurmaDetalhada() {
		return turmaDetalhada;
	}

	public void setTurmaDetalhada(Turma turmaDetalhada) {
		this.turmaDetalhada = turmaDetalhada;
	}

	public String getCodDisciplina() {
		return codDisciplina;
	}

	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}

	public String getNomeDisciplina() {
		return nomeDisciplina;
	}

	public void setNomeDisciplina(String nomeDisciplina) {
		this.nomeDisciplina = nomeDisciplina;
	}

	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}

	public List<Disciplina> getDisciplinasSelecionadas() {
		return disciplinasSelecionadas;
	}

	public void setDisciplinasSelecionadas(List<Disciplina> disciplinasSelecionadas) {
		this.disciplinasSelecionadas = disciplinasSelecionadas;
	}

}
