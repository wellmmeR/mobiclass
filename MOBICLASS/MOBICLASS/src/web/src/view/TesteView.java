package web.src.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.atividade.Atividade;

import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleModel;

import web.src.controller.Controller;
import web.src.entity.Theme;
import web.src.entity.Util;

@ManagedBean
@ViewScoped
public class TesteView implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -57181529138015853L;
	private ScheduleModel model;
	private List<Atividade> atividades;
	private Atividade atividadeSelecionada;
	private Double valor;
	private Controller controller;
	private List<Theme> temas;
	private String temaSelecionado;
	private String nomeTema = "afterdark";
	
	public void init(){
		controller = new Controller();
		gerarAtividades();
		setTemas(controller.gerarTemas());
		mensagem();
	}
	
	public void mensagem(){
		Util.exibirMensagemDeSucesso("aeew", "deu certo");
	}
	
	
	public ScheduleModel getModel() {
		if(model == null)
			model = new DefaultScheduleModel();
		return model;
	}
	
	private void gerarAtividades(){
		atividades = new ArrayList<Atividade>();
		for(int i=0; i<20; i++){
			Atividade a = new Atividade("at"+i, "Atividade"+i, "CAL1", 1, 1);
			atividades.add(a);
		}
		
	}
	
	public void submit(){
		nomeTema = temaSelecionado;
	}

	public void setModel(ScheduleModel model) {
		this.model = model;
	}

	public List<Atividade> getAtividades() {
		return atividades;
	}

	public void setAtividades(List<Atividade> atividades) {
		this.atividades = atividades;
	}


	public Atividade getAtividadeSelecionada() {
		return atividadeSelecionada;
	}


	public void setAtividadeSelecionada(Atividade atividadeSelecionada) {
		this.atividadeSelecionada = atividadeSelecionada;
	}


	public Double getValor() {
		return valor;
	}


	public void setValor(Double valor) {
		this.valor = valor;
	}


	public List<Theme> getTemas() {
		return temas;
	}


	public void setTemas(List<Theme> temas) {
		this.temas = temas;
	}


	public String getTemaSelecionado() {
		return temaSelecionado;
	}


	public void setTemaSelecionado(String temaSelecionado) {
		this.temaSelecionado = temaSelecionado;
	}
	public String getNomeTema() {
		return nomeTema;
	}
	public void setNomeTema(String nomeTema) {
		this.nomeTema = nomeTema;
	}
}
