package web.src.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.academico.Disciplina;
import jpa.atividade.AreaAtividade;
import jpa.atividade.Atividade;
import web.src.controller.CadastroAtividadeController;
import web.src.entity.NegocioException;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;
@ManagedBean
@ViewScoped
public class CadastroAtividadeView extends View{
	
	private static final long serialVersionUID = -1506350495120271390L;
	
	public static final int TIPO_QUIZ= 1;
	public static final int TIPO_JOGO_FORCA = 2;
	
	
	private CadastroAtividadeController controller;
	private String nome;
	private String codAtividade;
	private String codDisciplina;
	private String nomeAreaAtividade;
	private List<Disciplina> disciplinas;
	private Long codArea;
	private List<AreaAtividade> areasAtividades;
	private int tipo;
	private int disponivel;
	private Atividade atividade;
	private boolean modoEditar;

	public CadastroAtividadeView(){
		controller = new CadastroAtividadeController();
	}
	@PostConstruct
	public void init(){
		recuperarObjetosDaSessao();
		if(atividade != null)
		{
			modoEditar = true;
			pegarDadosDaAtividade();
		}
		pesquisarAreaAtividade();
		pesquisarDisciplinas();
			
	}
	private void pegarDadosDaAtividade() {
		nome = atividade.getNome();
		codAtividade = atividade.getIdAtividade().getCodigoAtividade();
		codDisciplina= atividade.getIdAtividade().getCodigoDisciplina();
		codArea = atividade.getIdAtividade().getAreaAtividade();
		setDisponivel(atividade.isDisponivel());
	}
	private void recuperarObjetosDaSessao() {
		atividade = (Atividade) Util.pegarObjetoDaSessaoERemover(SessaoEnum.OBJETO_ATIVIDADE);
	}
	private void pesquisarAreaAtividade() {
		setAreasAtividades(controller.buscarTodasAreasAtividades());
		
	}

	private void pesquisarDisciplinas() {
		setDisciplinas(controller.buscarTodasDisciplinas());
	}
	
	
	public void cadastrarAtividade(){
		try{
			String mensagem = controller.cadastrarAtividade(getNome(), getCodAtividade(), codDisciplina, getCodArea(), getDisponivel());
			Util.exibirMensagemDeSucesso(null, mensagem);
			cadastratTipoAtividade();
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	public void cadastrarAreaAtividade(){
		try{
			if(nomeAreaAtividade != null && !nomeAreaAtividade.isEmpty()){
				controller.cadastrarAreaAtividade(nomeAreaAtividade);
				pesquisarAreaAtividade();
				nomeAreaAtividade="";
			}
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	public void modificarAtividade(){
		try{
			String mensagem = controller.modificarAtividade(getNome(), getCodAtividade(), codDisciplina, getCodArea(), getDisponivel());
			Util.exibirMensagemDeSucesso(null, mensagem);
			cadastratTipoAtividade();
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	private void cadastratTipoAtividade() {
		Util.adicionarObjetoNaSessao(SessaoEnum.OBJETO_ATIVIDADE, new Atividade(codAtividade, nome, codDisciplina, getDisponivel(), codArea));
		if(tipo == TIPO_QUIZ){
			Util.redirecionar("cadastro-quiz.xhtml");
		}
		else if(tipo == TIPO_JOGO_FORCA){
			Util.redirecionar("cadastro-jogo-forca.xhtml");
		}
	}
	
	public void voltar(){
		Util.irPaginaAnterior();
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodAtividade() {
		return codAtividade;
	}
	public void setCodAtividade(String codAtividade) {
		this.codAtividade = codAtividade;
	}
	public String getCodDisciplina() {
		return codDisciplina;
	}
	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}
	public Long getCodArea() {
		return codArea;
	}
	public void setCodArea(Long codArea) {
		this.codArea = codArea;
	}
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}
	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}
	public List<AreaAtividade> getAreasAtividades() {
		return areasAtividades;
	}
	public void setAreasAtividades(List<AreaAtividade> areasAtividades) {
		this.areasAtividades = areasAtividades;
	}
	public boolean isModoEditar() {
		return modoEditar;
	}
	public void setModoEditar(boolean modoEditar) {
		this.modoEditar = modoEditar;
	}
	public int getDisponivel() {
		return disponivel;
	}
	public void setDisponivel(int disponivel) {
		this.disponivel = disponivel;
	}
	public String getNomeAreaAtividade(){
		return this.nomeAreaAtividade;
	}
	public void setNomeAreaAtividade(String nomeAreaAtividade){
		this.nomeAreaAtividade = nomeAreaAtividade;
	}
}
