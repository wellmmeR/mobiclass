package web.src.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.academico.Curso;
import jpa.academico.Turma;
import jpa.pessoa.Aluno;
import web.src.controller.DetalharCursoController;
import web.src.entity.NegocioException;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;

@ManagedBean
@ViewScoped
public class DetalharCursoView extends View{

	private static final long serialVersionUID = 2950129524909096688L;
	private static final String NOME_PAGINA = "detalhar-curso.xhtml";
	private DetalharCursoController controller;
	private Curso curso;
	//TURMA
	private String codTurma;
	private String nomeTurma;
	private Integer anoTurma;
	private List<Integer> anos;
	private List<Turma> turmas;
	private Turma turmaSelecionada;
	//TURMA FIM
	
	//ALUNO
	private Long raAluno;
	private String nomeAluno;
	private String emailAluno;
	private List<Aluno> alunos;
	private Aluno alunoSelecionado;
	private List<Aluno> alunosDaPesquisa;
	//ALUNO FIM
	
	public DetalharCursoView() {
		controller = new DetalharCursoController();
	}
	
	
	@PostConstruct
	public void init(){
		pegarObjetosDaSessao();
		if(curso!=null){	
			initTurmas();
			initAlunos();
		}
		else{
			Util.redirecionar("gestao-curso.xhtml");
		}
	}
	
	@SuppressWarnings("unchecked")
	private void pegarObjetosDaSessao() {
		curso = (Curso)Util.pegarObjetoDaSessaoERemover(SessaoEnum.OBJETO_CURSO);
		alunosDaPesquisa = (List<Aluno>) Util.pegarObjetoDaSessaoERemover(SessaoEnum.LISTA_PESSOA);
	}

	public void initTurmas(){
		try{
			anos = controller.buscarAnoDeTurmas(curso);
			pesquisarTurmas();
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	public void initAlunos(){
		try{
			controller.gravarAlunosDaPesquisa(alunosDaPesquisa, curso.getCodCurso());
			pesquisarAlunos();
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
		
	}
	
	public void voltar(){
		Util.irPaginaAnterior();
	}
	
	
	/*----------------------------------------------TURMAS----------------------------------------------------*/
	
	public void detalharTurma(){
		// TODO DFL: concluir o caminho pro detalhamento da turma
	}
	
	public void pesquisarTurmas(){
		try{
			turmas = controller.buscarTurmasDoCurso(curso.getCodCurso(), nomeTurma, codTurma, anoTurma);
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	public void limparTurma(){
		codTurma = "";
		anoTurma = null;
		nomeTurma = "";
	}
	
	public boolean nenhumaTurmaSelecionada(){
		if(turmaSelecionada == null)
			return true;
		return false;
	}
	
	/*----------------------------------------------TURMAS FIM----------------------------------------------------*/
	
	
	/*----------------------------------------------ALUNOS----------------------------------------------------*/
	public void vincularAluno(){
		Util.adicionarObjetoNaSessao(SessaoEnum.OBJETO_CURSO, curso);
		Util.adicionarObjetoNaSessao(SessaoEnum.TIPO_PAGINA, PesquisarPessoaView.MODO_PESSOA_SEM_CURSO);
		Util.irPara("pesquisar-pessoa.xhtml", NOME_PAGINA);
	}
	
	public void detalharAluno(){
		// TODO DFL: concluir o caminho pro detalhamento da turma
	}
	
	public void desvincularAluno(){
		try{
			controller.desvincularAlunoDoCurso(alunoSelecionado.getRA(), curso.getCodCurso());
			alunos.remove(alunoSelecionado);
			alunoSelecionado = null;
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	public void pesquisarAlunos(){
		try{
		alunos = controller.buscarAlunosDoCurso(curso, getRaAluno(), getNomeAluno(), getEmailAluno());
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	public void limparAluno(){
		nomeAluno = "";
		emailAluno = "";
		raAluno = null;
	}
	
	public boolean nenhumAlunoSelecionado(){
		if(alunoSelecionado == null)
			return true;
		return false;
	}
	
	/*----------------------------------------------ALUNOS FIM----------------------------------------------------*/
	
	
	
	
	/*----------------------------------------------METODOS GET AND SET----------------------------------------------------*/
	
	public String getCodTurma() {
		return codTurma;
	}

	public void setCodTurma(String codTurma) {
		this.codTurma = codTurma;
	}

	public String getNomeTurma() {
		return nomeTurma;
	}

	public void setNomeTurma(String nomeTurma) {
		this.nomeTurma = nomeTurma;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Integer getAnoTurma() {
		return anoTurma;
	}

	public void setAnoTurma(Integer anoTurma) {
		this.anoTurma = anoTurma;
	}

	public List<Integer> getAnos() {
		return anos;
	}

	public void setAnos(List<Integer> anos) {
		this.anos = anos;
	}

	public List<Turma> getTurmas() {
		return turmas;
	}

	public void setTurmas(List<Turma> turmas) {
		this.turmas = turmas;
	}

	public Turma getTurmaSelecionada() {
		return turmaSelecionada;
	}

	public void setTurmaSelecionada(Turma turmaSelecionada) {
		this.turmaSelecionada = turmaSelecionada;
	}

	public Long getRaAluno() {
		return raAluno;
	}

	public void setRaAluno(Long raAluno) {
		this.raAluno = raAluno;
	}

	public String getNomeAluno() {
		return nomeAluno;
	}

	public void setNomeAluno(String nomeAluno) {
		this.nomeAluno = nomeAluno;
	}

	public String getEmailAluno() {
		return emailAluno;
	}

	public void setEmailAluno(String emailAluno) {
		this.emailAluno = emailAluno;
	}

	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

	public Aluno getAlunoSelecionado() {
		return alunoSelecionado;
	}

	public void setAlunoSelecionado(Aluno alunoSelecionado) {
		this.alunoSelecionado = alunoSelecionado;
	}

	public List<Aluno> getAlunosDaPesquisa() {
		return alunosDaPesquisa;
	}

	public void setAlunosDaPesquisa(List<Aluno> alunosDaPesquisa) {
		this.alunosDaPesquisa = alunosDaPesquisa;
	}

}
