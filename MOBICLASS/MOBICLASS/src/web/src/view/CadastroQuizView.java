package web.src.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import web.src.controller.CadastroQuizController;
import web.src.entity.NegocioException;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;
import jpa.academico.Turma;
import jpa.atividade.AlternativaQuestao;
import jpa.atividade.Atividade;
import jpa.atividade.Questao;

@ManagedBean
@ViewScoped
public class CadastroQuizView extends View{

	private static final long serialVersionUID = 3966174960365146755L;
	
	private CadastroQuizController controller;
	private String codTurma;
	private List<Turma> turmas;
	private Integer anoTurma;
	private List<Integer> anosTurmas;
	private List<Questao> questoes;
	private Map<Questao, List<AlternativaQuestao>> alternativas;
	private Questao questaoSelecionada;
	private Atividade atividade;

	//modal questao
	private String pergunta;
	private Double valorPergunta;
	private boolean disponivel;
	//
	
	//modal alternativa
	private boolean alternativaCorreta;
	private String conteudoAlternativa;
	private AlternativaQuestao alternativaSelecionada;
	
	@PostConstruct
	public void init(){
		try{
			pegarObjetosDaSessao();
			controller = new CadastroQuizController();
			questoes = new ArrayList<Questao>();
			alternativas = new HashMap<Questao, List<AlternativaQuestao>>();
			pesquisarTurmas();
		}
		catch(Exception ex){
			Util.exibirMensagemDeErro("Erro na inicializa��o", ex.getMessage());
		}
	}
	
	private void pegarObjetosDaSessao() throws NegocioException{
		setAtividade((Atividade) Util.pegarObjetoDaSessaoERemover(SessaoEnum.OBJETO_ATIVIDADE));
	}
	
	private void pesquisarTurmas() {
		turmas = controller.buscarTodasTurmas();
		anosTurmas = new ArrayList<Integer>();
		for(Turma turma : turmas)
		{
			int ano = turma.getId().getAnoTurma();
			if(!anosTurmas.contains(ano))
				anosTurmas.add(ano);
		}	
	}
	
	public void limparCamposModalQuestao(){
		pergunta=null;
		valorPergunta=null;
		disponivel=false;
	}
	public void limparCamposModalAlternativa(){
		alternativaCorreta = false;
		conteudoAlternativa = null;
		alternativaSelecionada = null;
	}
	
	public void criarQuestao(){
		try{
			Questao q = controller.criarQuestao(getAtividade(),getPergunta(), getValorPergunta(), isDisponivel());
			questoes.add(q);
			alternativas.put(q, new ArrayList<AlternativaQuestao>());
			limparCamposModalQuestao();
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
		
	}
	public void modificarQuestao(){
		try{
			Questao q = controller.criarQuestao(getAtividade(),getPergunta(), getValorPergunta(), isDisponivel());
			questoes.add(q);
			alternativas.put(q, new ArrayList<AlternativaQuestao>());			
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
		
	}
	
	public void deletarQuestao(){
		questoes.remove(questaoSelecionada);
	}
	
	public void criarAlternativa(){
		try{
			AlternativaQuestao alternativa = controller.criarAlternativa(getQuestaoSelecionada(), isAlternativaCorreta(), getConteudoAlternativa());
			alternativas.get(questaoSelecionada).add(alternativa);
			limparCamposModalAlternativa();
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	public List<AlternativaQuestao> alternativasDaQuestaoSelecionada(){
		if(getQuestaoSelecionada() == null)
			return null;
		return alternativas.get(getQuestaoSelecionada());
	}
	
	public boolean questaoEstaSelecionada(){
		return !controller.objetoNulo(questaoSelecionada);//se quest�o selecionada for nulo, quer dizer que ela n�o esta selecionada
	}
	
	public boolean alternativaEstaSelecionada(){
		return !controller.objetoNulo(alternativaSelecionada);//se alternativa selecionada estiver nula, quer dizer que ela n�o esta selecionada
	}
	
	private boolean desabilitarBotoesDeletarEModificar(){
		boolean qSelecionada = !controller.objetoNulo(questaoSelecionada);
		boolean aSelecionada = !controller.objetoNulo(alternativaSelecionada);
		return !qSelecionada || !aSelecionada ? true:false; 
	}
	
	public boolean desabilitarBotaoDeletar(){
		
		return desabilitarBotoesDeletarEModificar(); 
	}
	public boolean desabilitarBotaoModificar(){
		
		return desabilitarBotoesDeletarEModificar(); 
	}
	
	public void voltar(){
		Util.irPaginaAnterior();
	}

	public String getCodTurma() {
		return codTurma;
	}

	public void setCodTurma(String codTurma) {
		this.codTurma = codTurma;
	}

	public List<Turma> getTurmas() {
		return turmas;
	}

	public void setTurmas(List<Turma> turmas) {
		this.turmas = turmas;
	}

	public Integer getAnoTurma() {
		return anoTurma;
	}

	public void setAnoTurma(Integer anoTurma) {
		this.anoTurma = anoTurma;
	}

	public List<Integer> getAnosTurmas() {
		return anosTurmas;
	}

	public void setAnosTurmas(List<Integer> anosTurmas) {
		this.anosTurmas = anosTurmas;
	}

	public List<Questao> getQuestoes() {
		return questoes;
	}

	public void setQuestoes(List<Questao> questoes) {
		this.questoes = questoes;
	}

	public Map<Questao, List<AlternativaQuestao>> getAlternativas() {
		return alternativas;
	}

	public void setAlternativas(Map<Questao, List<AlternativaQuestao>> alternativas) {
		this.alternativas = alternativas;
	}

	public Questao getQuestaoSelecionada() {
		return questaoSelecionada;
	}

	public void setQuestaoSelecionada(Questao questaoSelecionada) {
		this.questaoSelecionada = questaoSelecionada;
	}

	public String getPergunta() {
		return pergunta;
	}

	public void setPergunta(String pergunta) {
		this.pergunta = pergunta;
	}

	public Double getValorPergunta() {
		return valorPergunta;
	}

	public void setValorPergunta(Double valorPergunta) {
		this.valorPergunta = valorPergunta;
	}

	public boolean isDisponivel() {
		return disponivel;
	}

	public void setDisponivel(boolean disponivel) {
		this.disponivel = disponivel;
	}

	public boolean isAlternativaCorreta() {
		return alternativaCorreta;
	}

	public void setAlternativaCorreta(boolean alternativaCorreta) {
		this.alternativaCorreta = alternativaCorreta;
	}

	public String getConteudoAlternativa() {
		return conteudoAlternativa;
	}

	public void setConteudoAlternativa(String conteudoAlternativa) {
		this.conteudoAlternativa = conteudoAlternativa;
	}

	public AlternativaQuestao getAlternativaSelecionada() {
		return alternativaSelecionada;
	}

	public void setAlternativaSelecionada(AlternativaQuestao alternativaSelecionada) {
		this.alternativaSelecionada = alternativaSelecionada;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

}
