package web.src.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.academico.Disciplina;
import jpa.academico.Turma;
import jpa.academico.TurmaID;
import jpa.atividade.AreaAtividade;
import jpa.atividade.AtividadeDisponivel;
import jpa.util.Desconsiderar;
import web.src.controller.GestaoAtividadeDisponivelController;
import web.src.entity.NegocioException;
import web.src.entity.Util;
@ManagedBean
@ViewScoped
public class GestaoAtividadeDisponivelView extends View{
	
	private static final long serialVersionUID = 5860418394259525141L;
	private static final String NOME_PAGINA = "gestao-atividade.xhtml";
	
	private boolean viewIniciada;
	private String codAtividade;
	private String codDisciplina;
	private Long areaAtividade;
	private List<AreaAtividade> todasAsAreasDeAtividades; 
	private String codTurma;
	private Integer anoTurma;
	private int disponivel;
	private Date dataLiberacao;
	private Date dataEncerramento;
	private Double valorAtividade;
	
	private List<AtividadeDisponivel> atividades;
	private List<AtividadeDisponivel> atividadesSelecionadas;
	private Map<Turma, List<Disciplina>> disciplinasPorTurma;
	
	private GestaoAtividadeDisponivelController controller;
	
	public GestaoAtividadeDisponivelView() {
		controller = new GestaoAtividadeDisponivelController();
	}
	
	public void init(){
		if(!viewIniciada)
		{
			pesquisarTurmasEDisciplinas();
			pesquisarAreaAtividade();
			disponivel = Desconsiderar.tipoInt;
			anoTurma = Desconsiderar.tipoInt;
			areaAtividade = Desconsiderar.tipoLong;
			valorAtividade = Desconsiderar.tipoDouble;
			pesquisar();
			viewIniciada = true;
		}
	}
	
	private void pesquisarAreaAtividade() {
		todasAsAreasDeAtividades = controller.buscarTodasAreasAtividades();
		
	}

	private void pesquisarTurmasEDisciplinas(){
		disciplinasPorTurma = new TreeMap<Turma, List<Disciplina>>();
		try{
			for(Turma turma : controller.buscarTodasTurmas())
			{	
				if(!disciplinasPorTurma.containsKey(turma))
					disciplinasPorTurma.put(turma, controller.disciplinasDaTurma(turma.getId()));
			}
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro ao buscar Disciplinas da Turma", nEx.getMessage());
		}
		
	}

	public void pesquisar(){
		try{
			atividades =  controller.buscarAtividades(new AtividadeDisponivel(codAtividade, codDisciplina, codTurma, anoTurma, dataLiberacao, dataEncerramento, disponivel,  areaAtividade, valorAtividade));
		}
		catch(Exception ex){
			Util.exibirMensagemDeErro("Erro na Pesquisa", ex.getMessage());
		}
	}
	
	public void cadastrarAtividade(){
		Util.irPara("cadastro-atividade-disponivel.xhtml", NOME_PAGINA);
	}
	
	public void deletarAtividade(){
		try{
			controller.deletarAtividadeDisponivel(atividadesSelecionadas);
			Util.removerListaDentroDeLista(atividades, atividadesSelecionadas);
			Util.exibirMensagemDeSucesso(null, "Atividades Deletadas");
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro ao deletar Atividade Disponivel", nEx.getMessage());
		}
	}
	
	public void detalharAtividade(){
		
	}
	
	public void modificarAtividade(){
		
	}
	
	public boolean temAtividadeSelecionada(){
		if(atividadesSelecionadas != null && !atividadesSelecionadas.isEmpty())
			return true;
		return false;
	}
	
	public boolean temUmaAtividadeSelecionada(){
		if(temAtividadeSelecionada() && atividadesSelecionadas.size() == 1)
			return true;
		return false;
	}
	
	public String intDisponivelToString(AtividadeDisponivel at){
		int disponivel = at.getIsDisponivel();
		String mensagem;
		if(disponivel==1)
			mensagem = "Sim";
		else
			mensagem = "N�o";
		return mensagem;
	}
	
	public List<Integer> anosDaTurmaSelecionada(){
		List<Integer> anos = new ArrayList<Integer>();
		for(Turma t: disciplinasPorTurma.keySet()){
			if(t.getId().getCodigoTurma().equals(codTurma) || Desconsiderar.tipoString.equals(codTurma) || codTurma == null)
				if(!anos.contains(t.getId().getAnoTurma()))
					anos.add(t.getId().getAnoTurma());
		}
		return anos;
	}
	
	public List<Disciplina> disciplinasDaTurmaSelecionada(){
		Turma t = getTurmaSelecionada();
		if(t== null){
			List<Disciplina> todasDisciplinas = new ArrayList<Disciplina>();
			for(Turma turma :disciplinasPorTurma.keySet()){
				todasDisciplinas.addAll(disciplinasPorTurma.get(turma));
			}
			return todasDisciplinas;
		}
		return disciplinasPorTurma.get(t); 
	}
	
	private Turma getTurmaSelecionada(){
		Turma turma = null;
		for(Turma t : disciplinasPorTurma.keySet()){
			if(t.getId().equals(new TurmaID(codTurma, anoTurma))){
				turma = t;
				break;
			}
				
		}
		return turma;
	}
	
	public String getCodAtividade() {
		return codAtividade;
	}
	public void setCodAtividade(String codAtividade) {
		this.codAtividade = codAtividade;
	}
	public String getCodDisciplina() {
		return codDisciplina;
	}
	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}
	public Long getAreaAtividade() {
		return areaAtividade;
	}
	public void setAreaAtividade(Long areaAtividade) {
		this.areaAtividade = areaAtividade;
	}
	public String getCodTurma() {
		return codTurma;
	}
	public void setCodTurma(String codTurma) {
		this.codTurma = codTurma;
	}
	public Integer getAnoTurma() {
		return anoTurma;
	}
	public void setAnoTurma(Integer anoTurma) {
		this.anoTurma = anoTurma;
	}
	public int getDisponivel() {
		return disponivel;
	}
	public void setDisponivel(int disponivel) {
		this.disponivel = disponivel;
	}
	public Date getDataLiberacao() {
		return dataLiberacao;
	}
	public void setDataLiberacao(Date dataLiberacao) {
		this.dataLiberacao = dataLiberacao;
	}
	public Date getDataEncerramento() {
		return dataEncerramento;
	}
	public void setDataEncerramento(Date dataEncerramento) {
		this.dataEncerramento = dataEncerramento;
	}
	public Double getValorAtividade() {
		return valorAtividade;
	}
	public void setValorAtividade(Double valorAtividade) {
		this.valorAtividade = valorAtividade;
	}
	public List<AreaAtividade> getTodasAsAreasDeAtividades() {
		return todasAsAreasDeAtividades;
	}
	public void setTodasAsAreasDeAtividades(List<AreaAtividade> todasAsAreasDeAtividades) {
		this.todasAsAreasDeAtividades = todasAsAreasDeAtividades;
	}

	public List<AtividadeDisponivel> getAtividades() {
		return atividades;
	}

	public void setAtividades(List<AtividadeDisponivel> atividades) {
		this.atividades = atividades;
	}

	public List<AtividadeDisponivel> getAtividadesSelecionadas() {
		return atividadesSelecionadas;
	}

	public void setAtividadesSelecionadas(List<AtividadeDisponivel> atividadesSelecionadas) {
		this.atividadesSelecionadas = atividadesSelecionadas;
	}

	public Map<Turma, List<Disciplina>> getDisciplinasPorTurma() {
		return disciplinasPorTurma;
	}

	public void setDisciplinasPorTurma(Map<Turma, List<Disciplina>> disciplinasPorTurma) {
		this.disciplinasPorTurma = disciplinasPorTurma;
	}
	
	
	
	

}
