package web.src.view;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.pessoa.Pessoa;
import web.src.controller.CadastroPessoaController;
import web.src.entity.NegocioException;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;

@ManagedBean
@ViewScoped
public class CadastroPessoaView extends View{
	
	private static final long serialVersionUID = 1L;
	private Long RA;
	private Date dataNascimento;
	private String email;
	private String nome;
	private String senha;
	private char sexo;
	private int tipoPessoa;
	private boolean atualizando;
	Pessoa pessoa;
	
	private CadastroPessoaController controller;
	
	public CadastroPessoaView() {
		controller = new CadastroPessoaController();
	}
	@PostConstruct
	public void init(){
		pegarObjetosDaSessao();
		if(pessoa != null){
			setAtualizando(true);
			pegarDadosDaPessoa();
		}
			
	}
	
	private void pegarDadosDaPessoa() {
		setNome(pessoa.getNome());
		setEmail(pessoa.getEmail());
		setDataNascimento(pessoa.getDataNasc());
		setSenha(pessoa.getSenha());
		setSexo(pessoa.getSexo().charAt(0));
		RA = pessoa.getRA();
		
	}
	private void pegarObjetosDaSessao(){
		pessoa = (Pessoa) Util.pegarObjetoDaSessaoERemover(SessaoEnum.OBJETO_PESSOA);
		Integer tipo = (Integer) Util.pegarObjetoDaSessaoERemover(SessaoEnum.TIPO_PESSOA);
		if(tipo == null)
			Util.irPaginaAnterior();
		else
			tipoPessoa = tipo;
	}
	
	public void limpar(){
		dataNascimento = null;
		email = "";
		nome = "";
		senha = "";
		sexo = 'M';
		if(!isAtualizando())
			tipoPessoa = 0;
	}
	
	public void cadastrar(){
		try{
			Pessoa p = popularObjetoPessoa();
			String mensagem = controller.gravarPessoa(p, getTipoPessoa());
			limpar();
			Util.exibirMensagemDeSucesso(null, mensagem);
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	public void atualizar(){
		try{
			Pessoa p = popularObjetoPessoa();
			p.setRA(RA);
			String mensagem = controller.atualizarPessoa(p, getTipoPessoa());
			Util.exibirMensagemDeSucesso(null, mensagem);
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	public void voltar(){
		Util.irPaginaAnterior();
	}
	
	private Pessoa popularObjetoPessoa(){
		Pessoa p = new Pessoa();
		p.setDataNasc(getDataNascimento());
		p.setEmail(getEmail());
		p.setNome(getNome());
		p.setSenha(getSenha());
		p.setSexo(String.valueOf(getSexo()));
		return p;
	}
	
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public char getSexo() {
		return sexo;
	}
	public void setSexo(char sexo) {
		this.sexo = sexo;
	}
	public int getTipoPessoa() {
		return tipoPessoa;
	}
	public void setTipoPessoa(int tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}
	public boolean isAtualizando() {
		return atualizando;
	}
	public void setAtualizando(boolean atualizando) {
		this.atualizando = atualizando;
	}

}
