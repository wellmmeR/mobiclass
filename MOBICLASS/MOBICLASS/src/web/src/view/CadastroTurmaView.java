package web.src.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.academico.Curso;
import jpa.academico.Turma;
import web.src.controller.CadastroTurmaController;
import web.src.entity.NegocioException;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;

@ManagedBean
@ViewScoped
public class CadastroTurmaView extends View{

	private static final long serialVersionUID = -7524920036817065306L;
	
	
	private boolean modoEditar;
	private String codTurma;
	private String nomeTurma;
	private Integer anoTurma;
	private String codCurso;
	private List<Curso> cursos;
	private Turma turma;
	
	private CadastroTurmaController controller;
	
	@PostConstruct
	public void init(){
		controller = new CadastroTurmaController();
		obterObjetosDaSessao();
		if(turma!=null){
			modoEditar = true;
			pegarInfoObjeto();
		}
		buscarTodosCursos();
	}
	
	private void buscarTodosCursos() {
		cursos = controller.buscarTodosCursos();
	}

	private void pegarInfoObjeto() {
		codTurma = turma.getId().getCodigoTurma();
		nomeTurma = turma.getNomeTurma();
		anoTurma = turma.getId().getAnoTurma();
		codCurso = turma.getCodCurso();
	}

	public void obterObjetosDaSessao(){
		turma = (Turma)Util.pegarObjetoDaSessaoERemover(SessaoEnum.OBJETO_TURMA);
	}
	
	public void cadastrar(){
		try{
			controller.gravarTurma(codTurma, anoTurma, codCurso, nomeTurma);
			Util.exibirMensagemDeSucesso(null, "Turma Cadastrada");
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro ao tentar Gravar", nEx.getMessage());
		}
	}
	
	public void modificar(){
		try{
			controller.modificarTurma(codTurma, anoTurma, codCurso, nomeTurma);
			Util.exibirMensagemDeSucesso(null, "Turma Alterada");
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro ao tentar Gravar", nEx.getMessage());
		}
	}
	
	public void voltar(){
		Util.irPaginaAnterior();
	}

	public boolean isModoEditar() {
		return modoEditar;
	}

	public void setModoEditar(boolean modoEditar) {
		this.modoEditar = modoEditar;
	}

	public String getCodTurma() {
		return codTurma;
	}

	public void setCodTurma(String codTurma) {
		this.codTurma = codTurma;
	}

	public String getNomeTurma() {
		return nomeTurma;
	}

	public void setNomeTurma(String nomeTurma) {
		this.nomeTurma = nomeTurma;
	}

	public Integer getAnoTurma() {
		return anoTurma;
	}

	public void setAnoTurma(Integer anoTurma) {
		this.anoTurma = anoTurma;
	}

	public String getCodCurso() {
		return codCurso;
	}

	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}

	public List<Curso> getCursos() {
		return cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}
	

}
