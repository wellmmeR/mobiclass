package web.src.view;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.academico.Curso;
import jpa.academico.Disciplina;
import jpa.academico.Turma;
import jpa.academico.TurmaID;
import jpa.atividade.AreaAtividade;
import jpa.pessoa.Aluno;
import jpa.util.Desconsiderar;
import jpa.wrapper.AtividadeWrapper;
import web.src.controller.DetalharAlunoController;
import web.src.entity.NegocioException;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;

@ManagedBean
@ViewScoped
public class DetalharAlunoView extends View{

	private static final long serialVersionUID = 465833812134173835L;
	private static final String NOME_PAGINA = "detalhar-aluno.xhtml";
	
	private boolean viewIniciada;
	private Aluno alunoSelecionado;
	private DetalharAlunoController controller;

	//CURSO
	private List<Curso> cursos;
	private List<Curso> cursosSelecionados;
	private String nomeCurso;
	private String codCurso;
	private Integer cursoDisponivel;
	//CURSO FIM
	
	//TURMA
	private String codTurma;
	private String nomeTurma;
	private Integer anoTurma;
	private List<Integer> TodosAnosTurma;
	private List<Turma> turmas;
	private List<Turma> turmasSelecionadas;
	private String comboCurso;
	private List<Curso> cursosComboBox;
	//TURMA FIM
	
	//DISCIPLINA
	private String nomeDisciplina;
	private String codDisciplina;
	private List<Disciplina> disciplinas;
	private List<Disciplina> disciplinasSelecionadas;
	
	//DISCIPLINA FIM
	
	//ATIVIDADE
	private String nomeAtividade;
	private String codAtividade;
	private String codDisciplinaDaAtividade;
	private String codTurmaDaAtividade;
	private Integer anoTurmaDaAtividade;
	private Long areaAtividadeDaAtividade;
	private List<AtividadeWrapper> atividades;
	private List<AtividadeWrapper> atividadesSelecionadas;
	private List<Turma> turmasAtividades;
	private List<Integer> anosTurmasAtividades;
	private List<Disciplina> disciplinasAtividade;
	private List<AreaAtividade> areasAtividades;
	
	//ATIVIDADE FIM
	
	public DetalharAlunoView() {
		controller = new DetalharAlunoController();
	}
	
	public void init(){
		if(!viewIniciada)
		{
			pegarObjetosDaSessao();
			if(alunoSelecionado != null){
				initCursos();
				initTurmas();
				initDisciplinas();
				initAtividade();
			}
			else{
				Util.redirecionar("gestao-aluno.xhtml");
			}
			viewIniciada = true;
		}
	}

	private void pegarObjetosDaSessao() {
		alunoSelecionado = (Aluno) Util.pegarObjetoDaSessaoERemover(SessaoEnum.OBJETO_ALUNO);
	}

	public void voltar(){
		Util.irPaginaAnterior();
	}
	
/*----------------------------------------------CURSOS----------------------------------------------------*/
	public void initCursos(){
		cursoDisponivel = Desconsiderar.tipoInt;
		pesquisarCursos();
	}
	
	public void pesquisarCursos() {
		try{
			cursos = controller.cursosDoAluno(alunoSelecionado.getRA(), getNomeCurso(), getCodCurso(), getCursoDisponivel());
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	public void detalharCurso(){
		Util.adicionarObjetoNaSessao(SessaoEnum.OBJETO_CURSO, cursosSelecionados.get(0));
		Util.irPara("detalhar-curso.xhtml", NOME_PAGINA);
	}
	
	public void vincularCurso(){
		Util.adicionarObjetoNaSessao(SessaoEnum.OBJETO_VIEW, this);
		Util.irPara("pesquisar-curso.xhtml", NOME_PAGINA);
	}
	
	public void desvincularCurso(){
		try{
			controller.desvincularAlunoDoCurso(alunoSelecionado.getRA(), cursos.get(0).getCodCurso());
			cursos.remove(0);
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	public boolean nenhumCursoSelecionado(){
		if(cursosSelecionados == null || cursosSelecionados.isEmpty())
			return true;
		return false;
	}
	
	public boolean umCursoSelecionado(){
		if(nenhumCursoSelecionado() || cursosSelecionados.size() != 1)
			return false;
		return true;
	}
	
	@Override
	public List<Curso> pesquisarCursosView(Curso c) throws NegocioException{
		try{
			List<Curso> cursosDoAluno = controller.cursosDoAluno(alunoSelecionado.getRA(), c.getNome(), c.getCodCurso(), c.getIsDisponivel());
			List<Curso> cursosPossiveis = controller.buscarTodosCursos();
			Util.removerListaDentroDeLista(cursosPossiveis, cursosDoAluno);
			return cursosPossiveis;
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}
	
	@Override
	public void cursosDaPesquisa(List<Curso> cursos) throws NegocioException{
		try{
			controller.gravarCursosDaPesquisa(cursos, alunoSelecionado.getRA());
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}

/*----------------------------------------------CURSOSFIM----------------------------------------------------*/

/*----------------------------------------------TURMAS----------------------------------------------------*/
	public void initTurmas(){
		instanciarComboBox();
		pesquisarTurmas();
	}
	private void instanciarComboBox() {
		cursosComboBox = new ArrayList<Curso>();
		if(cursos != null)
			cursosComboBox.addAll(cursos);
	}

	public void pesquisarTurmas(){
		try{
			turmas = controller.turmasDoAluno(alunoSelecionado.getRA(), getAnoTurma(), getCodTurma(), getNomeTurma(), getComboCurso());
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	public void detalharTurma(){
		
	}
	
	public void vincularTurma(){
		Util.adicionarObjetoNaSessao(SessaoEnum.OBJETO_VIEW, this);
		Util.irPara("pesquisar-turma.xhtml", NOME_PAGINA);
	}
	
	public void desvincularTurma(){
		try{
			TurmaID id = turmasSelecionadas.get(0).getId();
			controller.desvincularTurmaDoAluno(id.getCodigoTurma(), id.getAnoTurma(), alunoSelecionado.getRA());
			turmasSelecionadas.remove(0);
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro ao desvincular", nEx.getMessage());
		}
	}
	
	public boolean nenhumaTurmaSelecionada(){
		if(turmasSelecionadas == null || turmasSelecionadas.isEmpty())
			return true;
		return false;
	}
	
	public boolean umaTurmaSelecionada(){
		if(nenhumaTurmaSelecionada() || turmasSelecionadas.size() != 1)
			return false;
		return true;
	}
	
	@Override
	public void turmasDaPesquisa(List<Turma> turmas) throws NegocioException {
		try{
			controller.gravarTurmasDaPesquisa(turmas, alunoSelecionado.getRA());
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
		
	}
	
	@Override
	public List<Turma> pesquisarTurmaView(Turma t) throws NegocioException {
		try{
			TurmaID id = t.getId();
			List<Turma> turmasDoAluno = controller.turmasDoAluno(alunoSelecionado.getRA(),id.getAnoTurma() ,id.getCodigoTurma(), t.getNomeTurma(), t.getCodCurso());
			List<Turma> turmasPossiveis = controller.turmasPossiveisDoAluno(alunoSelecionado.getRA());
			Util.removerListaDentroDeLista(turmasPossiveis, turmasDoAluno);
			return turmasPossiveis;
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}
/*----------------------------------------------TURMAS FIM----------------------------------------------------*/
	
/*----------------------------------------------DISCIPLINAS----------------------------------------------------*/
	private void initDisciplinas() {
		pesquisarDisciplina();
		
	}
	
	public void pesquisarDisciplina(){
		try{
			disciplinas = controller.disciplinasDoAluno(alunoSelecionado.getRA(), nomeDisciplina, codDisciplina);
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	public void vincularDisciplina(){
		Util.adicionarObjetoNaSessao(SessaoEnum.OBJETO_VIEW, this);
		Util.irPara("pesquisar-disciplina.xhtml", NOME_PAGINA);
	}
	
	public void desvincularDisciplina(){
		try{
			controller.desvincularDisciplinaDoAluno(disciplinasSelecionadas.get(0).getCodigoDisciplina(), alunoSelecionado.getRA());
			Util.removerListaDentroDeLista(disciplinas, disciplinasSelecionadas);
			disciplinasSelecionadas = null;
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("erro ao desvincular", nEx.getMessage());
		}
	}
	
	public void detalharDisciplina(){
		
	}
	
	@Override
	public List<Disciplina> pesquisarDisciplinaView(Disciplina d) throws NegocioException{
		try{
			List<Disciplina> disciplinasDoAluno = controller.disciplinasDoAluno(alunoSelecionado.getRA(), d.getNome(), d.getCodigoDisciplina());
			List<Disciplina> disciplinasPossiveis = controller.disciplinasPossiveisDoAluno(alunoSelecionado.getRA());
			Util.removerListaDentroDeLista(disciplinasPossiveis, disciplinasDoAluno);
			return disciplinasPossiveis;
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
		
	}
	
	@Override
	public void disciplinasDaPesquisa(List<Disciplina> disciplinas)throws NegocioException {
		try{
			controller.gravarDisciplinasDaPesquisa(disciplinas, alunoSelecionado.getRA());
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}
	
	public boolean nenhumaDisciplinaSelecionada(){
		if(disciplinasSelecionadas == null || disciplinasSelecionadas.isEmpty())
			return true;
		return false;
	}
	
	public boolean umaDisciplinaSelecionada(){
		if(nenhumaDisciplinaSelecionada() || disciplinasSelecionadas.size() != 1)
			return false;
		return true;
	}
	
/*----------------------------------------------DISCIPLINAS FIM----------------------------------------------------*/
	

/*----------------------------------------------ATIVIDADES----------------------------------------------------*/
	
	public void initAtividade(){
		anoTurmaDaAtividade = Desconsiderar.tipoInt;
		areaAtividadeDaAtividade = Desconsiderar.tipoLong;
		montarCombosBox();
		pesquisarAtividade();
	}
	
	private void montarCombosBox() {
		montarComboTurma();
		montarComboAnosTurmas();
		montarComboDisciplina();
	}

	private void montarComboDisciplina() {
		try{
			turmasAtividades = controller.turmasPossiveisDoAluno(alunoSelecionado.getRA());
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro ao montar combo Disciplina", nEx.getMessage());
		}
	}

	private void montarComboTurma() {
		try{
			turmasAtividades = controller.turmasPossiveisDoAluno(alunoSelecionado.getRA());
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro ao montar combo Turma", nEx.getMessage());
		}
		
	}
	
	private void montarComboAnosTurmas(){
		try{
			anosTurmasAtividades = controller.anosPossiveisDaTurma(codTurmaDaAtividade);
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro ao montar combo Ano", nEx.getMessage());
		}
	}

	public void pesquisarAtividade(){
		try{
			atividades = controller.buscarAtividadesDoAluno(alunoSelecionado.getRA(), nomeAtividade, codAtividade, codTurmaDaAtividade, anoTurmaDaAtividade, codDisciplinaDaAtividade, areaAtividadeDaAtividade);
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro na pesquisa de atividades", nEx.getMessage());
		}
	}
	
	public void limparAtividade(){
		
	}
	
	public void detalharAtividade(){
		
	}
	
	public void deletarAtividadeDoAluno(){
		try{
			controller.deletarAtividadesDoAluno(atividadesSelecionadas, alunoSelecionado.getRA());
			Util.removerListaDentroDeLista(atividades, atividadesSelecionadas);
			Util.exibirMensagemDeSucesso(null, "Atividade Deletada");
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro ao deletar", nEx.getMessage());
		}		
	}
	
	public boolean nenhumaAtividadeSelecionada(){
		if(atividadesSelecionadas == null || atividadesSelecionadas.isEmpty())
			return true;
		return false;
	}
	
	public boolean umaAtividadeSelecionada(){
		if(nenhumaAtividadeSelecionada() || atividadesSelecionadas.size() != 1)
			return false;
		return true;
	}
	
/*----------------------------------------------ATIVIDADES FIM----------------------------------------------------*/

	public Aluno getAlunoSelecionado() {
		return alunoSelecionado;
	}

	public void setAlunoSelecionado(Aluno alunoSelecionado) {
		this.alunoSelecionado = alunoSelecionado;
	}

	public String getNomeDisciplina() {
		return nomeDisciplina;
	}

	public void setNomeDisciplina(String nomeDisciplina) {
		this.nomeDisciplina = nomeDisciplina;
	}

	public String getCodDisciplina() {
		return codDisciplina;
	}

	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}


	public List<Turma> getTurmas() {
		return turmas;
	}


	public void setTurmas(List<Turma> turmas) {
		this.turmas = turmas;
	}


	public List<Turma> getTurmasSelecionadas() {
		return turmasSelecionadas;
	}


	public void setTurmasSelecionadas(List<Turma> turmasSelecionadas) {
		this.turmasSelecionadas = turmasSelecionadas;
	}
	
	public String getCodTurma() {
		return codTurma;
	}

	public void setCodTurma(String codTurma) {
		this.codTurma = codTurma;
	}

	public String getNomeTurma() {
		return nomeTurma;
	}

	public void setNomeTurma(String nomeTurma) {
		this.nomeTurma = nomeTurma;
	}

	public Integer getAnoTurma() {
		return anoTurma;
	}

	public void setAnoTurma(Integer anoTurma) {
		this.anoTurma = anoTurma;
	}

	public List<Integer> getTodosAnosTurma() {
		return TodosAnosTurma;
	}

	public void setTodosAnosTurma(List<Integer> todosAnosTurma) {
		TodosAnosTurma = todosAnosTurma;
	}

	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}

	public List<Disciplina> getDisciplinasSelecionadas() {
		return disciplinasSelecionadas;
	}

	public void setDisciplinasSelecionadas(List<Disciplina> disciplinasSelecionadas) {
		this.disciplinasSelecionadas = disciplinasSelecionadas;
	}

	public List<Curso> getCursos() {
		return cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}

	public List<Curso> getCursosComboBox() {
		return cursosComboBox;
	}

	public void setCursosComboBox(List<Curso> cursosComboBox) {
		this.cursosComboBox = cursosComboBox;
	}

	public String getNomeCurso() {
		return nomeCurso;
	}

	public void setNomeCurso(String nomeCurso) {
		this.nomeCurso = nomeCurso;
	}

	public String getCodCurso() {
		return codCurso;
	}

	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}

	public Integer getCursoDisponivel() {
		return cursoDisponivel;
	}

	public void setCursoDisponivel(Integer cursoDisponivel) {
		this.cursoDisponivel = cursoDisponivel;
	}

	public String getComboCurso() {
		return comboCurso;
	}

	public void setComboCurso(String comboCurso) {
		this.comboCurso = comboCurso;
	}

	public List<Curso> getCursosSelecionados() {
		return cursosSelecionados;
	}

	public void setCursosSelecionados(List<Curso> cursosSelecionados) {
		this.cursosSelecionados = cursosSelecionados;
	}

	public String getCodAtividade() {
		return codAtividade;
	}

	public void setCodAtividade(String codAtividade) {
		this.codAtividade = codAtividade;
	}

	public String getCodDisciplinaDaAtividade() {
		return codDisciplinaDaAtividade;
	}

	public void setCodDisciplinaDaAtividade(String codDisciplinaDaAtividade) {
		this.codDisciplinaDaAtividade = codDisciplinaDaAtividade;
	}

	public String getCodTurmaDaAtividade() {
		return codTurmaDaAtividade;
	}

	public void setCodTurmaDaAtividade(String codTurmaDaAtividade) {
		this.codTurmaDaAtividade = codTurmaDaAtividade;
	}

	public Integer getAnoTurmaDaAtividade() {
		return anoTurmaDaAtividade;
	}

	public void setAnoTurmaDaAtividade(Integer anoTurmaDaAtividade) {
		this.anoTurmaDaAtividade = anoTurmaDaAtividade;
	}

	public Long getAreaAtividadeDaAtividade() {
		return areaAtividadeDaAtividade;
	}

	public void setAreaAtividadeDaAtividade(Long areaAtividadeDaAtividade) {
		this.areaAtividadeDaAtividade = areaAtividadeDaAtividade;
	}

	public List<AtividadeWrapper> getAtividades() {
		return atividades;
	}

	public void setAtividades(List<AtividadeWrapper> atividades) {
		this.atividades = atividades;
	}

	public List<AtividadeWrapper> getAtividadesSelecionadas() {
		return atividadesSelecionadas;
	}

	public void setAtividadesSelecionadas(
			List<AtividadeWrapper> atividadesSelecionadas) {
		this.atividadesSelecionadas = atividadesSelecionadas;
	}

	public String getNomeAtividade() {
		return nomeAtividade;
	}

	public void setNomeAtividade(String nomeAtividade) {
		this.nomeAtividade = nomeAtividade;
	}

	public List<Turma> getTurmasAtividades() {
		return turmasAtividades;
	}

	public void setTurmasAtividades(List<Turma> turmasAtividades) {
		this.turmasAtividades = turmasAtividades;
	}

	public List<Integer> getAnosTurmasAtividades() {
		return anosTurmasAtividades;
	}

	public void setAnosTurmasAtividades(List<Integer> anosTurmasAtividades) {
		this.anosTurmasAtividades = anosTurmasAtividades;
	}

	public List<AreaAtividade> getAreasAtividades() {
		return areasAtividades;
	}

	public void setAreasAtividades(List<AreaAtividade> areasAtividades) {
		this.areasAtividades = areasAtividades;
	}

	public List<Disciplina> getDisciplinasAtividade() {
		return disciplinasAtividade;
	}

	public void setDisciplinasAtividade(List<Disciplina> disciplinasAtividade) {
		this.disciplinasAtividade = disciplinasAtividade;
	}
}
