package web.src.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.DAO.relacionamento.TurmaDisciplinaDAO;
import jpa.academico.Disciplina;
import jpa.academico.Turma;
import jpa.atividade.AreaAtividade;
import jpa.atividade.Atividade;
import jpa.atividade.AtividadeDisponivel;
import jpa.util.Desconsiderar;
import web.src.controller.CadastroAtividadeDisponivelController;
import web.src.controller.GestaoAtividadeController;
import web.src.entity.NegocioException;
import web.src.entity.Util;

@ManagedBean
@ViewScoped
public class CadastroAtividadeDisponivelView extends View{

	private static final long serialVersionUID = -8356894759637614725L;
	private static final int DISPONIVEL = 1;
	
	private boolean viewIniciada;
	private String nomeAtividade;
	private String codAtividade;
	private String codDisciplina;
	private List<Disciplina> todasAsDisciplinas;
	private Long areaAtividade;
	private List<AreaAtividade> todasAsAreasDeAtividades;
	
	private List<Atividade> atividades;
	private List<Atividade> atividadesSelecionadas;
	
	private String codTurma;
	private List<Turma> turmas;
	private Integer anoTurma;
	private List<Integer> anosTurmas;
	private Date dataLiberacao;
	private Date dataEncerramento;
	private Double valorAtividade; 
	
	
	private GestaoAtividadeController gestaoAtividadeController;
	private CadastroAtividadeDisponivelController controller;
	
	public CadastroAtividadeDisponivelView() {
		controller = new CadastroAtividadeDisponivelController();
		gestaoAtividadeController = new GestaoAtividadeController();
		todasAsDisciplinas = new ArrayList<Disciplina>();
		todasAsAreasDeAtividades = new ArrayList<AreaAtividade>();
		turmas = new ArrayList<Turma>();
		areaAtividade = Desconsiderar.tipoLong;
	}
	
	public void init(){
		if(!viewIniciada)
		{
			pesquisarTodasDisciplinas();
			pesquisarTodasAreasAtividades();
			pesquisarAtividade();
			viewIniciada = true;
		}
	}
	
	public void cadastrarAtividadeDisponivel(){
		try{
			for(Atividade aSel : atividadesSelecionadas){
				AtividadeDisponivel atDisp = new 
						AtividadeDisponivel(aSel.getIdAtividade().getCodigoAtividade(), aSel.getIdAtividade().getCodigoDisciplina(), 
						codTurma, anoTurma, dataLiberacao, dataEncerramento, DISPONIVEL, aSel.getIdAtividade().getAreaAtividade(), valorAtividade);
				controller.gravarAtividadeDisponivelEGerarAtividadesParaAlunos(atDisp);
				limparCamposCadastro();
			}
			Util.exibirMensagemDeSucesso(null, "Atividades Cadastradas");
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro ao tentar gravar:", nEx.getMessage());
		}
	}
	
	private void pesquisarTodasDisciplinas(){
		todasAsDisciplinas = controller.buscarTodasDisciplinas();
	}
	
	private void pesquisarTodasAreasAtividades(){
		todasAsAreasDeAtividades = controller.buscarTodasAreasAtividades();
	}

	public void pesquisarAtividade(){
		try{
			atividades =  gestaoAtividadeController.buscarAtividades(new Atividade(codAtividade, nomeAtividade, codDisciplina, DISPONIVEL,  areaAtividade));
		}
		catch(Exception ex){
			Util.exibirMensagemDeErro("Erro na Pesquisa", ex.getMessage());
		}
	}
	
	public String nomeAreaAtividade(Long id){
		String nomeAreaAtividade="";
		if(id!=null)
			nomeAreaAtividade = gestaoAtividadeController.getNomeAreaAtividade(id);
		return nomeAreaAtividade;
	}
	
	public String intDisponivelToString(Atividade at){
		int disponivel = at.isDisponivel();
		String mensagem;
		if(disponivel==1)
			mensagem = "Sim";
		else
			mensagem = "N�o";
		return mensagem;
	}
	
	public void atualizarSelectOneAnosTurmas(){
		
		anosTurmas = new ArrayList<Integer>();
		for(Turma t: turmas){
			if(t.getId().getCodigoTurma().equals(codTurma) || Desconsiderar.tipoString.equals(codTurma) || codTurma == null)
				if(!anosTurmas.contains(t.getId().getAnoTurma()))
					anosTurmas.add(t.getId().getAnoTurma());
		}
	}
	
	public void atualizarSelectTurmasEanos(){
		atualizarSelectOneTurmas();
		atualizarSelectOneAnosTurmas();
	}
	
	public void atualizarSelectOneTurmas(){
		TurmaDisciplinaDAO dao = new TurmaDisciplinaDAO();
		
		turmas = new ArrayList<Turma>();
		for(Atividade atividade : atividadesSelecionadas){
			List<Turma> listaAux = dao.buscarTurmasDaDisciplina(null, atividade.getIdAtividade().getCodigoDisciplina());
			for(Turma t: listaAux){
				if(!turmas.contains(t))
					turmas.add(t);
			}
			
		}
	}
	
	public void limparCamposCadastro(){
		valorAtividade = null;
		dataLiberacao = null;
		dataEncerramento = null;
	}
	
	public boolean cadastrarAtividadeDisponivellHabilitado(){
		if(atividadesSelecionadas == null || atividadesSelecionadas.isEmpty())
			return false;
		return true;
	}
	
	public String getCodAtividade() {
		return codAtividade;
	}
	public void setCodAtividade(String codAtividade) {
		this.codAtividade = codAtividade;
	}
	public String getCodDisciplina() {
		return codDisciplina;
	}
	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}
	public List<Disciplina> getTodasAsDisciplinas() {
		return todasAsDisciplinas;
	}
	public void setTodasAsDisciplinas(List<Disciplina> todasAsDisciplinas) {
		this.todasAsDisciplinas = todasAsDisciplinas;
	}
	public Long getAreaAtividade() {
		return areaAtividade;
	}
	public void setAreaAtividade(Long areaAtividade) {
		this.areaAtividade = areaAtividade;
	}
	public List<AreaAtividade> getTodasAsAreasDeAtividades() {
		return todasAsAreasDeAtividades;
	}
	public void setTodasAsAreasDeAtividades(
			List<AreaAtividade> todasAsAreasDeAtividades) {
		this.todasAsAreasDeAtividades = todasAsAreasDeAtividades;
	}
	public String getNomeAtividade() {
		return nomeAtividade;
	}
	public void setNomeAtividade(String nomeAtividade) {
		this.nomeAtividade = nomeAtividade;
	}
	public List<Atividade> getAtividades() {
		return atividades;
	}

	public void setAtividades(List<Atividade> atividades) {
		this.atividades = atividades;
	}

	public List<Atividade> getAtividadesSelecionadas() {
		return atividadesSelecionadas;
	}

	public void setAtividadesSelecionadas(List<Atividade> atividadesSelecionadas) {
		this.atividadesSelecionadas = atividadesSelecionadas;
	}

	public String getCodTurma() {
		return codTurma;
	}

	public void setCodTurma(String codTurma) {
		this.codTurma = codTurma;
	}

	public List<Turma> getTurmas() {
		return turmas;
	}

	public void setTurmas(List<Turma> turmas) {
		this.turmas = turmas;
	}

	public Integer getAnoTurma() {
		return anoTurma;
	}

	public void setAnoTurma(Integer anoTurma) {
		this.anoTurma = anoTurma;
	}

	public List<Integer> getAnosTurmas() {
		return anosTurmas;
	}

	public void setAnosTurmas(List<Integer> anosTurmas) {
		this.anosTurmas = anosTurmas;
	}

	public Date getDataLiberacao() {
		return dataLiberacao;
	}

	public void setDataLiberacao(Date dataLiberacao) {
		this.dataLiberacao = dataLiberacao;
	}

	public Date getDataEncerramento() {
		return dataEncerramento;
	}

	public void setDataEncerramento(Date dataEncerramento) {
		this.dataEncerramento = dataEncerramento;
	}

	public Double getValorAtividade() {
		return valorAtividade;
	}

	public void setValorAtividade(Double valorAtividade) {
		this.valorAtividade = valorAtividade;
	}
	
	
	

}
