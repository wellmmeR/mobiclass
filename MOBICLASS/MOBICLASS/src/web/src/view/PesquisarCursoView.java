package web.src.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.academico.Curso;
import web.src.controller.PesquisaController;
import web.src.entity.NegocioException;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;

@ManagedBean
@ViewScoped
public class PesquisarCursoView extends View{

	private static final long serialVersionUID = 6194483409158168053L;
	private static final int DISPONIVEL = 1; 
	
	private PesquisaController controller;
	private String nomeCurso;
	private String codCurso;
	private List<Curso> cursos;
	private List<Curso> cursosSelecionados;
	private View viewChamadoraDaPesquisa;
	
	public PesquisarCursoView() {
		controller = new PesquisaController();
	}
	
	@PostConstruct
	public void init(){
		pegarObjetosDaSessao();
		if(viewChamadoraDaPesquisa == null)
			Util.irPaginaAnterior();
		pesquisar();
	}
	
	
	public void pegarObjetosDaSessao(){
		viewChamadoraDaPesquisa = (View) Util.pegarObjetoDaSessaoERemover(SessaoEnum.OBJETO_VIEW);
	}
	
	public void pesquisar(){
		try{
			cursos = viewChamadoraDaPesquisa.pesquisarCursosView(new Curso(codCurso, nomeCurso, DISPONIVEL));
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro na Pesquisa", nEx.getMessage());
		}
		
	}
	
	public void concluir(){
		try{
			viewChamadoraDaPesquisa.cursosDaPesquisa(cursosSelecionados);
			Util.irPaginaAnterior();
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro no vinculo", nEx.getMessage());
		}
	}
	
	public String getNomeCurso() {
		return nomeCurso;
	}
	public void setNomeCurso(String nomeCurso) {
		this.nomeCurso = nomeCurso;
	}
	public PesquisaController getController() {
		return controller;
	}
	public void setController(PesquisaController controller) {
		this.controller = controller;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public List<Curso> getCursos() {
		return cursos;
	}
	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}
	public List<Curso> getCursosSelecionados() {
		return cursosSelecionados;
	}
	public void setCursosSelecionados(List<Curso> cursosSelecionados) {
		this.cursosSelecionados = cursosSelecionados;
	}

}
