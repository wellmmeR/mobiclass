package web.src.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.academico.Curso;
import jpa.util.Desconsiderar;
import web.src.controller.GestaoCursoController;
import web.src.entity.NegocioException;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;

@ManagedBean
@ViewScoped
public class GestaoCursoView extends View {
	private static final long serialVersionUID = -4167572163471222035L;
	private static final String NOME_PAGINA = "gestao-curso.xhtml";
	
	private GestaoCursoController controller;
	private List<Curso> cursos;
	private Curso cursoSelecionado;
	
	private String nomeCurso;
	private String codigoCurso;
	private Integer cursoDisponivel;
	
	public GestaoCursoView(){
		controller = new GestaoCursoController();
	}
	
	@PostConstruct
	public void iniciar(){
		try{
			cursoDisponivel = Desconsiderar.tipoInt;
			cursos = controller.buscarCursos(null, null, null); //busca todos os cursos
		}
		catch(NegocioException nEx){
			Util.exibirMensagem(nEx.getTitulo(), nEx.getMessage(), nEx.getTipo());
		}
		
	}
	
	public void cadastrarCurso(){
		redirecionarPaginaCadastro();
	}
	
	public void modificarCurso(){
		Util.adicionarObjetoNaSessao(SessaoEnum.OBJETO_CURSO, cursoSelecionado);
		redirecionarPaginaCadastro();
	}
	
	private void redirecionarPaginaCadastro(){
		Util.irPara("cadastro-curso.xhtml", NOME_PAGINA);
	}
	
	public void deletarCurso(){
		try{
			controller.deletarCurso(cursoSelecionado);
			cursos.remove(cursoSelecionado);
			cursoSelecionado = null;
		}
		catch(NegocioException nEx){
			nEx.printStackTrace();
			System.out.println(nEx.getMessage());
		}
	}
	
	public void detalharCurso(){
		Util.adicionarObjetoNaSessao(SessaoEnum.OBJETO_CURSO, cursoSelecionado);
		Util.irPara("detalhar-curso.xhtml", NOME_PAGINA);
	}
	
	public void pesquisar(){
		try{
			cursos = controller.buscarCursos(getNomeCurso(), getCodigoCurso(), getCursoDisponivel());
			cursoSelecionado = null;
			if(cursos.isEmpty())
				Util.exibirMensagemDeAviso(null, "Nenhum Curso Encontrado");
		}
		catch(NegocioException nEx){
			Util.exibirMensagem(nEx.getTitulo(), nEx.getMessage(), nEx.getTipo());
		}
	}
	
	public void limparFiltros(){
		nomeCurso = null;
		codigoCurso = null;
		cursoDisponivel = 2;
	}
	
	public boolean cursoEstaSelecionado(){
		if(cursoSelecionado==null)
			return false;
		return true;
	}
	public List<Curso> getCursos() {
		return cursos;
	}
	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}
	public Curso getCursoSelecionado() {
		return cursoSelecionado;
	}
	public void setCursoSelecionado(Curso cursoSelecionado) {
		this.cursoSelecionado = cursoSelecionado;
	}
	public String getNomeCurso() {
		return nomeCurso;
	}
	public void setNomeCurso(String nomeCurso) {
		this.nomeCurso = nomeCurso;
	}
	public String getCodigoCurso() {
		return codigoCurso;
	}
	public void setCodigoCurso(String codigoCurso) {
		this.codigoCurso = codigoCurso;
	}
	public Integer getCursoDisponivel() {
		return cursoDisponivel;
	}
	public void setCursoDisponivel(Integer cursoDisponivel) {
		this.cursoDisponivel = cursoDisponivel;
	}
	


}
