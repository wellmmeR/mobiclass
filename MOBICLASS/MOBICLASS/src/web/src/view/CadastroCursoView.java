package web.src.view;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.academico.Curso;
import web.src.controller.CadastroCursoController;
import web.src.entity.NegocioException;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;

@ManagedBean
@ViewScoped
public class CadastroCursoView extends View {

	
	private static final long serialVersionUID = 1L;
	
	private String cod;
	private String nome;
	private boolean disponivel;
	private boolean atualizando = false;
	CadastroCursoController controller;
	public CadastroCursoView() {
		controller = new CadastroCursoController();
	}
	@PostConstruct
	public void init(){
		Curso curso = (Curso)Util.pegarObjetoDaSessaoERemover(SessaoEnum.OBJETO_CURSO);
		if(curso != null){
			modoAtualizar(curso);
		}
	}
	
	private void modoAtualizar(Curso curso){
		this.cod = curso.getCodCurso();
		this.nome = curso.getNome();
		this.disponivel = Util.inteiroParaBoolean(curso.getIsDisponivel());
		setAtualizando(true);
	}
	
	public void limpar(){
		cod = "";
		nome = "";
		disponivel = true;
	}
	
	public void cadastrar(){
		try{
			String mensagem = controller.gravarCurso(getCod(), getNome(), isDisponivel());
			Util.exibirMensagemDeSucesso(null, mensagem);
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
		
	}
	
	public void atualizar(){
		try{
			String mensagem = controller.atualizarCurso(getCod(), getNome(), isDisponivel());
			Util.exibirMensagemDeSucesso(null, mensagem);
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
		
	}
	
	public void voltar(){
		Util.irPaginaAnterior();
	}
	
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public boolean isDisponivel() {
		return disponivel;
	}
	public void setDisponivel(boolean disponivel) {
		this.disponivel = disponivel;
	}
	public boolean isAtualizando() {
		return atualizando;
	}
	public void setAtualizando(boolean atualizando) {
		this.atualizando = atualizando;
	} 

}
