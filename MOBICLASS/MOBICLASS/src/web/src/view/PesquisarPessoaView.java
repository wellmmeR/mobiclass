package web.src.view;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.pessoa.Pessoa;
import web.src.controller.PesquisaController;
import web.src.entity.NegocioException;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;
@ManagedBean
@ViewScoped
public class PesquisarPessoaView extends View{

	private static final long serialVersionUID = 8658340744279961864L;
	public static final String MODO_PESSOA_SEM_CURSO = "semCurso";
	
	private Long raPessoa;
	private String nomePessoa;
	private String emailPessoa;
	private Date dataNascimento;
	private String sexoPessoa;
	private boolean isProfessor;
	private List<Pessoa> pessoas;
	private List<Pessoa> pessoasSelecionadas;
	private PesquisaController controller;
	private String modo;
	
	public PesquisarPessoaView() {
		controller = new PesquisaController();
	}
	@PostConstruct
	public void init(){
		modo = MODO_PESSOA_SEM_CURSO;
		pesquisar();
	}
	
	public void pesquisar(){
		try{
			if(MODO_PESSOA_SEM_CURSO.equals(modo))
				pessoas = controller.procurarPessoasSemTurma(getRaPessoa(),getNomePessoa(), getEmailPessoa(), getDataNascimento(), getSexoPessoa(), isProfessor());
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}

	public void concluir(){
		if(pessoas == null || pessoas.isEmpty())
			redirecionar();
		else if(pessoasSelecionadas == null || pessoasSelecionadas.isEmpty())
			Util.exibirMensagemDeErro(null, "Nenhum " + sujeito() + " selecionado");
		else
			redirecionar();
	}
	
	private void redirecionar() {
		Util.adicionarObjetoNaSessao(SessaoEnum.LISTA_PESSOA, pessoasSelecionadas);
		Util.irPaginaAnterior();
	}
	public void limpar(){
		raPessoa = null;
		nomePessoa = "";
		emailPessoa = "";
		dataNascimento = null;
		sexoPessoa = "M";
	}
	
	public String sujeito(){
		if(isProfessor)
			return "Professor";
		else
			return "Aluno";
	}
	
	public boolean nenhumSelecionado(){
		if(pessoasSelecionadas == null || pessoasSelecionadas.isEmpty())
			return true;
		return false;
	}
	
	public String getNomePessoa() {
		return nomePessoa;
	}
	public void setNomePessoa(String nomePessoa) {
		this.nomePessoa = nomePessoa;
	}
	public String getEmailPessoa() {
		return emailPessoa;
	}
	public void setEmailPessoa(String emailPessoa) {
		this.emailPessoa = emailPessoa;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public String getSexoPessoa() {
		return sexoPessoa;
	}
	public void setSexoPessoa(String sexoPessoa) {
		this.sexoPessoa = sexoPessoa;
	}
	public boolean isProfessor() {
		return isProfessor;
	}
	public void setProfessor(boolean isProfessor) {
		this.isProfessor = isProfessor;
	}
	public List<Pessoa> getPessoas() {
		return pessoas;
	}
	public void setPessoas(List<Pessoa> pessoas) {
		this.pessoas = pessoas;
	}
	public Long getRaPessoa() {
		return raPessoa;
	}
	public void setRaPessoa(Long raPessoa) {
		this.raPessoa = raPessoa;
	}
	public List<Pessoa> getPessoasSelecionadas() {
		return pessoasSelecionadas;
	}
	public void setPessoasSelecionadas(List<Pessoa> pessoasSelecionadas) {
		this.pessoasSelecionadas = pessoasSelecionadas;
	}

}
