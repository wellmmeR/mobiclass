package web.src.view;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.UploadedFile;

import web.src.controller.ImportarDadosController;
import web.src.entity.NegocioException;
import web.src.entity.Util;

@ManagedBean
@ViewScoped
public class ImportarDadosView extends View{

	private static final long serialVersionUID = 4708151713010122552L;
	private ImportarDadosController controller;
	private UploadedFile file;
	private String caminho;
	private String nomeArquivo;
	private String formatoArquivo;
	private Integer tipoImportado;
	
	@PostConstruct
	public void init(){
		tipoImportado = 0;
		controller = new ImportarDadosController();
	}
	
	public void importarArquivo(){
		try{
			controller.importar(file, tipoImportado);
			Util.exibirMensagemDeSucesso(null, "Dados Importados");
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(null, "Erro ao Importar: " + nEx.getMessage());
		}
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public String getCaminho() {
		return caminho;
	}

	public void setCaminho(String caminho) {
		this.caminho = caminho;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}

	public String getFormatoArquivo() {
		return formatoArquivo;
	}

	public void setFormatoArquivo(String formatoArquivo) {
		this.formatoArquivo = formatoArquivo;
	}

	public Integer getTipoImportado() {
		return tipoImportado;
	}

	public void setTipoImportado(Integer tipoImportado) {
		this.tipoImportado = tipoImportado;
	}

}
