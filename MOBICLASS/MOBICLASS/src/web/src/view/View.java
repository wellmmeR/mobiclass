package web.src.view;

import java.io.Serializable;
import java.util.List;

import jpa.academico.Curso;
import jpa.academico.Disciplina;
import jpa.academico.Turma;
import web.src.entity.NegocioException;

public class View implements Serializable {

	private static final long serialVersionUID = -3039003493012267053L;
	
	public void cursosDaPesquisa(List<Curso> cursos) throws NegocioException{}
	public List<Curso> pesquisarCursosView(Curso c) throws NegocioException{return null;}
	
	public void turmasDaPesquisa(List<Turma> turmas) throws NegocioException{}
	public List<Turma> pesquisarTurmaView(Turma t) throws NegocioException{return null;}
	
	public void disciplinasDaPesquisa(List<Disciplina> disciplinas) throws NegocioException{}
	public List<Disciplina> pesquisarDisciplinaView(Disciplina d) throws NegocioException{return null;}

}
