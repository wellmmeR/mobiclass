package web.src.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.academico.Curso;
import jpa.academico.Turma;
import jpa.util.Desconsiderar;
import web.src.controller.PesquisaController;
import web.src.entity.NegocioException;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;

@ManagedBean
@ViewScoped
public class PesquisarTurmaView extends View{

	private static final long serialVersionUID = 139915740983747632L;
	
	private List<Turma> turmas;
	private List<Turma> turmasSelecionadas;
	private String codTurma;
	private String nomeTurma;
	private Integer anoTurma;
	private List<Integer> todosAnosDasTurmas;
	private List<Curso> cursosComboBox;
	private String comboCurso;
	private PesquisaController controller;
	private View viewChamadoraDaPesquisa;
	
	public PesquisarTurmaView() {
		controller = new PesquisaController();
	}
	
	@PostConstruct
	public void init(){
		pegarObjetosDaSessao();
		if(viewChamadoraDaPesquisa == null)
			Util.irPaginaAnterior();
		anoTurma = Desconsiderar.tipoInt;
		turmas = controller.buscarTodasTurmas();
		todosAnosDasTurmas = anosDasTurmasPesquisadas(); 
		pesquisar();
		
	}
	
	private List<Integer> anosDasTurmasPesquisadas(){
		List<Integer> anos = new ArrayList<Integer>();
		for(Turma t : turmas){
			if(!anos.contains(t.getId().getAnoTurma())){
				anos.add(t.getId().getAnoTurma());
			}
		}
		return anos;
	}
	
	public void pegarObjetosDaSessao(){
		viewChamadoraDaPesquisa = (View) Util.pegarObjetoDaSessaoERemover(SessaoEnum.OBJETO_VIEW);
	}
	
	public void pesquisar(){
		try{
			turmas = viewChamadoraDaPesquisa.pesquisarTurmaView(new Turma(codTurma, nomeTurma, anoTurma, comboCurso));
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro na pesquisa", nEx.getMessage());
		}
			
	}
	
	public void concluir(){
		try{
			viewChamadoraDaPesquisa.turmasDaPesquisa(turmasSelecionadas);
			Util.irPaginaAnterior();
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro no vinculo ", nEx.getMessage());
		}
	}
	
	public String getCodTurma() {
		return codTurma;
	}
	public void setCodTurma(String codTurma) {
		this.codTurma = codTurma;
	}
	public String getNomeTurma() {
		return nomeTurma;
	}
	public void setNomeTurma(String nomeTurma) {
		this.nomeTurma = nomeTurma;
	}
	public Integer getAnoTurma() {
		return anoTurma;
	}
	public void setAnoTurma(Integer anoTurma) {
		this.anoTurma = anoTurma;
	}
	public List<Integer> getTodosAnosDasTurmas() {
		return todosAnosDasTurmas;
	}
	public void setTodosAnosDasTurmas(List<Integer> todosAnosDasTurmas) {
		this.todosAnosDasTurmas = todosAnosDasTurmas;
	}
	public List<Curso> getCursosComboBox() {
		return cursosComboBox;
	}
	public void setCursosComboBox(List<Curso> cursosComboBox) {
		this.cursosComboBox = cursosComboBox;
	}
	public String getComboCurso() {
		return comboCurso;
	}
	public void setComboCurso(String comboCurso) {
		this.comboCurso = comboCurso;
	}

	public List<Turma> getTurmasSelecionadas() {
		return turmasSelecionadas;
	}

	public void setTurmasSelecionadas(List<Turma> turmasSelecionadas) {
		this.turmasSelecionadas = turmasSelecionadas;
	}

	public List<Turma> getTurmas() {
		return turmas;
	}

	public void setTurmas(List<Turma> turmas) {
		this.turmas = turmas;
	}

}
