package web.src.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import web.src.entity.NegocioException;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;
import jpa.academico.Disciplina;

@ManagedBean
@ViewScoped
public class PesquisarDisciplinaView extends View{

	private static final long serialVersionUID = 7430330350667963459L;
	
	private String nomeDisciplina;
	private String codDisciplina;
	private List<Disciplina> disciplinas;
	private List<Disciplina> disciplinasSelecionadas;
	
	private View viewChamadoraDaPesquisa;
	
	@PostConstruct
	public void init(){
		obterObjetosDaSessao();
		if(viewChamadoraDaPesquisa != null)
		{
			pesquisar();
		}
		else{
			Util.irPaginaAnterior();
		}
		
	}
	
	
	private void obterObjetosDaSessao() {
		viewChamadoraDaPesquisa = (View) Util.pegarObjetoDaSessaoERemover(SessaoEnum.OBJETO_VIEW);
	}


	public void pesquisar(){
		try{
			disciplinas = viewChamadoraDaPesquisa.pesquisarDisciplinaView(new Disciplina(codDisciplina, nomeDisciplina));
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro na pesquisa", nEx.getMessage());
		}
	}
	
	public void concluir(){
		try{
			viewChamadoraDaPesquisa.disciplinasDaPesquisa(disciplinasSelecionadas);
			Util.irPaginaAnterior();
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro no vinculo", nEx.getMessage());
		}
	}
	
	
	public String getNomeDisciplina() {
		return nomeDisciplina;
	}
	public void setNomeDisciplina(String nomeDisciplina) {
		this.nomeDisciplina = nomeDisciplina;
	}
	public String getCodDisciplina() {
		return codDisciplina;
	}
	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}

	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}

	public List<Disciplina> getDisciplinasSelecionadas() {
		return disciplinasSelecionadas;
	}

	public void setDisciplinasSelecionadas(List<Disciplina> disciplinasSelecionadas) {
		this.disciplinasSelecionadas = disciplinasSelecionadas;
	}

}
