package web.src.controller;

import jpa.DAO.AreaAtividadeDAO;
import jpa.atividade.AreaAtividade;
import web.src.entity.NegocioException;

public class CadastroAreaAtividadeController extends Controller {

	private static final long serialVersionUID = 1L;
	private String mensagem;

	public String gravarAreaAtividade(String nome)
			throws NegocioException {

		try{
			AreaAtividade a = new AreaAtividade(nome);
			AreaAtividadeDAO aDao = new AreaAtividadeDAO();
			if (campoNomePreenchido(a)) {
				aDao.adicionar(a);
				return "Area de Atividade " + a.getAreaAtividade()
						+ " criada com sucesso";
			} else {
				throw new NegocioException(mensagem);
			}
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}

	}

	private boolean campoNomePreenchido(AreaAtividade a) {
		mensagem = "Preencher Campo Obrigatorio";
		if (a == null)
			return false;
		if (a.getAreaAtividade() == null || a.getAreaAtividade().trim().isEmpty())
			return false;

		return true;
	}
}
