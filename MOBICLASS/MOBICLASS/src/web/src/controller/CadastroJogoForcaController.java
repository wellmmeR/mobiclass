package web.src.controller;

import jpa.DAO.JogoForcaDAO;
import jpa.atividade.JogoForca;
import jpa.util.Desconsiderar;
import web.src.entity.NegocioException;

public class CadastroJogoForcaController extends Controller{

	private static final long serialVersionUID = 8627521165184808407L;
	
	
	
	public String cadastrarJogoForca(String codAtividade, String codDisciplina, Long codArea, int disponivel, String perguntaForca, String palavra, String dica, int numTentativas)throws NegocioException{
		try{
			JogoForca jg = new JogoForca(codAtividade, codDisciplina, codArea, 1, perguntaForca, palavra, dica, numTentativas);
			validarJogoForca(jg);
			JogoForcaDAO dao = new JogoForcaDAO();
			dao.adicionar(jg);
			return "Jogo gravado com sucesso";
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
		
	}	
	
	private void validarJogoForca(JogoForca jg)throws Exception{
		
		if(jg.getCodAtividade() == null || jg.getCodAtividade().equals(Desconsiderar.tipoString))
			throw new Exception("C�digo da Atividade nulo");
		
		if(jg.getCodDisciplina() == null || jg.getCodDisciplina().equals(Desconsiderar.tipoString))
			throw new Exception("C�digo da Disciplina nulo");
		
		if(jg.getAreaAtividade() == Desconsiderar.tipoLong)
			throw new Exception("C�digo da area da atividade nulo");
		
		if(jg.getDica() == null || jg.getDica().equals(Desconsiderar.tipoString))
			throw new Exception("Informe uma Dica");
		
		if(jg.getNumTentativas() <= 0)
			throw new Exception("N�mero de tentativas invalido");
		
		if(jg.getPalavra() == null || jg.getPalavra().equals(Desconsiderar.tipoString))
			throw new Exception("Informe a Palavra");
		
		if(jg.getPergunta() == null || jg.getPergunta().equals(Desconsiderar.tipoString))
			throw new Exception("Informe uma Pergunta");
		
		if(jg.getPalavra().length() > 15)
			throw new Exception("Palavra muito grande. M�ximo permitido 15 caracteres");
	}

}
