package web.src.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import jpa.DAO.AlunoDAO;
import jpa.DAO.AtividadeDAO;
import jpa.DAO.PontuacaoDAO;
import jpa.DAO.TurmaDAO;
import jpa.DAO.relacionamento.AlunoAtividadeDAO;
import jpa.DAO.relacionamento.AlunoCursoDAO;
import jpa.DAO.relacionamento.AlunoDisciplinaTurmaDAO;
import jpa.DAO.relacionamento.AlunoTurmaDAO;
import jpa.DAO.relacionamento.AtividadeDisponivelDAO;
import jpa.DAO.relacionamento.TurmaDisciplinaDAO;
import jpa.academico.AlunoCurso;
import jpa.academico.AlunoCursoID;
import jpa.academico.AlunoDisciplinaTurma;
import jpa.academico.AlunoTurma;
import jpa.academico.AlunoTurmaID;
import jpa.academico.Curso;
import jpa.academico.Disciplina;
import jpa.academico.Turma;
import jpa.academico.TurmaID;
import jpa.atividade.Atividade;
import jpa.atividade.AtividadeDisponivel;
import jpa.atividade.AtividadeDisponivelID;
import jpa.atividade.AtividadeExecutada;
import jpa.atividade.AtividadeExecutadaID;
import jpa.atividade.AtividadeID;
import jpa.atividade.Pontuacao;
import jpa.atividade.PontuacaoID;
import jpa.util.Desconsiderar;
import jpa.wrapper.AtividadeWrapper;
import web.src.entity.NegocioException;

public class DetalharAlunoController extends Controller{

	private static final long serialVersionUID = 6759081478591814159L;
	
	public List<Curso> cursosDoAluno(Long raAluno, String nomeCurso, String codCurso, Integer disponivel) throws NegocioException{
		try{
			AlunoCursoDAO alDao = new AlunoCursoDAO();
			List<Curso> cursos =alDao.buscarCursosPorAluno(new Curso(codCurso,nomeCurso,disponivel), raAluno);
			return cursos;
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			throw new NegocioException(ex.getMessage());
		}
	}
	
	public void gravarCursosDaPesquisa(List<Curso> cursos, Long raAluno) throws NegocioException{
		try{
			if(cursos != null && raAluno != null)
			{
				AlunoCursoDAO acDao = new AlunoCursoDAO();
				Calendar c = Calendar.getInstance();
				List<AlunoCurso> alunosDoCurso = new ArrayList<AlunoCurso>();
				for(Curso curso: cursos)
					alunosDoCurso.add(new AlunoCurso(curso.getCodCurso(),c.get(Calendar.YEAR), raAluno));
				acDao.adicionar(alunosDoCurso);
				cursos = null;
			}
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}
	
	public void desvincularAlunoDoCurso(Long ra, String codCurso) throws NegocioException{
		try{
			AlunoCursoDAO acDao = new AlunoCursoDAO();
			acDao.deletarPorId(new AlunoCursoID(codCurso, ra));
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}
	
	///////////////////////////////////////////////////////
	
	public List<Turma> turmasDoAluno(Long raAluno, Integer anoTurma, String codTurma, String nomeTurma, String codCurso) throws NegocioException{
		try{
			if(anoTurma == null)
				anoTurma = Desconsiderar.tipoInt;
			AlunoTurmaDAO atDao = new AlunoTurmaDAO();
			List<Turma> turmas = atDao.consultarTurmasDoAluno(raAluno, new Turma(codTurma, nomeTurma, anoTurma, codCurso));
			return turmas;
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}
	
	public List<Turma> turmasPossiveisDoAluno(Long raAluno) throws NegocioException{
		try{
			AlunoDAO dao = new AlunoDAO();
			TurmaDAO tDao = new TurmaDAO();
			List<Curso> cursosDoAluno = dao.consultarTodosCursosDoAluno(raAluno);
			List<Turma> turmasPossiveis = new ArrayList<Turma>();
			for(Curso c : cursosDoAluno){
				turmasPossiveis.addAll(tDao.consultarTurmas(new Turma(null, null, Desconsiderar.tipoInt, c.getCodCurso())));
			}
			return turmasPossiveis;
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}
	
	public List<Integer> anosPossiveisDaTurma(String codTurma) throws NegocioException{
		try{
			TurmaDAO tDao = new TurmaDAO();
			List<Integer> possiveis = new ArrayList<Integer>();
			for(Turma t : tDao.consultarTurmas(new Turma(codTurma, null, Desconsiderar.tipoInt, null))){
				if(!possiveis.contains(t.getId().getAnoTurma()))
					possiveis.add(t.getId().getAnoTurma());
			}
			return possiveis;
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}
	
	public List<Disciplina> disciplinasPossiveisDaTurma(TurmaID id) throws NegocioException{
		try{
			TurmaDisciplinaDAO dao = new TurmaDisciplinaDAO();
			return dao.buscarDisciplinasDaTurma(null, id.getCodigoTurma(), id.getAnoTurma());
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}
	
	public void gravarTurmasDaPesquisa(List<Turma> turmas, Long raAluno) throws NegocioException{
		try{
			if(turmas != null && raAluno != null)
			{
				AlunoTurmaDAO atDao = new AlunoTurmaDAO();
				List<AlunoTurma> listaDeAlunoTurma = new ArrayList<AlunoTurma>();
				for(Turma turma: turmas)
				{
					listaDeAlunoTurma.add(new AlunoTurma(raAluno, turma.getId().getCodigoTurma(), turma.getId().getAnoTurma()));
				}
				atDao.adicionar(listaDeAlunoTurma);
			}
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}
	
	public void desvincularTurmaDoAluno(String codTurma, Integer ano, Long ra) throws NegocioException{
		try{
			AlunoTurmaDAO dao = new AlunoTurmaDAO();
			List<AlunoTurma> lista = dao.consultarAlunoTurma(new AlunoTurma(ra, codTurma, ano));
			dao.deletarPorId(lista);
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	
	public List<Disciplina> disciplinasDoAluno(Long raAluno, String nomeDisciplina, String codDisciplina) throws NegocioException{
		try{
			AlunoDisciplinaTurmaDAO adtDao = new AlunoDisciplinaTurmaDAO();
			return adtDao.buscarDisciplinasDoAluno(new AlunoDisciplinaTurma(raAluno, codDisciplina, null, Desconsiderar.tipoInt, Desconsiderar.tipoInt), new Disciplina(codDisciplina, nomeDisciplina));
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			throw new NegocioException(ex.getMessage());
		}
	}
	
	public List<Disciplina> disciplinasPossiveisDoAluno(Long raAluno) throws NegocioException{
		try{
			TurmaDisciplinaDAO tdDao = new TurmaDisciplinaDAO();
			List<Turma> turmasDoAluno = new AlunoDAO().consultarTodasTurmasDoAluno(raAluno);
			List<Disciplina> disciplinasPossiveis = new ArrayList<Disciplina>();
			for(Turma t: turmasDoAluno){
				for(Disciplina d : tdDao.buscarDisciplinasDaTurma(null, t.getId().getCodigoTurma(), t.getId().getAnoTurma()))
					if(!disciplinasPossiveis.contains(d))
						disciplinasPossiveis.add(d);
			}
			return disciplinasPossiveis;
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			throw new NegocioException(ex.getMessage());
		}
	}
	
	public void gravarDisciplinasDaPesquisa(List<Disciplina> disciplinas, Long ra) throws NegocioException{
		try{
			AlunoTurmaDAO atDAO = new AlunoTurmaDAO();
			TurmaDisciplinaDAO dao = new TurmaDisciplinaDAO();
			AlunoDisciplinaTurmaDAO adtDao = new AlunoDisciplinaTurmaDAO();
			List<AlunoDisciplinaTurma> alunosDisciplinasTurmas = new ArrayList<AlunoDisciplinaTurma>();
			for(Disciplina d : disciplinas){
				for(Turma t :dao.buscarTurmasDaDisciplina(null, d.getCodigoDisciplina())){
					AlunoTurma at = atDAO.consultarPorId(new AlunoTurmaID(ra, t.getId().getAnoTurma(), t.getId().getCodigoTurma()));
					if(at != null){
						alunosDisciplinasTurmas.add(new AlunoDisciplinaTurma(ra, d.getCodigoDisciplina(), t.getId().getCodigoTurma(), t.getId().getAnoTurma(), 0));
					}
				}
			}
			adtDao.adicionar(alunosDisciplinasTurmas);
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			throw new NegocioException(ex.getMessage());
		}
	}
	
	public void desvincularDisciplinaDoAluno(String codDisciplina, Long ra) throws NegocioException{
		try{
			AlunoDisciplinaTurmaDAO dao = new AlunoDisciplinaTurmaDAO();
			List<AlunoDisciplinaTurma> lista = dao.buscarAlunoDisciplinaTurma(new AlunoDisciplinaTurma(ra, codDisciplina, null, Desconsiderar.tipoInt, Desconsiderar.tipoInt));
			dao.deletarPorId(lista);
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}

	//////////////////////////////////////////////////////////////////////////
	
	public List<AtividadeWrapper> buscarAtividadesDoAluno(long raAluno, String nomeAtividade, String codAtividade, String codTurma, int ano, String codDisciplina, long areaAtividade) throws NegocioException{
		try{
			AtividadeDAO atDao = new AtividadeDAO();
			AlunoAtividadeDAO aaDao = new AlunoAtividadeDAO();
			AtividadeDisponivelDAO atDDao = new AtividadeDisponivelDAO();
			PontuacaoDAO pDao = new PontuacaoDAO();
			List<AtividadeWrapper> retorno = new ArrayList<AtividadeWrapper>();
			List<AtividadeExecutada> lista = aaDao.buscarAtividadesExecutadas(new AtividadeExecutada(codTurma, ano, raAluno, codAtividade, codDisciplina, areaAtividade, Desconsiderar.tipoInt));
			for(AtividadeExecutada a : lista){
				Atividade at = atDao.consultarPorId(new AtividadeID(a.getId().getCodAtividade(), a.getId().getCodDisciplina(), a.getId().getAreaAtividade()));
				AtividadeDisponivel atDisp = atDDao.consultarPorId(new AtividadeDisponivelID(a.getId().getCodAtividade(), a.getId().getCodDisciplina(), a.getId().getCodTurma(), a.getId().getAnoTurma(), a.getId().getAreaAtividade()));
				Pontuacao p = pDao.consultarPorId(new PontuacaoID(raAluno, a.getId().getCodAtividade(), a.getId().getCodDisciplina(), a.getId().getAnoTurma(), a.getId().getCodTurma(), a.getId().getAreaAtividade()));
				retorno.add(montarWrapper(a, at, atDisp, p));
			}
			return retorno;
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}
	
	public void deletarAtividadesDoAluno(List<AtividadeWrapper> lista, Long raALuno) throws NegocioException{
		try{
			AlunoAtividadeDAO aaDao = new AlunoAtividadeDAO();
			for(AtividadeWrapper a : lista){
				aaDao.deletarPorId(new AtividadeExecutadaID(raALuno, a.getCodAtividade(), a.getCodDisciplina(), a.getCodTurma(), a.getAnoTurma(), a.getAreaAtividade()));
			}
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}

	private AtividadeWrapper montarWrapper(AtividadeExecutada a, Atividade at, AtividadeDisponivel atDisp, Pontuacao p) {
		AtividadeWrapper w = new AtividadeWrapper();
		
		w.setCodAtividade(a.getId().getCodAtividade());
		w.setCodDisciplina(a.getId().getCodDisciplina());
		w.setCodTurma(a.getId().getCodTurma());
		w.setAnoTurma(a.getId().getAnoTurma());
		w.setAreaAtividade(a.getId().getAreaAtividade());
		w.setDataAlunoComecou(a.getDataInicio());
		w.setDataAlunoTerminou(a.getDataFim());
		w.setFinalizada(a.getIsFinalizado());
		
		if(at != null)
		{
			w.setNomeAtividade(at.getNome());
		}
		
		if(atDisp != null)
		{
			w.setDataAbertura(atDisp.getDataLiberacao());
			w.setDataEncerramento(atDisp.getDataEncerramento());
			w.setValorTotalAtividade(atDisp.getValorAtividade());
		}
		
		if(p != null)
			w.setNotaDoAluno(p.getPontos());
		
		return w;
	}

}
