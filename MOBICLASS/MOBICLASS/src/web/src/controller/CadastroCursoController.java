package web.src.controller;

import jpa.DAO.CursoDAO;
import jpa.academico.Curso;
import web.src.entity.NegocioException;
import web.src.entity.Util;

public class CadastroCursoController extends Controller {

	private static final long serialVersionUID = 1L;
	
	private String mensagem;
	
	public String gravarCurso(String cod,String nome, boolean disponivel) throws NegocioException{
		
		try{
			Curso c = new Curso(cod, nome, Util.booleanParaInteiro(disponivel));
			CursoDAO cDao = new CursoDAO();
			if(camposObrigatorioPreenchido(c) && !existeId(c.getCodCurso())){		
				cDao.adicionar(c);
				return "Curso " + c.getCodCurso() + " criado com sucesso";
			}
			else{
				throw new NegocioException(mensagem);
			}
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	public String atualizarCurso(String cod,String nome, boolean disponivel) throws NegocioException{
		try{
			Curso c = new Curso(cod, nome, Util.booleanParaInteiro(disponivel));
			CursoDAO cDao = new CursoDAO();
			if(camposObrigatorioPreenchido(c)){
				cDao.atualizar(c);
				return "Curso " + c.getCodCurso() + " modificado com sucesso";
			}
			else{
				throw new NegocioException(mensagem);
			}
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	private boolean camposObrigatorioPreenchido(Curso c){
		mensagem =  "Preencher Campos Obrigatorios";
		if(c == null)
			return false;
		if(c.getCodCurso() == null || c.getCodCurso().trim().isEmpty())
			return false;
		if(c.getNome() == null && c.getNome().trim().isEmpty())
			return false;
		
		return true;
	}
	
	private boolean existeId(String codCurso) throws NegocioException{
		try{
			CursoDAO cDao = new CursoDAO();
			Curso c = cDao.consultarPorId(codCurso);
			if(c == null)
				return false;
			else{
				mensagem =  "Codigo em uso";
				return true;
			}
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}

}
