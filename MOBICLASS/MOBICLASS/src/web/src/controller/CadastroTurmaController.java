package web.src.controller;

import jpa.DAO.TurmaDAO;
import jpa.academico.Turma;
import web.src.entity.NegocioException;

public class CadastroTurmaController extends Controller{

	private static final long serialVersionUID = -4623196637189991997L;
	

	public void gravarTurma(String codTurma, Integer ano, String codCurso, String nome) throws NegocioException{
		try{
			verificarAtributos(codTurma, ano, nome);
			TurmaDAO dao = new TurmaDAO();
			Turma t = new Turma(codTurma, nome, ano, codCurso);
			if(dao.consultarPorId(t.getId()) != null)
				throw new Exception("Turma j� Existe");
			dao.adicionar(t);
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}
	
	public void modificarTurma(String codTurma, Integer ano, String codCurso, String nome) throws NegocioException{
		try{
			verificarAtributos(codTurma, ano, nome);
			Turma t = new Turma(codTurma, nome, ano, nome);
			new TurmaDAO().atualizar(t);
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}

	private void verificarAtributos(String codTurma, Integer ano, String nome) throws Exception{
		if(codTurma == null || codTurma.isEmpty())
			throw new Exception("c�digo da turma invalido");
		if(nome == null || nome.isEmpty())
			throw new Exception("nome da turma invalido");
		if(ano == null)
			throw new Exception("ano da turma invalido");
	}
}
