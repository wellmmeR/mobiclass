package web.src.controller;

import jpa.DAO.AlunoDAO;
import jpa.DAO.ProfessorDAO;
import jpa.pessoa.Aluno;
import jpa.pessoa.Pessoa;
import jpa.pessoa.Professor;
import web.src.entity.NegocioException;
import web.src.entity.Util;

public class CadastroPessoaController extends Controller{

	private static final long serialVersionUID = 1L;
	
	public String gravarPessoa(Pessoa p, int tipo) throws NegocioException{
		
		if(validarPessoa(p)){
			String mensagem=null;
			if(tipo == Util.TIPO_ALUNO){
				Aluno a = virarAluno(p);
				a.setRA(gerarRA(tipo));
				mensagem = gravarAluno(a);
			}
			else if(tipo == Util.TIPO_PROFESSOR){
				Professor pro = virarProfessor(p);
				pro.setRA(gerarRA(tipo));
				mensagem = gravarProfessor(pro);
			}
				
			return mensagem;
		}
		else
			throw new NegocioException("Preencher Campos Obrigatorios");
	}
	
	public String atualizarPessoa(Pessoa p, int tipo) throws NegocioException{
		if(validarPessoa(p)){
			String mensagem=null;
			if(tipo == Util.TIPO_ALUNO){
				Aluno a = virarAluno(p);
				a.setRA(p.getRA());
				mensagem = atualizarAluno(a);
			}
			else if(tipo == Util.TIPO_PROFESSOR){
				Professor pro = virarProfessor(p);
				pro.setRA(p.getRA());
				mensagem = atualizarProfessor(pro);
			}
				
			return mensagem;
		}
		else
			throw new NegocioException("Preencher Campos Obrigatorios");
	}
	
	public String gravarAluno(Aluno a) throws NegocioException{
		try{
			AlunoDAO aDao = new AlunoDAO();
			aDao.adicionar(a);
			return "Aluno Cadastrado com sucesso, RA: " + a.getRA();
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	public String gravarProfessor(Professor p) throws NegocioException{
		try{	
			ProfessorDAO pDao = new ProfessorDAO();
			pDao.adicionar(p);
			return "Professor Cadastrado com sucesso, RA: " + p.getRA();
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	public String atualizarAluno(Aluno a) throws NegocioException{
		try{
			AlunoDAO aDao = new AlunoDAO();
			aDao.atualizar(a);
			return "Aluno Atualizado com sucesso, RA: " + a.getRA();
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}

	public String atualizarProfessor(Professor p) throws NegocioException{
		try{
			ProfessorDAO pDao = new ProfessorDAO();
			pDao.atualizar(p);
			return "Professor Atualizado com sucesso, RA: " + p.getRA();
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	
	
	private Aluno virarAluno(Pessoa p) throws NegocioException{
		Aluno a = new Aluno();
		a.setDataNasc(p.getDataNasc());
		a.setEmail(p.getEmail());
		a.setNome(p.getNome());
		a.setSenha(p.getSenha());
		a.setSexo(p.getSexo());
		return a;
	}
	
	private Professor virarProfessor(Pessoa p) throws NegocioException{
		Professor pro = new Professor();
		pro.setDataNasc(p.getDataNasc());
		pro.setEmail(p.getEmail());
		pro.setNome(p.getNome());
		pro.setSenha(p.getSenha());
		pro.setSexo(p.getSexo());
		return pro;
	}
	
	private long gerarRA(int tipo) throws NegocioException{
		try{
			if(tipo==Util.TIPO_ALUNO)
			{
				AlunoDAO aDao = new AlunoDAO();
				return aDao.primeiroRADisponivel();
			}
			else if(tipo==Util.TIPO_PROFESSOR){
				ProfessorDAO pDao = new ProfessorDAO();
				return pDao.primeiroRADisponivel();
			}
			throw new NegocioException("Tipo de Pessoa Invalido");
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	private boolean validarPessoa(Pessoa p){
		if(p == null)
			return false;
		if(p.getNome() == null || p.getNome().trim().isEmpty())
			return false;
		if( p.getSenha() == null || p.getSenha().trim().isEmpty())
			return false;
		if(p.getDataNasc() == null)
			return false;
		
		return true;
	}
	

}
