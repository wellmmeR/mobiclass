package web.src.controller;

import java.util.List;

import jpa.DAO.AreaAtividadeDAO;
import jpa.DAO.AtividadeDAO;
import jpa.DAO.DisciplinaDAO;
import jpa.DAO.relacionamento.AtividadeDisponivelDAO;
import jpa.academico.Disciplina;
import jpa.atividade.AreaAtividade;
import jpa.atividade.Atividade;
import jpa.atividade.AtividadeDisponivel;
import jpa.util.Desconsiderar;

public class GestaoAtividadeController extends Controller{

	private static final long serialVersionUID = -349111312579915338L;
	
	public List<Atividade> buscarAtividades(Atividade a) throws Exception{
		AtividadeDAO atDao = new AtividadeDAO();
		
		List<Atividade> atividades = atDao.buscarAtividade(new Atividade(a.getIdAtividade().getCodigoAtividade(), a.getNome(), a.getIdAtividade().getCodigoDisciplina(), 
				a.isDisponivel(), a.getIdAtividade().getAreaAtividade()));
		
		for(Atividade atividade :atividades){
			atividade.setQuantidadeAtividadesDisponiveis(procurarQuantidadeAtividadesDisponiveis(atividade));
		}
		return atividades;
	}

	public List<Disciplina> buscarTodasDisciplinas(){
		DisciplinaDAO dao = new DisciplinaDAO();
		List<Disciplina> lista = dao.getItensDaTabela();
		return lista;
	}

	public List<AreaAtividade> buscarTodasAreasAtividades(){
		AreaAtividadeDAO dao = new AreaAtividadeDAO();
		List<AreaAtividade> lista = dao.getItensDaTabela();
		return lista;
	}
	
	private int procurarQuantidadeAtividadesDisponiveis(Atividade a){
		try{
			AtividadeDisponivelDAO dao = new AtividadeDisponivelDAO(); 
			List<AtividadeDisponivel> lista = dao.buscarAtividadesDisponiveis(new AtividadeDisponivel(a.getIdAtividade().getCodigoAtividade(), a.getIdAtividade().getCodigoDisciplina(), Desconsiderar.tipoString, 
					Desconsiderar.tipoInt, null, null, Desconsiderar.tipoInt, a.getIdAtividade().getAreaAtividade(), Desconsiderar.tipoDouble));
			return lista.size();
		}
		catch(Exception ex){
			ex.printStackTrace();
			return 0;
		}
	}
	
	public void deletarAtividade(List<Atividade> atividades) throws Exception{
		AtividadeDAO dao = new AtividadeDAO();
		for(Atividade a: atividades){
			dao.deletarPorId(a.getIdAtividade());
		}
	}
	
	public String getNomeAreaAtividade(long id){
		try{
			AreaAtividadeDAO areaDao = new AreaAtividadeDAO();
			AreaAtividade a = areaDao.consultarPorId(id);
			return a.getAreaAtividade();
		}
		catch(Exception ex){
			ex.printStackTrace();
			return "";
		}
		
		
	}

}
