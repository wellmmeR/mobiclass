package web.src.controller;

import java.util.List;

import jpa.DAO.relacionamento.AlunoAtividadeDAO;
import jpa.DAO.relacionamento.AlunoDisciplinaTurmaDAO;
import jpa.DAO.relacionamento.AtividadeDisponivelDAO;
import jpa.academico.AlunoDisciplinaTurma;
import jpa.atividade.AtividadeDisponivel;
import jpa.atividade.AtividadeDisponivelID;
import jpa.atividade.AtividadeExecutada;
import jpa.pessoa.Aluno;
import jpa.util.Desconsiderar;
import web.src.entity.NegocioException;

public class CadastroAtividadeDisponivelController extends Controller{

	private static final long serialVersionUID = 4466801322534074031L;
	private static final int NAO_CONCLUIU = 0;
	
	public void gravarAtividadeDisponivelEGerarAtividadesParaAlunos(AtividadeDisponivel atDisp) throws NegocioException{
		try{
			AtividadeDisponivelDAO adDao = new AtividadeDisponivelDAO();
			AlunoDisciplinaTurmaDAO adtDao = new AlunoDisciplinaTurmaDAO();
			AlunoAtividadeDAO alunoAtividadeDAO = new AlunoAtividadeDAO();
			adDao.adicionar(atDisp);
			AtividadeDisponivelID id = atDisp.getId();
			List<Aluno> alunos = adtDao.buscarAlunosDaDisciplinaETurma(new AlunoDisciplinaTurma(Desconsiderar.tipoLong, id.getCodDisciplina(), id.getCodTurma(), id.getAnoTurma(), NAO_CONCLUIU));
			for(Aluno a : alunos){
				AtividadeExecutada aExecutada = new AtividadeExecutada(id.getCodTurma(), id.getAnoTurma(), a.getRA(), id.getCodAtividade(), id.getCodDisciplina(), id.getAreaAtividade(), 0);
				alunoAtividadeDAO.adicionar(aExecutada);
			}
		}
		catch(Exception ex){
			throw new NegocioException("Atividade: " + atDisp.getId().getCodAtividade() + ":" + atDisp.getId().getCodTurma() + ":" + ex.getMessage());
		}
	}
	
	public void validarAtividadeDisponivel(){
		
	}

}
