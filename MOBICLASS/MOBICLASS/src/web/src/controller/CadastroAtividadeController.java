package web.src.controller;

import jpa.DAO.AreaAtividadeDAO;
import jpa.DAO.AtividadeDAO;
import jpa.atividade.AreaAtividade;
import jpa.atividade.Atividade;
import jpa.atividade.AtividadeID;
import web.src.entity.NegocioException;

public class CadastroAtividadeController extends Controller{

	private static final long serialVersionUID = 1L;
	private String mensagem;

	public String cadastrarAtividade(String nome,String codAtividade,String codDisciplina, long codArea, int disponivel)throws NegocioException{
		try{
			Atividade a = new Atividade(codAtividade, nome, codDisciplina, disponivel, codArea);
			if(verificarAtributosDaAtividade(a) && atividadeDisponivel(codAtividade, codDisciplina, codArea)){
				AtividadeDAO aDAO = new AtividadeDAO();
				aDAO.adicionar(a);
				mensagem ="Atividade gravada com sucesso";
				return mensagem;
			}
			else {
				throw new NegocioException(mensagem);
			}
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
		
	}
	
	public String modificarAtividade(String nome,String codAtividade,String codDisciplina, long codArea, int disponivel)throws NegocioException{
		try{
			Atividade a = new Atividade(codAtividade, nome, codDisciplina, disponivel, codArea);
			if(verificarAtributosDaAtividade(a)){
				AtividadeDAO aDAO = new AtividadeDAO();
				aDAO.atualizar(a);
				mensagem ="Atividade atualizada com sucesso";
				return mensagem;
			}
			else {
				throw new NegocioException(mensagem);
			}
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
		
	}
	
	private boolean atividadeDisponivel(String codAtividade, String codDisciplina, long codArea) throws NegocioException{
		try{
			AtividadeDAO aDAO = new AtividadeDAO();
			AtividadeID id = new AtividadeID(codAtividade, codDisciplina, codArea);
			if(aDAO.consultarPorId(id)!= null){
				mensagem = "J� existe essa Atividade";
				return false;
			}	
			return true;
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	private boolean verificarAtributosDaAtividade(Atividade atividade){
		if(atividade.getNome() == null || atividade.getNome().trim().isEmpty()){
			mensagem = "Preencher o nome da atividade";
			return false;
		}
		AtividadeID a = atividade.getIdAtividade();
		if(a.getCodigoDisciplina() == null || a.getCodigoDisciplina().trim().isEmpty()){
			mensagem = "Preencher a disciplina";
			return false;
		}
		if(a.getCodigoAtividade() == null || a.getCodigoAtividade().trim().isEmpty()){
			mensagem = "Preencher o codigo da atividade";
			return false;
		}
		if(a.getAreaAtividade() == 0L){
			mensagem = "Preencher c�digo da area da atividade";
			return false;
		}
		return true;
	}
	
	public void cadastrarAreaAtividade(String nome) throws NegocioException{
		try{
			AreaAtividadeDAO dao = new AreaAtividadeDAO();
			dao.adicionar(new AreaAtividade(nome));
		}
		catch(Exception ex){
			throw new NegocioException("Erro ao cadastrar Area da Atividade", ex.getMessage());
		}
	}
		
}
