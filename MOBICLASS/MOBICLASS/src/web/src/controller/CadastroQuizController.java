package web.src.controller;

import jpa.DAO.AlternativaDAO;
import jpa.DAO.QuestaoDAO;
import jpa.atividade.AlternativaQuestao;
import jpa.atividade.Atividade;
import jpa.atividade.Questao;
import web.src.entity.NegocioException;
import web.src.entity.Util;

public class CadastroQuizController extends Controller{

	private static final long serialVersionUID = -1889075732630757536L;
	
	public void deletarQuestao(Questao q){
		if(q!=null){
//			QuestaoDAO dao = new QuestaoDAO();
//			AlternativaDAO alternativaDao = new AlternativaDAO();
			
		}		
	}
	
	public Questao criarQuestao(Atividade a,String nome, double valor, boolean disponivel) throws NegocioException{
		try{
			Questao q = new Questao();
			q.setAreaAtividade(a.getIdAtividade().getAreaAtividade());
			q.setCodAtividade(a.getIdAtividade().getCodigoAtividade());
			q.setCodDisciplina(a.getIdAtividade().getCodigoDisciplina());
			q.setNomeQuestao(nome);
			q.setValorPergunta(valor);
			q.setIsDisponivel(Util.booleanParaInteiro(disponivel));
			gravarQuestao(q);
			return q;
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	public AlternativaQuestao criarAlternativa(Questao q, boolean correta, String conteudo) throws NegocioException{
		try{
			AlternativaQuestao alternativa = new AlternativaQuestao(conteudo, Util.booleanParaInteiro(correta), q.getCodPergunta());
			gravarAlternativa(alternativa);
			return alternativa;
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	private void gravarQuestao(Questao q) throws NegocioException{
		try{
			QuestaoDAO qDao = new QuestaoDAO();
			qDao.adicionar(q);
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	private void gravarAlternativa(AlternativaQuestao alternativa) throws NegocioException{
		try{
			AlternativaDAO alternativaDao = new AlternativaDAO();
			alternativaDao.adicionar(alternativa);
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	public boolean objetoNulo(Object o){
		if(o==null)
			return true;
		
		return false;
	}

}
