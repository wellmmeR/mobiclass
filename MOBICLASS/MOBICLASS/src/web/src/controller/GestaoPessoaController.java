package web.src.controller;

import java.util.Date;
import java.util.List;

import jpa.DAO.AlunoDAO;
import jpa.pessoa.Aluno;
import jpa.util.Desconsiderar;
import web.src.entity.NegocioException;

public class GestaoPessoaController extends Controller{
	
	private static final long serialVersionUID = 6031105646548025252L;

	public List<Aluno> procurarAlunos(Long raPessoa, String nomePessoa, String emailPessoa, Date dtNascimento, String sexo) throws NegocioException{
		try{
			Long ra = raPessoa == null? Desconsiderar.tipoLong: raPessoa.longValue();
			sexo = (sexo == null) ? (null):( sexo.equals("A") ? (null):(sexo) );
			Aluno a = new Aluno(ra, nomePessoa, dtNascimento, emailPessoa, null, sexo);
			List<Aluno> alunos = procurarAluno(a);
			return alunos;
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	private List<Aluno> procurarAluno(Aluno a) throws NegocioException{
		try{
			AlunoDAO dao = new AlunoDAO();
			return dao.buscarAlunos(a);
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	public void deletarAlunos(List<Aluno> alunos) throws NegocioException{
		try{
			AlunoDAO dao = new AlunoDAO();
			dao.deletarPorId(alunos);
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}

}
