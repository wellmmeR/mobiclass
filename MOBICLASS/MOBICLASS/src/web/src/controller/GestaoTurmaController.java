package web.src.controller;

import java.util.ArrayList;
import java.util.List;

import web.src.entity.NegocioException;
import jpa.DAO.TurmaDAO;
import jpa.academico.Turma;

public class GestaoTurmaController extends Controller{

	private static final long serialVersionUID = 777274350535027467L;
	
	public List<Integer> todosAnosDeTurmas(){
		List<Turma> turmas = buscarTodasTurmas();
		List<Integer> anos = new ArrayList<Integer>();
		for(Turma turma : turmas){
			if(!anos.contains(turma.getId().getAnoTurma())){
				anos.add(turma.getId().getAnoTurma());
			}
		}
		return anos;
	}
	
	public List<Turma> buscarTurmas(String nome, String codCurso, String codTurma, Integer ano) throws NegocioException{
		try{
			Turma t = new Turma(codTurma, nome, ano, codCurso);
			TurmaDAO dao = new TurmaDAO();
			return dao.consultarTurmas(t);
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}
	
	public void deletarTurmas(List<Turma> turmas) throws NegocioException{
		try{
			new TurmaDAO().deletarPorId(turmas); 
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}

}
