package web.src.controller;

import java.util.ArrayList;
import java.util.List;

import jpa.DAO.DisciplinaDAO;
import jpa.DAO.relacionamento.TurmaDisciplinaDAO;
import jpa.academico.Disciplina;
import jpa.academico.TurmaDisciplina;
import jpa.academico.TurmaID;
import web.src.entity.NegocioException;

public class DetalharTurmaController extends Controller{

	private static final long serialVersionUID = 3564153812747232287L;
	
	public List<Disciplina> buscarDisciplinas(Disciplina d) throws NegocioException{
		try{
			DisciplinaDAO dao = new DisciplinaDAO();
			return dao.buscarDisciplinas(d);
		}
		catch(Exception ex){
			throw new NegocioException("Erro ao buscar Disciplinas: " + ex.getMessage());
		}
	}
	
	public List<Disciplina> disciplinasDaTurma(String codDisciplina, String nomeDisciplina, String codTurma, Integer ano) throws NegocioException{
		try{
			TurmaDisciplinaDAO dao = new TurmaDisciplinaDAO();
			return dao.buscarDisciplinasDaTurma(new Disciplina(codDisciplina, nomeDisciplina), codTurma, ano);
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}
	
	public void vincularDisciplina(List<Disciplina> disciplinas, TurmaID id) throws NegocioException{
		try{
			TurmaDisciplinaDAO dao = new TurmaDisciplinaDAO();
			List<TurmaDisciplina> lista = new ArrayList<TurmaDisciplina>(); 
			for (Disciplina disciplina : disciplinas) {
				lista.add(new TurmaDisciplina(disciplina.getCodigoDisciplina(), id.getCodigoTurma(), id.getAnoTurma()));
			}		
			dao.adicionar(lista);
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}
	
	public void desvincularDisciplina(List<Disciplina> disciplinas, TurmaID id) throws NegocioException{
		try{
			TurmaDisciplinaDAO dao = new TurmaDisciplinaDAO();
			List<TurmaDisciplina> lista = new ArrayList<TurmaDisciplina>();
			for(Disciplina d: disciplinas){
				lista.add(new TurmaDisciplina(d.getCodigoDisciplina(), id.getCodigoTurma(), id.getAnoTurma()));
			}
			dao.deletarPorId(lista);
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
	}

}
