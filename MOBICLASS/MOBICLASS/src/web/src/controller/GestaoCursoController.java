package web.src.controller;

import java.util.List;

import jpa.DAO.CursoDAO;
import jpa.academico.Curso;
import web.src.entity.NegocioException;

public class GestaoCursoController extends Controller{

	private static final long serialVersionUID = -4451357604327919484L;

	public List<Curso> buscarCursos(String nome, String codCurso, Integer disponivel) throws NegocioException{
		try{
			CursoDAO dao = new CursoDAO();
			if(disponivel != null && disponivel.equals(2))
				disponivel=null;
			return dao.buscarCursos(codCurso, nome, disponivel);
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	public void deletarCurso(Curso c) throws NegocioException{
		try{
			CursoDAO dao = new CursoDAO();
			dao.deletarPorId(c.getCodCurso());
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
}
