package jpa.mgr.atividade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jws.WebService;

import jpa.DAO.AlternativaDAO;
import jpa.DAO.AreaAtividadeDAO;
import jpa.DAO.AtividadeDAO;
import jpa.DAO.JogoForcaDAO;
import jpa.DAO.LogDAO;
import jpa.DAO.PaginaSlideDAO;
import jpa.DAO.PontuacaoDAO;
import jpa.DAO.QuestaoDAO;
import jpa.DAO.relacionamento.AlunoAtividadeDAO;
import jpa.DAO.relacionamento.AtividadeDisponivelDAO;
import jpa.atividade.AlternativaQuestao;
import jpa.atividade.AreaAtividade;
import jpa.atividade.Atividade;
import jpa.atividade.AtividadeDisponivel;
import jpa.atividade.AtividadeExecutada;
import jpa.atividade.AtividadeExecutadaID;
import jpa.atividade.AtividadeID;
import jpa.atividade.JogoForca;
import jpa.atividade.PaginaSlide;
import jpa.atividade.Pontuacao;
import jpa.atividade.Questao;
import jpa.constante.log.DadosLog;
import jpa.constante.log.Log;
import jpa.loggin.MgrLoggin;
import jpa.util.Desconsiderar;

import com.google.gson.Gson;
@WebService
public class MgrAtividade {
	private DadosLog lblLog = new DadosLog();
	private MgrLoggin loggin = new MgrLoggin();

	// Cadastro--------------------------------------------------------------------
	public String addAreaAtividade(long raProfessor, String senhaProfessor,
			String areaAtividade) {
		if (loggin.isProfessorLogado(raProfessor, senhaProfessor) == 0)
			return lblLog._professorNaoLogado;
		try {
			AreaAtividade at = new AreaAtividade(areaAtividade);
			new AreaAtividadeDAO().adicionar(at);
			return lblLog._true;
		} catch (Exception e) {
			return lblLog._false + e.getMessage();
		}
	}

	public String addAtividadeSlides(long raProfessor, String senhaProfessor,
			String codAtividade, long areaAtividade, String codDisciplina,
			String codTurma, int anoTurma, Date data, int totalPagina,
			int paginaAtual) {
		if (loggin.isProfessorLogado(raProfessor, senhaProfessor) == 0)
			return lblLog._professorNaoLogado;
		try {
			PaginaSlide ps = new PaginaSlide(codAtividade, codDisciplina,
					codTurma, anoTurma, data, totalPagina, paginaAtual,
					areaAtividade);
			new PaginaSlideDAO().adicionar(ps);
			return lblLog._true;
		} catch (Exception e) {
			return lblLog._false + e.getMessage();
		}
	}

	public String addPergunta(long raMaster, String senhaMaster,
			String nomQuestao, String codDisciplina, String codAtividade,
			long areaAtividade, int categoria, String valor, int isDisponivel) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0)
			return lblLog._masterNaoLogado;
		try {
			Questao q = new Questao(nomQuestao, codDisciplina, isDisponivel,
					codAtividade, categoria, Double.parseDouble(valor),
					areaAtividade);
			new QuestaoDAO().adicionar(q);
			gravaLog(lblLog._regAdd + raMaster, lblLog._perguntaAdd,
					lblLog._tipoCadastro);
			return lblLog._true;
		} catch (Exception e) {
			gravaLog(lblLog._erroCadastro + e.getMessage(), lblLog._erroMetodo
					+ "addPergunta", lblLog._tipoExcecao);
			return lblLog._false + e.getMessage();
		}
	}

	public String addRespostaPergunta(long raMaster, String senhaMaster,
			String nomResposta, long codPergunta, int isCorreta) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0)
			return lblLog._masterNaoLogado;
		try {
			AlternativaQuestao aq = new AlternativaQuestao(nomResposta,
					isCorreta, codPergunta);
			new AlternativaDAO().adicionar(aq);
			gravaLog(lblLog._regAdd + raMaster, lblLog._respostaAdd,
					lblLog._tipoCadastro);
			return lblLog._true;
		} catch (Exception e) {
			gravaLog(lblLog._erroCadastro + e.getMessage(), lblLog._erroMetodo
					+ "addRespostaPergunta", lblLog._tipoExcecao);
			return lblLog._false + e.getMessage();
		}
	}

	public String addAtividade(long raProfessor, String senhaProfessor,
			String codAtividade, long areaAtividade, String nome,
			String codDisciplina, int isDisponivel) {
		if (loggin.isProfessorLogado(raProfessor, senhaProfessor) == 0)
			return lblLog._professorNaoLogado;
		try {
			Atividade atv = new Atividade(codAtividade, nome, codDisciplina,
					isDisponivel, areaAtividade);
			new AtividadeDAO().adicionar(atv);
			gravaLog(lblLog._regAdd + raProfessor, lblLog._atividadeAdd,
					lblLog._tipoCadastro);
			return lblLog._true;
		} catch (Exception e) {
			gravaLog(lblLog._erroCadastro + e.getMessage(), lblLog._erroMetodo
					+ "addAtividade", lblLog._tipoExcecao);
			return lblLog._false + e.getMessage();
		}
	}

	public String addPontuacao(long raAluno, String senhaAluno,
			String codAtividade, long areaAtividade, String codDisciplina,
			String codTurma, int anoTurma, String descricao, String pontos,
			String codLancamento) {
		if (loggin.isAlunoLogado(raAluno, senhaAluno) == 0)
			return lblLog._alunoNaoLogado;
		try {
			Pontuacao p = new Pontuacao(raAluno, codAtividade, codDisciplina,
					codTurma, anoTurma, descricao, Double.parseDouble(pontos),
					codLancamento, areaAtividade);
			new PontuacaoDAO().adicionar(p);
			gravaLog(lblLog._regAdd + raAluno, lblLog._disciplinaAdd,
					lblLog._tipoCadastro);
			return lblLog._true;
		} catch (Exception e) {
			gravaLog(lblLog._erroCadastro + e.getMessage(), lblLog._erroMetodo
					+ "addPontuacao", lblLog._tipoExcecao);
			return lblLog._false + e.getMessage();
		}
	}

	// Remocao---------------------------------------------------------------------
	public String removeAtividade(long raProfessor, String senhaProfessor,
			String codAtividade, long areaAtividade, String codDisciplina) {
		if (loggin.isProfessorLogado(raProfessor, senhaProfessor) == 0)
			return lblLog._professorNaoLogado;
		try {
			AtividadeDAO dao = new AtividadeDAO();
			AtividadeID id = new AtividadeID(codAtividade, codDisciplina, areaAtividade);
			Atividade atividade = dao.consultarPorId(id);
			if (atividade == null)
				return "false " + lblLog._erroQueryVazia;
			dao.deletarPorId(id);
			return lblLog._true + lblLog._atividadeDeletada;
		} catch (Exception e) {
			return lblLog._false + e.getMessage();
		}
	}

	// Visualiza��o----------------------------------------------------------------
	public int getPagina(String codAtividade, long areaAtividade,
			String codDisciplina, String codTurma, int anoTurma, int paginaAtual) {
		try {
			PaginaSlideDAO dao = new PaginaSlideDAO();
			PaginaSlide ps = dao.buscarSinglePaginaSlide(
					new PaginaSlide(codAtividade, codDisciplina, codTurma, anoTurma, new Date(), Desconsiderar.tipoInt, paginaAtual, areaAtividade));
			if (ps == null)
				return -1;
			return ps.getPaginaAtual();
		} catch (Exception e) {
			return -1;
		}
	}

	public ArrayList<String> getAtividades(long raPessoa, String senhaPessoa,
			String codAtividade, long areaAtividade, String codDisciplina) {
		if (loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0
				&& loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0)
			return null;
		try {
			AtividadeDAO dao = new AtividadeDAO();
			List<Atividade> atividade = dao.buscarAtividade(new Atividade(codAtividade, null, codDisciplina, Desconsiderar.tipoInt, areaAtividade));
			ArrayList<String> arrayList = new ArrayList<String>();
			for (Atividade atv : atividade) {
				arrayList.add(new Gson().toJson(atv));
			}
			return arrayList;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getAtividade", lblLog._tipoExcecao);
			return null;
		}
	}

	// ////////
	public ArrayList<String> getAtividadeDisponivel(long raPessoa,
			String senhaPessoa, String codDisciplina, long areaAtividade,
			String codTurma, int anoTurma) {
		if (loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0
				&& loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0)
			return null;
		try {
			AtividadeDisponivelDAO  dao = new AtividadeDisponivelDAO();
			List<AtividadeDisponivel> listAtividade = dao.buscarAtividadesDisponiveis(new AtividadeDisponivel(null, codDisciplina, codTurma, anoTurma, null, null, Desconsiderar.tipoInt, Desconsiderar.tipoDouble, areaAtividade));
			if (listAtividade == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> atvDisp = new ArrayList<String>();
			Date now = new Date();
			for (AtividadeDisponivel atv : listAtividade) {
				if(atv.getDataLiberacao() != null && atv.getDataLiberacao().before(now))
					atvDisp.add(new Gson().toJson(atv));
			}
			return atvDisp;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getAtividadeDisponivel", lblLog._tipoExcecao);
			return null;
		}
	}

	public ArrayList<String> getQuestaoCategoria(long raPessoa,
			String senhaPessoa, String codDisciplina, String codAtividade,
			long areaAtividade, int categoria) {
		if (loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0
				&& loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0)
			return null;
		try {
			QuestaoDAO dao = new QuestaoDAO();
			AlternativaDAO alternativaDao = new AlternativaDAO();
			List<Questao> list = dao.buscarQuestao(new Questao(null, codDisciplina, 1, codAtividade, categoria, Desconsiderar.tipoInt, areaAtividade));
			if (list == null) {
				gravaLog(lblLog._erroQuery, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> aL = new ArrayList<String>();
			for (Questao qs : list) {
				aL.add(qs.toString());
				List<AlternativaQuestao> listAlter = alternativaDao.buscarAlternativas(new AlternativaQuestao(null, Desconsiderar.tipoInt, qs.getCodPergunta()));
				for (AlternativaQuestao aQ : listAlter) {
					aL.add("|-|-" + aQ.toString() + "-|-|");
				}
			}
			return aL;
		} catch (Exception e) {
			return null;
		}
	}

	public ArrayList<String> getQuestao(long raPessoa, String senhaPessoa,
			String codDisciplina, String codAtividade, long areaAtividade) {
		if (loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0
				&& loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0)
			return null;
		try {
			QuestaoDAO dao = new QuestaoDAO();
			AlternativaDAO alternativaDao = new AlternativaDAO();
			List<Questao> list = dao.buscarQuestao(new Questao(null, codDisciplina, 1, codAtividade, Desconsiderar.tipoInt, Desconsiderar.tipoInt, areaAtividade));
			if (list == null) {
				gravaLog(lblLog._erroQuery, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> aL = new ArrayList<String>();
			for (Questao qs : list) {
				aL.add(qs.toString());
				List<AlternativaQuestao> listAlter = alternativaDao.buscarAlternativas(new AlternativaQuestao(null, Desconsiderar.tipoInt, qs.getCodPergunta()));
				for (AlternativaQuestao aQ : listAlter) {
					aL.add("|-|-" + aQ.toString() + "-|-|");
				}
			}
			return aL;
		} catch (Exception e) {
			return null;
		}
	}
	
	public List<String> getJogosForca(long raPessoa, String senhaPessoa, 
			String codDisciplina, String codAtividade, long areaAtividade){
		if (loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0
				&& loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0)
			return null;
		try {
			JogoForcaDAO dao = new JogoForcaDAO();
			List<JogoForca> list = dao.buscarJogosForca(new JogoForca(codAtividade, codDisciplina, areaAtividade, 1,  null, null, null, Desconsiderar.tipoInt));
			if (list == null || list.isEmpty()) {
				gravaLog(lblLog._erroQuery, null, lblLog._tipoQuery);
				return null;
			}
			List<String> listaJSON = new ArrayList<String>();
			for(JogoForca jogo: list){
				listaJSON.add(new Gson().toJson(jogo));
			}
			return listaJSON;
		} catch (Exception e) {
			return null;
		}
		
	}
	
	public String marcarInicioAtividadeExecutada(long raPessoa,String senhaPessoa, 
			String codAti, String codDisciplina, long areaAtividade,String codTurma, int anoTurma, long raAluno){
		if (loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0
				&& loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0)
			return null;
		try {
			AlunoAtividadeDAO dao = new AlunoAtividadeDAO();
			AtividadeExecutada atv = dao.consultarPorId(new AtividadeExecutadaID(raAluno, codAti, codDisciplina, codTurma, anoTurma, areaAtividade));
			if(atv==null)
				return null;
			atv.setDataInicio(new Date());
			dao.atualizar(atv);
			return lblLog._true;
		} catch (Exception e) {
			return null;
		}
	}
	
	public String marcarFimAtividadeExecutada(long raPessoa,String senhaPessoa, 
			String codAti, String codDisciplina, long areaAtividade,String codTurma, int anoTurma, long raAluno, String nota){
		if (loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0
				&& loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0)
			return null;
		try {
			AlunoAtividadeDAO dao = new AlunoAtividadeDAO();
			AtividadeExecutada atv = dao.consultarPorId(new AtividadeExecutadaID(raAluno, codAti, codDisciplina, codTurma, anoTurma, areaAtividade));
			if(atv==null)
				return null;
			atv.setDataFim(new Date());
			atv.setIsFinalizado(1);
			dao.atualizar(atv);
			new PontuacaoDAO().adicionar(new Pontuacao(raAluno, atv.getId().getCodAtividade(), atv.getId().getCodDisciplina(), 
					atv.getId().getCodTurma(), atv.getId().getAnoTurma(), null, Double.parseDouble(nota), "", atv.getId().getAreaAtividade()));
			return lblLog._true;
		} catch (Exception e) {
			return null;
		}
	}

	public ArrayList<String> getPontuacaoAluno(long raPessoa,
			String senhaPessoa, long raAluno, String codDisciplina,
			String codTurma, int anoTurma, String codAtividade,
			long areaAtividade) {
		PontuacaoDAO dao = new PontuacaoDAO();
		try {
			if (loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0
					&& loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0)
				return null;

			List<Pontuacao> lista = dao.buscarPontuacao(new Pontuacao(raAluno, codAtividade, codDisciplina, codTurma, anoTurma, null, Desconsiderar.tipoInt, null, areaAtividade));
			if (lista == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> aL = new ArrayList<String>();
			for (Pontuacao p : lista) {
				aL.add(new Gson().toJson(p));
			}
			return aL;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getPontuacaoAluno", lblLog._tipoExcecao);
			return null;
		}
	}

	public ArrayList<String> getAreaAtividade(long raPessoa,
			String senhaPessoa, String codAtividade, String codTurma,
			int anoTurma, String codDisciplina) {
		try {
			if (loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0
					&& loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0)
				return null;
			AtividadeDisponivelDAO dao = new AtividadeDisponivelDAO();
			List<AreaAtividade> lista = dao.buscarAreaAtividadeDaAtividadeDisponivel(new AtividadeDisponivel(codAtividade, codDisciplina, codTurma, anoTurma, null, null, 1, Desconsiderar.tipoDouble, Desconsiderar.tipoLong));
			if (lista == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> aL = new ArrayList<String>();
			for (AreaAtividade at : lista) {
				aL.add(at.toString());
			}
			return aL;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getPontuacaoAluno", lblLog._tipoExcecao);
			return null;
		}
	}

	public ArrayList<String> getAreasAtividade(long raPessoa, String senhaPessoa) {
		try {
			if (loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0
					&& loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0)
				return null;
			AreaAtividadeDAO dao = new AreaAtividadeDAO();
			List<AreaAtividade> lista = dao.getItensDaTabela();
			if (lista == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> aL = new ArrayList<String>();
			for (AreaAtividade at : lista) {
				aL.add(at.toString());
			}
			return aL;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getPontuacaoAluno", lblLog._tipoExcecao);
			return null;
		}
	}

	// Atribui��o------------------------------------------------------------------
	public String setPagina(long raProfessor, String senhaProfessor,
			String codAtividade, long areaAtividade, String codDisciplina,
			String codTurma, int anoTurma, int paginaAtual) {
		try {
			if (loggin.isProfessorLogado(raProfessor, senhaProfessor) == 0)
				return lblLog._professorNaoLogado;
			PaginaSlideDAO dao = new PaginaSlideDAO();
			PaginaSlide ps = dao.buscarSinglePaginaSlide(new PaginaSlide(codAtividade, codDisciplina, codTurma, anoTurma, null, Desconsiderar.tipoInt, Desconsiderar.tipoInt, areaAtividade));
			if (ps == null)
				return null;
			ps.setPaginaAtual(paginaAtual);
			dao.atualizar(ps);
			return lblLog._true;
		} catch (Exception e) {
			return lblLog._false + e.getMessage();
		}
	}

	public String setOnAtividade(long raProfessor, String senhaProfessor,
			String codAtividade, long areaAtividade, String codDisciplina,
			String codTurma, int anoTurma, String dataLiberacao,
			String dataFechamento, int isDisponivel, String valorAtividade) {
		if (loggin.isProfessorLogado(raProfessor, senhaProfessor) == 0)
			return lblLog._professorNaoLogado;
		try {
			AtividadeDAO atividadeDao = new AtividadeDAO();
			AtividadeDisponivelDAO atividadeDisponivelDao = new AtividadeDisponivelDAO();
			
			Atividade atividade = atividadeDao.buscarSingleAtividade(new Atividade(codAtividade, null, codDisciplina, isDisponivel, areaAtividade));
			if (atividade == null) {
				gravaLog(lblLog._erroQuery, null, lblLog._tipoQuery);
				return lblLog._atvNotFound;
			}
			AtividadeDisponivel ad = new AtividadeDisponivel(
					atividade.getIdAtividade().getCodigoAtividade(),
					atividade.getIdAtividade().getCodigoDisciplina(), codTurma, anoTurma,
					dataLiberacao, dataFechamento, isDisponivel,
					Double.parseDouble(valorAtividade), atividade.getIdAtividade().getAreaAtividade());
			atividadeDisponivelDao.adicionar(ad);
			gravaLog(lblLog._atividadeLiberada + raProfessor, null,
					lblLog._tipoCadastro);
			return lblLog._true;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery, "setAtividadeExecutada",
					lblLog._tipoExcecao);
			return lblLog._false + e.getMessage();
		}
	}

	public String setOffAtividade(long raProfessor, String senhaProfessor,
			String codAtividade, long areaAtividade, String codDisciplina,
			String codTurma, int anoTurma, String dataFim) {
		if (loggin.isProfessorLogado(raProfessor, senhaProfessor) == 0)
			return lblLog._professorNaoLogado;
		try {
			AtividadeDisponivelDAO atDDao = new AtividadeDisponivelDAO();
			AtividadeDisponivel atividade = atDDao.buscarSingleAtividadeDisponivel(new AtividadeDisponivel(null, codDisciplina, codTurma, anoTurma, null, null, Desconsiderar.tipoInt, Desconsiderar.tipoDouble, areaAtividade));
			if (atividade == null)
				return lblLog._atvNotFound;
			// Merge
			atividade.setIsDisponivel(0);
			atividade.setDataEncerramento(dataFim);
			atDDao.atualizar(atividade);
			gravaLog(lblLog._atividadeEditada + raProfessor, null,
					lblLog._tipoEdicao);
			return lblLog._true;
		} catch (Exception e) {
			return lblLog._false + e.getMessage();
		}
	}

	public String setStartAtividade(String codAtividade, long areaAtividade,
			String codDisciplina, String codTurma, int anoTurma, long raAluno,
			String senhaAluno) {
		if (loggin.isAlunoLogado(raAluno, senhaAluno) == 0) {
			gravaLog(lblLog._alunoNaoLogado, null, lblLog._tipoLogin);
			return "false - Aluno n�o logado";
		}
		try {
			AtividadeDisponivelDAO dao = new AtividadeDisponivelDAO();
			AtividadeDisponivel atividade = dao.buscarSingleAtividadeDisponivel(new AtividadeDisponivel(codAtividade, codDisciplina, codTurma, anoTurma, null, null, 1, Desconsiderar.tipoDouble, areaAtividade));
			if (atividade == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoCadastro);
				return lblLog._atvNotFound;
			}
			AtividadeExecutada atvExec = new AtividadeExecutada(
					atividade.getId().getCodTurma(), atividade.getId().getAnoTurma(), raAluno,
					atividade.getId().getCodAtividade(), atividade.getId().getCodDisciplina(),
					atividade.getId().getAreaAtividade(), 0);
			AlunoAtividadeDAO atDao = new AlunoAtividadeDAO();
			atDao.adicionar(atvExec);
			gravaLog(lblLog._atribuicaoAdd + raAluno, "Atividade => Aluno",
					lblLog._tipoCadastro);
			return lblLog._true;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery, "setAtividadeExecutada",
					lblLog._tipoExcecao);
			return lblLog._false + e.getMessage();
		}
	}

	public String setStopAtividade(String codAtividade, long areaAtividade,
			String codDisciplina, String codTurma, int anoTurma, long raAluno,
			String senhaAluno) {
		if (loggin.isAlunoLogado(raAluno, senhaAluno) == 0)
			return lblLog._alunoNaoLogado;
		try {
			AlunoAtividadeDAO atDao = new AlunoAtividadeDAO();
			AtividadeExecutada atividade = atDao.consultarPorId(new AtividadeExecutadaID(raAluno, codAtividade, codDisciplina, codTurma, anoTurma, areaAtividade));
			if (atividade == null)
				return lblLog._atvNotFound;
			atividade.setIsFinalizado(1);
			atDao.atualizar(atividade);
			gravaLog(lblLog._alunoLogout + raAluno, null, lblLog._tipoCadastro);
			return lblLog._true;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery, null, lblLog._tipoCadastro);
			return lblLog._false + e.getMessage();
		}
	}
	
	public String getAtividadeExecutada(long raPessoa,String senhaPessoa, 
			String codAti, String codDisciplina, long areaAtividade,String codTurma, int anoTurma, long raAluno){
		try {
			if (loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0
					&& loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0)
				return null;
			AlunoAtividadeDAO dao = new AlunoAtividadeDAO();
			List<AtividadeExecutada> lista = dao.buscarAtividadesExecutadas(new AtividadeExecutada(codTurma, anoTurma, raAluno, codAti, codDisciplina, areaAtividade, Desconsiderar.tipoInt));
			if (lista == null || lista.isEmpty()) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			String retorno = new Gson().toJson(lista.get(0));
			return retorno;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getPontuacaoAluno", lblLog._tipoExcecao);
			return null;
		}
	}
	
	public String getNomeAtividade(long raPessoa, String senhaPessoa,
			String codAtividade, long areaAtividade, String codDisciplina){
		if (loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0
				&& loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0)
			return null;
		try {
			AtividadeDAO dao = new AtividadeDAO();
			List<Atividade> atividades = dao.buscarAtividade(new Atividade(codAtividade, null, codDisciplina, Desconsiderar.tipoInt, areaAtividade));
			if(atividades == null || atividades.isEmpty())
			{
				gravaLog(lblLog._erroQuery, null, lblLog._tipoQuery);
				return null;
			}
			String nome = atividades.get(0).getNome();
			return nome;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getAtividade", lblLog._tipoExcecao);
			return null;
		}
	}

	// Login-----------------------------------------------------------------------
	public String loginAluno(long raAluno, String senhaAluno) {
		try {
			String mensagem = loggin.loginAluno(raAluno, senhaAluno);
			return mensagem;
		} catch (Exception e) {
			return lblLog._false + e.getMessage();
		}
	}

	public String loginProfessor(long raProfessor, String senhaProfessor) {
		try {
			if (loggin.loginProfessor(raProfessor, senhaProfessor))
				return lblLog._true;
			else
				return lblLog._false;
		} catch (Exception e) {
			return lblLog._false + e.getMessage();
		}
	}

	// Logoff----------------------------------------------------------------------
	public String logoffAluno(long raAluno, String senhaAluno) {
		try {
			if (loggin.alunoFinalizaSessao(raAluno, senhaAluno))
				return lblLog._true;
			else
				return lblLog._false;
		} catch (Exception e) {
			return lblLog._false + e.getMessage();
		}
	}

	public String logoffProfessor(long raProfessor, String senhaProfessor) {
		try {
			if (loggin.professorFinalizaSessao(raProfessor, senhaProfessor))
				return lblLog._true;
			else
				return lblLog._false;
		} catch (Exception e) {
			return lblLog._false + e.getMessage();
		}
	}

	// Fim-------------------------------------------------------------------------
	private void gravaLog(String dadosLog, String detalhe, int tipo) {
		try {
			Log log = new Log(dadosLog, detalhe, tipo);
			new LogDAO().adicionar(log);
		} catch (Exception e) {
			return;
		}
	}
}
