package jpa.mgr.academico;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

import jpa.DAO.AlunoDAO;
import jpa.DAO.CursoDAO;
import jpa.DAO.DisciplinaDAO;
import jpa.DAO.LogDAO;
import jpa.DAO.ProfessorDAO;
import jpa.DAO.TurmaDAO;
import jpa.DAO.relacionamento.AlunoCursoDAO;
import jpa.DAO.relacionamento.AlunoDisciplinaTurmaDAO;
import jpa.DAO.relacionamento.AlunoTurmaDAO;
import jpa.DAO.relacionamento.DisciplinaProfessorDAO;
import jpa.DAO.relacionamento.ProfessorTurmaDAO;
import jpa.DAO.relacionamento.TurmaDisciplinaDAO;
import jpa.academico.AlunoCurso;
import jpa.academico.AlunoDisciplinaTurma;
import jpa.academico.AlunoDisciplinaTurmaID;
import jpa.academico.AlunoTurma;
import jpa.academico.AlunoTurmaID;
import jpa.academico.Curso;
import jpa.academico.Disciplina;
import jpa.academico.DisciplinaProfessor;
import jpa.academico.ProfessorTurma;
import jpa.academico.Turma;
import jpa.academico.TurmaDisciplina;
import jpa.academico.TurmaID;
import jpa.constante.log.DadosLog;
import jpa.constante.log.Log;
import jpa.loggin.MgrLoggin;
import jpa.pessoa.Aluno;
import jpa.pessoa.Professor;
import jpa.util.Desconsiderar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
@WebService
public class MgrAcademico {
	private DadosLog lblLog = new DadosLog();
	private MgrLoggin loggin = new MgrLoggin();

	// Cadastro--------------------------------------------------------------------
	public String addCurso(long raMaster, String senhaMaster, String codCurso,
			String nomCurso, int isDisponivel) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0) {
			gravaLog(lblLog._erroCadastro, "Professor nao logado",
					lblLog._tipoCadastro);
			return lblLog._masterNaoLogado;
		}
		try {
			Curso c = new Curso(codCurso, nomCurso, isDisponivel);
			new CursoDAO().adicionar(c);
			gravaLog(lblLog._regAdd + raMaster, lblLog._cursoAdd,
					lblLog._tipoCadastro);
			return lblLog._true;
		} catch (Exception e) {
			gravaLog(lblLog._erroCadastro + e.getMessage(), lblLog._erroMetodo
					+ "addCurso", lblLog._tipoExcecao);
			return lblLog._false + e.getMessage();
		}
	}

	public String addAluno(long raMaster, String senhaMaster, long raAluno,
			String nomAluno, String datNascimento, String email, String senha,
			String sexo) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0)
			return lblLog._masterNaoLogado;

		try {
			Aluno a = new Aluno(raAluno, nomAluno, datNascimento, email, senha,
					sexo);
				
			new AlunoDAO().adicionar(a);
			gravaLog(lblLog._regAdd + raMaster, lblLog._alunoAdd,
					lblLog._tipoCadastro);
			return lblLog._true;
		} catch (Exception e) {
			gravaLog(lblLog._erroCadastro + e.getMessage(), lblLog._erroMetodo
					+ "addAluno", lblLog._tipoExcecao);
			return lblLog._false + e.getMessage();
		}
	}

	public String addProfessor(long raMaster, String senhaMaster,
			long raProfessor, String nomProfessor, String datNascimento,
			String email, String senha, String sexo) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0)
			return lblLog._masterNaoLogado;
		try {
			Professor p = new Professor(raProfessor, nomProfessor,
					datNascimento, email, senha, sexo);
			new ProfessorDAO().adicionar(p);
			gravaLog(lblLog._regAdd + raMaster, lblLog._professorAdd,
					lblLog._tipoCadastro);
			return lblLog._true;
		} catch (Exception e) {
			gravaLog(lblLog._erroCadastro + e.getMessage(), lblLog._erroMetodo
					+ "addProfessor", lblLog._tipoExcecao);
			return lblLog._false + e.getMessage();
		}
	}

	public String addTurma(long raMaster, String senhaMaster, String codTurma,
			String nomTurma, int anoTurma, String codCurso) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0)
			return lblLog._masterNaoLogado;
		try {
			
			Turma t = new Turma(codTurma, nomTurma, anoTurma, codCurso);
			new TurmaDAO().adicionar(t);
			gravaLog(lblLog._regAdd + raMaster, lblLog._turmaAdd,
					lblLog._tipoCadastro);
			return lblLog._true;
		} catch (Exception e) {
			gravaLog(lblLog._erroCadastro + e.getMessage(), lblLog._erroMetodo
					+ "addTurma", lblLog._tipoExcecao);
			return lblLog._false + e.getMessage();
		}
	}

	public String addDisciplina(long raMaster, String senhaMaster,
			String codDisciplina, String nome) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0)
			return lblLog._masterNaoLogado;
		try {
			Disciplina d = new Disciplina(codDisciplina, nome);
			new DisciplinaDAO().adicionar(d);
			gravaLog(lblLog._regAdd + raMaster, lblLog._disciplinaAdd,
					lblLog._tipoCadastro);
			return lblLog._true;
		} catch (Exception e) {
			gravaLog(lblLog._erroCadastro + e.getMessage(), lblLog._erroMetodo
					+ "addDisciplina", lblLog._tipoExcecao);
			return lblLog._false + e.getMessage();
		}
	}

	// Remo��o---------------------------------------------------------------------
	public String removeDisciplinaAluno(long raMaster, String senhaMaster,
			long raAluno, String codDisciplina, String codTurma, int anoTurma) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0)
			return lblLog._masterNaoLogado;
		try {
			AlunoDisciplinaTurmaDAO adtDao = new AlunoDisciplinaTurmaDAO();
			AlunoDisciplinaTurmaID id = new AlunoDisciplinaTurmaID(codDisciplina, codTurma, anoTurma, raAluno);
			AlunoDisciplinaTurma disAlu = adtDao.consultarPorId(id);
			if (disAlu != null) {
				adtDao.deletarPorId(id);
				return lblLog._true;
			}
			gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
			return lblLog._disciplinaNotFound;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "removeDisciplinaAluno", lblLog._tipoExcecao);
			return lblLog._false + e.getMessage();
		}
	}

	public String removeAlunoTurma(long raMaster, String senhaMaster,
			long raAluno, String codTurma, int anoTurma) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0)
			return lblLog._masterNaoLogado;
		try {
			AlunoTurmaDAO dao = new AlunoTurmaDAO();
			AlunoTurmaID id = new AlunoTurmaID(raAluno, anoTurma, codTurma);
			AlunoTurma alunoTurma = dao.consultarPorId(id);
			if (alunoTurma == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return lblLog._alunoNotFound;
			}
			AlunoDisciplinaTurmaDAO adtDao = new AlunoDisciplinaTurmaDAO();
			List<AlunoDisciplinaTurma> disAluno = adtDao.buscarAlunoDisciplinaTurma(new AlunoDisciplinaTurma(raAluno, null, codTurma, anoTurma,Desconsiderar.tipoInt));
			if (disAluno == null)
				gravaLog(lblLog._erroQueryVazia, "Aluno sem disciplinas",lblLog._tipoQuery);
			
			adtDao.deletarPorId(disAluno);
			dao.deletarPorId(id);
			return lblLog._true;
		} catch (Exception e) {
			gravaLog(lblLog._erroQueryVazia + e.getMessage(),
					lblLog._erroMetodo + "removeAlunoTurma",
					lblLog._tipoExcecao);
			return lblLog._false + e.getMessage();
		}
	}

	// Visualiza��o----------------------------------------------------------------
	public ArrayList<String> getTurmasProfessorAno(long raMaster,
			String senhaMaster, long raProfessor, int anoTurma) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0)
			return null;
		try {
			ProfessorTurmaDAO dao = new ProfessorTurmaDAO();
			List<Turma> list = dao.buscarTurmasDoProfessor(raProfessor, new Turma(null, null, anoTurma, null));
			if (list == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> turmas = new ArrayList<String>();
			for (Turma t : list) {
				turmas.add(t.toString());
			}
			return turmas;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getDisciplinaProfessor", lblLog._tipoExcecao);
			return null;
		}
	}

	public ArrayList<String> getTurmasProfessor(long raMaster,
			String senhaMaster, long raProfessor) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0)
			return null;
		try {
			ProfessorTurmaDAO dao = new ProfessorTurmaDAO();
			List<Turma> list = dao.buscarTurmasDoProfessor(raProfessor, new Turma(null, null, Desconsiderar.tipoInt, null));
			if (list == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> turmas = new ArrayList<String>();
			for (Turma t : list) {
				turmas.add(t.toString());
			}
			return turmas;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getDisciplinaProfessor", lblLog._tipoExcecao);
			return null;
		}
	}

	public ArrayList<String> getDisciplinaProfessor(long raMaster,
			String senhaMaster, long raProfessor) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0)
			return null;
		try {
			DisciplinaProfessorDAO dpDao = new DisciplinaProfessorDAO();
			List<Disciplina> listDisciplina = dpDao.buscarDisciplinasDoProfessor(raProfessor, new Disciplina(null, null));
			if (listDisciplina == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> disciplinas = new ArrayList<String>();
			for (Disciplina d : listDisciplina) {
				disciplinas.add(d.toString());
			}
			return disciplinas;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getDisciplinaProfessor", lblLog._tipoExcecao);
			return null;
		}
	}
	
	/**
	 *  
	 * @param raMaster
	 * @param senhaMaster
	 * @param codTurma
	 * @param anoTurma
	 * @return Alunos pertencentes a Turma
	 */
	public ArrayList<String> getAlunosTurma(long raMaster, String senhaMaster,
			String codTurma, int anoTurma) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0)
			return null;
		try {
			AlunoTurmaDAO dao = new AlunoTurmaDAO();
			List<Aluno> listAlunoTurma = dao.consultarAlunosDaTurma(codTurma, anoTurma, new Aluno(Desconsiderar.tipoInt, null, "", null, null, null));
			if (listAlunoTurma == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> alunos = new ArrayList<String>();
			for (Aluno lat : listAlunoTurma) {
				alunos.add(lat.toString());
			}
			return alunos;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getAlunoTurma", lblLog._tipoExcecao);
			return null;
		}
	}

	public ArrayList<String> getCursos(long raMaster, String senhaMaster,
			int isDisponivel) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0)
			return null;
		try {
			CursoDAO dao = new CursoDAO();
			List<Curso> listCurso = dao.buscarCursos(null, null, isDisponivel);
			if (listCurso == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> alCursos = new ArrayList<String>();
			for (Curso curso : listCurso) {
				alCursos.add(curso.toString());
			}
			return alCursos;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getAlunoTurma", lblLog._tipoExcecao);
			return null;
		}
	}

	public ArrayList<String> getAlunos(long raPessoa, String senhaPessoa) {
		if (loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0
				&& loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0)
			return null;
		try {
			AlunoDAO dao = new AlunoDAO();
			List<Aluno> alunos = dao.getItensDaTabela();
			if (alunos == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> strAlu = new ArrayList<String>();
			for (Aluno a : alunos) {
				strAlu.add(a.toString());
			}
			return strAlu;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getTurma", lblLog._tipoExcecao);
			return null;
		}
	}

	public ArrayList<String> getProfessores(long raPessoa, String senhaPessoa) {
		if (loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0
				&& loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0)
			return null;
		try {
			ProfessorDAO dao = new ProfessorDAO();
			List<Professor> listProfessores = dao.getItensDaTabela();
			if (listProfessores == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> strPro = new ArrayList<String>();
			for (Professor dis : listProfessores) {
				strPro.add(dis.toString());
			}
			return strPro;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getTurma", lblLog._tipoExcecao);
			return null;
		}
	}

	public ArrayList<String> getTurmas(long raPessoa, String senhaPessoa,
			int anoTurma) {
		if (loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0
				&& loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0)
			return null;
		try {
			TurmaDAO dao = new TurmaDAO();
			List<Turma> listTurmas = dao.consultarTurmas(new Turma(null, null, anoTurma, null));
			if (listTurmas == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> strTur = new ArrayList<String>();
			for (Turma dis : listTurmas) {
				strTur.add(dis.toString());
			}
			return strTur;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getTurma", lblLog._tipoExcecao);
			return null;
		}
	}

	public ArrayList<String> getDisciplinas(long raPessoa, String senhaPessoa) {
		if (loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0
				&& loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0)
			return null;
		try {
			DisciplinaDAO dao = new DisciplinaDAO();
			List<Disciplina> listDisciplinas = dao.buscarDisciplinas(new Disciplina());
			if (listDisciplinas == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> strDis = new ArrayList<String>();
			for (Disciplina dis : listDisciplinas) {
				strDis.add(dis.toString());
			}
			return strDis;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getDisciplinas", lblLog._tipoExcecao);
			return null;
		}
	}

	public ArrayList<String> getDisciplinasTurma(long raPessoa,
			String senhaPessoa, String codTurma, int anoTurma) {
		if (loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0
				&& loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0)
			return null;
		try {
			
			TurmaDisciplinaDAO tdDao = new TurmaDisciplinaDAO(); 
			List<Disciplina> listDisciplinas = tdDao.buscarDisciplinasDaTurma(new Disciplina(null, null), codTurma, anoTurma);
			if (listDisciplinas == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> strDis = new ArrayList<String>();
			for (Disciplina dis : listDisciplinas) {
				strDis.add(dis.toString());
			}
			return strDis;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getDisciplinasTurma", lblLog._tipoExcecao);
			return null;
		}
	}
	/**
	 * 
	 * @param raMaster
	 * @param senhaMaster
	 * @param codCurso
	 * @param raAluno
	 * @return Turmas pertencentes ao Aluno
	 */
	public ArrayList<String> getTurmasCursoAluno(long raMaster,
			String senhaMaster, String codCurso, long raAluno) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0
				&& loggin.isAlunoLogado(raMaster, senhaMaster) == 0)
			return null;
		try {
			AlunoTurmaDAO atd = new AlunoTurmaDAO();			
			List<Turma> listAlunoTurma = atd.consultarTurmasDoAluno(raAluno, new Turma(null, null, Desconsiderar.tipoInt, codCurso));
			if (listAlunoTurma == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> strAlunoTurma = new ArrayList<String>();
			for (Turma turma : listAlunoTurma) {
				strAlunoTurma.add(new Gson().toJson(turma));
			}
			return strAlunoTurma;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getTurmasAluno", lblLog._tipoExcecao);
			return null;
		}
	}

	/**
	 * 
	 * @param raMaster
	 * @param senhaMaster
	 * @param raAluno
	 * @return Turmas pertencentes ao Aluno
	 */
	public ArrayList<String> getTurmasAluno(long raMaster, String senhaMaster,
			long raAluno) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0
				&& loggin.isAlunoLogado(raMaster, senhaMaster) == 0)
			return null;
		try {
			AlunoTurmaDAO dao = new AlunoTurmaDAO();
			List<Turma> listAlunoTurma = dao.consultarTurmasDoAluno(raAluno, new Turma(null, null, Desconsiderar.tipoInt, null));
			if (listAlunoTurma == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> strAlunoTurma = new ArrayList<String>();
			for (Turma turma : listAlunoTurma) {
				strAlunoTurma.add(new Gson().toJson(turma.toString()));
			}
			return strAlunoTurma;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getTurmasAluno", lblLog._tipoExcecao);
			return null;
		}
	}

	public ArrayList<String> getDisciplinaAlunoTurma(long raPessoa,
			String senhaPessoa, String codTurma, long raAluno, int status) {
		if (loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0
				&& loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0)
			return null;
		try {
			AlunoDisciplinaTurmaDAO adtDao = new AlunoDisciplinaTurmaDAO();
			List<Disciplina> listAlunoDisc = 
					adtDao.buscarDisciplinasDoAluno(new AlunoDisciplinaTurma(raAluno, null, codTurma, Desconsiderar.tipoInt, status), new Disciplina());
			if (listAlunoDisc == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> strAlunoTurma = new ArrayList<String>();
			for (Disciplina dis : listAlunoDisc) {
				strAlunoTurma.add(new Gson().toJson(dis));
			}
			return strAlunoTurma;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getDisciplinasAluno", lblLog._tipoExcecao);
			return null;
		}
	}

	public ArrayList<String> getCursoAluno(long raPessoa, String senhaPessoa,
			long raAluno) {
		if (loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0
				&& loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0)
			return null;
		try {
			AlunoCursoDAO dao = new AlunoCursoDAO();
			List<Curso> cursosAluno = dao.buscarCursosPorAluno(new Curso(null, null, Desconsiderar.tipoInt), raAluno);
			if (cursosAluno == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return null;
			}
			ArrayList<String> arrayList = new ArrayList<String>();
			for (Curso alunoCurso : cursosAluno) {
				arrayList.add(new Gson().toJson(alunoCurso));
			}
			return arrayList;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), lblLog._erroMetodo
					+ "getDisciplinasAluno", lblLog._tipoExcecao);
			return null;
		}
	}

	// Atribui��o------------------------------------------------------------------
	public String setAlunoCurso(long raMaster, String senhaMaster,
			long raAluno, String codCurso, int anoIngresso) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0)
			return lblLog._masterNaoLogado;
		try {
			AlunoCursoDAO acDao = new AlunoCursoDAO();
			AlunoCurso ac = new AlunoCurso(codCurso, anoIngresso, raAluno);
			if (acDao.consultarPorId(ac.getId()) != null) {
				gravaLog(lblLog._AlunoCursoAdded, null, lblLog._tipoCadastro);
				return lblLog._AlunoCursoAdded;
			}
			acDao.adicionar(ac);
			gravaLog(lblLog._atribuicaoAdd + raMaster, "Curso => Aluno",
					lblLog._tipoCadastro);
			return lblLog._true;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), "setAlunoCurso",
					lblLog._tipoExcecao);
			return lblLog._false + e.getMessage();
		}
	}

	public String setProfessorTurma(long raMaster, String senhaMaster,
			long raProfessor, String codTurma, int anoTurma) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0)
			return lblLog._masterNaoLogado;
		try {
			TurmaDAO tDao = new TurmaDAO();
			ProfessorDAO pDao = new ProfessorDAO();
			ProfessorTurmaDAO ptDao = new ProfessorTurmaDAO();
			Professor professor = pDao.consultarPorId(raProfessor);
			
			if(professor == null){
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return lblLog._professorNotFound;
			}
			Turma turma = tDao.consultarPorId(new TurmaID(codTurma, anoTurma));
			if (turma == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return lblLog._turmaNotFound;
			}
			ProfessorTurma pt = new ProfessorTurma
					(raProfessor,turma.getId().getCodigoTurma(),turma.getId().getAnoTurma(), turma.getCodCurso());
			ptDao.adicionar(pt);
			gravaLog(lblLog._atribuicaoAdd + raMaster, "Professor => Turma",
					lblLog._tipoCadastro);
			return lblLog._true;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), "setProfessorTurma",
					lblLog._tipoExcecao);
			return lblLog._false + e.getMessage();
		}
	}

	public String setDisciplinaProfessor(long raMaster, String senhaMaster,
			String codDisciplina, long raProfessor) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0)
			return lblLog._masterNaoLogado;
		try {
			DisciplinaDAO dDao = new DisciplinaDAO();
			ProfessorDAO pDao = new ProfessorDAO();
			DisciplinaProfessorDAO dpDao = new DisciplinaProfessorDAO();
			Disciplina di = dDao.consultarPorId(codDisciplina);
			if (di == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return lblLog._disciplinaNotFound;
			}
			
			Professor professor = pDao.consultarPorId(raProfessor);
			if (professor == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return lblLog._professorNotFound;
			}
			DisciplinaProfessor dp = new DisciplinaProfessor(professor.getRA(),
					di.getCodigoDisciplina());
			dpDao.adicionar(dp);
			gravaLog(lblLog._atribuicaoAdd + raMaster,
					"Disciplina => Professor", lblLog._tipoCadastro);
			return lblLog._true;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(),
					"setDisciplinaProfessor", lblLog._tipoExcecao);
			return lblLog._false + e.getMessage();
		}
	}

	public String setDisciplinaTurma(long raMaster, String senhaMaster,
			String codDisciplina, String codTurma, int anoTurma) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0)
			return lblLog._masterNaoLogado;
		try {
			DisciplinaDAO dDao = new DisciplinaDAO();
			TurmaDAO tDao = new TurmaDAO();
			TurmaDisciplinaDAO tdDao = new TurmaDisciplinaDAO();
			Disciplina disciplina = dDao.consultarPorId(codDisciplina);
			if (disciplina == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return lblLog._disciplinaNotFound;
			}
			
			Turma turma = (Turma) tDao.consultarPorId(new TurmaID(codTurma, anoTurma));
			if (turma == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return lblLog._turmaNotFound;
			}
			TurmaDisciplina dt = new TurmaDisciplina(
					disciplina.getCodigoDisciplina(), turma.getId().getCodigoTurma(),
					turma.getId().getAnoTurma());
			tdDao.adicionar(dt);
			gravaLog(lblLog._atribuicaoAdd + raMaster, "Disciplina => Turma",
					lblLog._tipoCadastro);
			return lblLog._true;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), "setDisciplinaTurma",
					lblLog._tipoExcecao);
			return lblLog._false + e.getMessage();
		}
	}

	public String setAlunoTurma(long raMaster, String senhaMaster,
			long raAluno, String codTurma, int anoTurma, String codCurso) {
		if (loggin.isProfessorLogado(raMaster, senhaMaster) == 0)
			return lblLog._masterNaoLogado;
		try {
			AlunoDAO aDao = new AlunoDAO();
			TurmaDAO tDao = new TurmaDAO();
			AlunoTurmaDAO atDao = new AlunoTurmaDAO();
			TurmaDisciplinaDAO tdDao = new TurmaDisciplinaDAO();
			AlunoDisciplinaTurmaDAO adtDao = new AlunoDisciplinaTurmaDAO();
			Aluno aluno = (Aluno) aDao.consultarPorId(raAluno);
			if (aluno == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return lblLog._alunoNotFound;
			}
			Turma turma = tDao.consultarPorId(new TurmaID(codTurma, anoTurma));

			if (turma == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return lblLog._turmaNotFound;
			}

			// Adiciona aluno na turma;
			AlunoTurma at = new AlunoTurma(aluno.getRA(),
					turma.getId().getCodigoTurma(), turma.getId().getAnoTurma());
			atDao.adicionar(at);
			
			List<TurmaDisciplina> dp = tdDao.buscarTurmaDisciplina(new TurmaDisciplina(null, codTurma, anoTurma));
			for (TurmaDisciplina laco : dp) {
				AlunoDisciplinaTurma tad = new AlunoDisciplinaTurma(aluno.getRA(), laco.getId().getCodDisciplina(),turma.getId().getCodigoTurma(), turma.getId().getAnoTurma(), 1);
				adtDao.adicionar(tad);
			}
			gravaLog(lblLog._atribuicaoAdd + raMaster,
					"Aluno => Turma | Disciplianas => Aluno",
					lblLog._tipoCadastro);
			return lblLog._true;
		} catch (Exception e) {
			gravaLog(lblLog._erroQuery + e.getMessage(), "setAlunoTurma",
					lblLog._tipoExcecao);
			return lblLog._false + e.getMessage();
		}
	}
	
	public String getAluno(long raPessoa, String senhaPessoa, long raAluno){
		if (loggin.isAlunoLogado(raPessoa, senhaPessoa) == 0
				&& loggin.isProfessorLogado(raPessoa, senhaPessoa) == 0)
			return null;
		try{
			AlunoDAO dao = new AlunoDAO();
			Aluno aluno = dao.consultarPorId(raAluno);
			if(aluno == null){
				gravaLog(lblLog._erroQueryVazia, "getAluno", lblLog._tipoQuery);
				return null;
			}
			GsonBuilder builder = new GsonBuilder().setDateFormat("dd/MM/yyyy");
			return builder.create().toJson(aluno);
		}
		catch(Exception ex){
			gravaLog(lblLog._erroMetodo + ex.getMessage(), "getAluno",
					lblLog._tipoExcecao);
			return lblLog._false + ex.getMessage();
		}
		
		
	}

	// Login-----------------------------------------------------------------------
	public String loginAluno(long raAluno, String senhaAluno) {
		try {
			String mensagem = loggin.loginAluno(raAluno, senhaAluno);
			return mensagem;
		} catch (Exception e) {
			return lblLog._false + e.getMessage();
		}
	}

	public String loginProfessor(long raProfessor, String senhaProfessor) {
		try {
			if (loggin.loginProfessor(raProfessor, senhaProfessor))
				return lblLog._true;
			else
				return lblLog._false;
		} catch (Exception e) {
			return lblLog._false + e.getMessage();
		}
	}

	// Logoff----------------------------------------------------------------------
	public String logoffAluno(long raAluno, String senhaAluno) {
		try {
			if (loggin.alunoFinalizaSessao(raAluno, senhaAluno))
				return lblLog._true;
			else
				return lblLog._false;
		} catch (Exception e) {
			return lblLog._false + e.getMessage();
		}
	}

	public String logoffProfessor(long raProfessor, String senhaProfessor) {
		try {
			if (loggin.professorFinalizaSessao(raProfessor, senhaProfessor))
				return lblLog._true;
			else
				return lblLog._false;
		} catch (Exception e) {
			return lblLog._false + e.getMessage();
		}
	}

	// FIM-------------------------------------------------------------------------
	// Grava logs;
	private void gravaLog(String dadosLog, String detalhe, int tipo) {
		try {
			Log log = new Log(dadosLog, detalhe, tipo);
			new LogDAO().adicionar(log);
		} catch (Exception e) {
			return;
		}
	}
}
