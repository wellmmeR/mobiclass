package jpa.DAO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import web.src.entity.PersistenceFactory;

public abstract class DAO<T,ID> implements Serializable{

	private static final long serialVersionUID = -3714836088341646149L;
	
	public static EntityManagerFactory factory;
	
	public DAO(){
		factory = getPersistenceFactory().getFactory();
		c = getType();
	}
	
	public abstract Class<T> getType();
	private Class<T> c;
	
	public void deletarRelacionamentos(ID id) throws Exception{};
	public void deletarRelacionamentos(List<T> lista) throws Exception{};
	
	public void adicionar(T object) throws Exception{
		EntityManager em = factory.createEntityManager();
		try{
			em.getTransaction().begin();
			em.persist(object);
			em.getTransaction().commit();
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			throw ex;
		}
		finally{
			em.close();
		}
		
	}
	
	public void atualizar(T object) throws Exception{
		EntityManager em = factory.createEntityManager();
		try{
			em.getTransaction().begin();
			em.merge(object);
			em.getTransaction().commit();
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			throw ex;
		}
		finally{
			em.close();
		}
	}
	
	public T consultarPorId(ID id) throws Exception{
		return factory.createEntityManager().find(getType(), id);
	}
	
	public void deletarPorId(ID id) throws Exception{
		EntityManager em = factory.createEntityManager();
		try{
			deletarRelacionamentos(id);
			em.getTransaction().begin();
			Object o = em.getReference(c, id);
			em.remove(o);
			em.getTransaction().commit();
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			throw ex;
		}
		finally{
			em.close();
		}
	}
	
	public void deletarPorId(List<T> lista) throws Exception{
		EntityManager em = factory.createEntityManager();
		if(lista!=null && !lista.isEmpty()){	
		
			try{
				deletarRelacionamentos(lista);
				em.getTransaction().begin();
				CriteriaBuilder cb = em.getCriteriaBuilder();
				CriteriaDelete<T> criteria = cb.createCriteriaDelete(c);
				Root<T> e = criteria.from(c);
				criteria.where(e.in(lista));
				Query q = em.createQuery(criteria);
				q.executeUpdate();
				em.getTransaction().commit();
			}
			catch(Exception ex){
				ex.printStackTrace();
				System.out.println(ex.getMessage());
				throw ex;
			}
			finally{
				em.close();
			}
		}
	}
	
	public void adicionar(List<T> lista) throws Exception{
		EntityManager em = factory.createEntityManager();
		try{
			em.getTransaction().begin();
			for(T object : lista){
				em.persist(object);
			}
			em.getTransaction().commit();
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			throw ex;
		}
		finally{
			em.close();
		}
	}
	
	public void atualizar(List<T> lista) throws Exception{
		EntityManager em = factory.createEntityManager();
		try{
			em.getTransaction().begin();
			for(T object : lista){
				em.merge(object);
			}
			em.getTransaction().commit();
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			throw ex;
		}
		finally{
			em.close();
		}
	}
	
	public List<T> getItensDaTabela(){
		EntityManager em = factory.createEntityManager();
		CriteriaQuery<T> cq = em.getCriteriaBuilder().createQuery(c);
		cq.select(cq.from(c));
		return em.createQuery(cq).getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	public List<T> buscarPorAtributos(Map<String, Object> atributos) throws Exception{
		EntityManager em = factory.createEntityManager();
		try{
			List<T> resultado;
	        //set up the Criteria query
	        CriteriaBuilder cb = em.getCriteriaBuilder();
	        CriteriaQuery<T> query = cb.createQuery(c);
	        Root<T> root = query.from(c);
	 
	        List<Predicate> predicates = new ArrayList<Predicate>();
	        for(String s : atributos.keySet())
	        {
	            if(root.get(s) != null){
	                if(atributos.get(s) instanceof Map){
	                	
						Map<String, Object> subMapa =(Map<String,Object>) atributos.get(s);
	                	for(String subString: subMapa.keySet()){
	                		if(subMapa.get(subString) != null)
	                			predicates.add(cb.equal(root.get(s).get(subString), subMapa.get(subString)));
	                	}
	                }
	                else{
	                	if(atributos.get(s)!=null)
	                		predicates.add(cb.equal(root.get(s), atributos.get(s)));
	                }
	            	
	            }
	        }
	        query.where(predicates.toArray(new Predicate[]{}));
	        TypedQuery<T> q = em.createQuery(query);
	 
	        resultado = q.getResultList();
	        return resultado;
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			throw ex;
		}
		finally{
			em.close();
		}
		
    }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Deprecated
	public List<T> consultarPorJPQL(String jpql){
		EntityManager em = factory.createEntityManager();
		List lista = null;
		try{
			Query query = em.createQuery(jpql);
			lista = query.getResultList();
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage());
		}
		finally{
			em.close();
		}
		return lista;
	}

	public PersistenceFactory getPersistenceFactory() {
		FacesContext context = FacesContext.getCurrentInstance();
		if(context == null)
			return new PersistenceFactory();
		return context.getApplication().evaluateExpressionGet(context, "#{persistenceFactory}", PersistenceFactory.class);
	}
	
}
