package jpa.DAO;

import java.util.List;

import javax.persistence.TypedQuery;

import jpa.atividade.AreaAtividade;

public class AreaAtividadeDAO extends DAO<AreaAtividade, Long>{

	private static final long serialVersionUID = 1L;

	@Override
	public Class<AreaAtividade> getType() {
		return AreaAtividade.class;
	}
	
	public long primeiroIdDisponivel(){
		String jpql = "select a.id from AreaAtividade a";
		TypedQuery<Long> q = factory.createEntityManager().createQuery(jpql, Long.class);
		List<Long> codigos = q.getResultList();
		if(codigos.size()>0)
		{
			long raDisponivel = 1;
			for(Long ra : codigos){
				if(ra > raDisponivel)
					break;
				raDisponivel++;
			}
			return raDisponivel;
		}
		else
			return 1L;
	}

}
