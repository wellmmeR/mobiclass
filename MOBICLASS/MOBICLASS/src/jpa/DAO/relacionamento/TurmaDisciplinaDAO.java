package jpa.DAO.relacionamento;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.DAO.DAO;
import jpa.academico.Disciplina;
import jpa.academico.Turma;
import jpa.academico.TurmaDisciplina;
import jpa.academico.TurmaDisciplinaID;
import jpa.util.Condicao;
import jpa.util.CondicaoEnum;
import jpa.util.Where;

public class TurmaDisciplinaDAO extends DAO<TurmaDisciplina, TurmaDisciplinaID>{

	private static final long serialVersionUID = 6702896938025912487L;
	
	@Override
	public Class<TurmaDisciplina> getType() {
		return TurmaDisciplina.class;
	}
	
	public List<Disciplina> buscarDisciplinasDaTurma(Disciplina d, String codTurma, Integer anoTurma) throws Exception{
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("d.codigoDisciplina", "td.id.codDisciplina", true));
		condicoes.add(new Condicao("td.id.codTurma", codTurma));
		condicoes.add(new Condicao("td.id.anoTurma", anoTurma));
		if(d != null)
		{
			condicoes.add(new Condicao("d.nome", d.getNome(), CondicaoEnum.LIKE));
			condicoes.add(new Condicao("d.codigoDisciplina", d.getCodigoDisciplina()));
		}
		
		Where w = Where.criarWhere(condicoes);
		EntityManager em = factory.createEntityManager();
		String jpql = "SELECT d FROM Disciplina d, TurmaDisciplina td " + w.getStringWhere();
		TypedQuery<Disciplina> query = em.createQuery(jpql, Disciplina.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<Disciplina> disciplinas = query.getResultList();
		em.close();
		return disciplinas;
		
	}
	
	public List<Turma> buscarTurmasDaDisciplina(Turma t, String codDisciplina){
		
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("t.id.codigoTurma", "td.id.codTurma", true));
		condicoes.add(new Condicao("t.id.anoTurma", "td.id.anoTurma", true));
		condicoes.add(new Condicao("td.id.codDisciplina", codDisciplina));
		if(t != null)
		{
			condicoes.add(new Condicao("t.id.codigoTurma", t.getId().getCodigoTurma()));
			condicoes.add(new Condicao("t.id.anoTurma", t.getId().getAnoTurma()));
			condicoes.add(new Condicao("t.codCurso", t.getCodCurso()));
			condicoes.add(new Condicao("t.nomeTurma", t.getNomeTurma(), CondicaoEnum.LIKE));
		}
		
		Where w = Where.criarWhere(condicoes);
		EntityManager em = factory.createEntityManager();
		String jpql = "SELECT t FROM Turma t, TurmaDisciplina td " + w.getStringWhere();
		TypedQuery<Turma> query = em.createQuery(jpql, Turma.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<Turma> turmas = query.getResultList();
		em.close();
		return turmas;
	}
	
	public List<TurmaDisciplina> buscarTurmaDisciplina(TurmaDisciplina td) throws Exception{
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("td.id.codDisciplina", td.getId().getCodDisciplina()));
		condicoes.add(new Condicao("td.id.codTurma", td.getId().getCodTurma()));
		condicoes.add(new Condicao("td.id.anoTurma", td.getId().getAnoTurma()));
		
		Where w = Where.criarWhere(condicoes);
		EntityManager em = factory.createEntityManager();
		String jpql = "SELECT td FROM TurmaDisciplina td ";
		TypedQuery<TurmaDisciplina> query = em.createQuery(jpql, TurmaDisciplina.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<TurmaDisciplina> listaDeTurmaDisciplina = query.getResultList();
		em.close();
		return listaDeTurmaDisciplina;
	}
}
