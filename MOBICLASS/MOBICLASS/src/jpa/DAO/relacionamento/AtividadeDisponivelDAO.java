package jpa.DAO.relacionamento;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.DAO.DAO;
import jpa.atividade.AreaAtividade;
import jpa.atividade.Atividade;
import jpa.atividade.AtividadeDisponivel;
import jpa.atividade.AtividadeDisponivelID;
import jpa.util.Condicao;
import jpa.util.Where;

public class AtividadeDisponivelDAO extends DAO<AtividadeDisponivel, AtividadeDisponivelID>{

	private static final long serialVersionUID = 4138910158932369643L;

	@Override
	public Class<AtividadeDisponivel> getType() {

		return AtividadeDisponivel.class;
	}
	
	@Override
	public void deletarRelacionamentos(AtividadeDisponivelID id) throws Exception{
		AlunoAtividadeDAO aaDao = new AlunoAtividadeDAO();
		
		Map<String, Object> mapaDeAtributos = new HashMap<>();
		Map<String, Object> subMapa= new HashMap<>();
		
		subMapa.put("anoTurma", id.getAnoTurma());
		subMapa.put("codTurma", id.getCodTurma());
		subMapa.put("areaAtividade", id.getAreaAtividade());
		subMapa.put("codAtividade", id.getCodAtividade());
		subMapa.put("codDisciplina", id.getCodDisciplina());
		mapaDeAtributos.put("id", subMapa);
		aaDao.deletarPorId(aaDao.buscarPorAtributos(mapaDeAtributos));
	}

	@Override
	public void deletarRelacionamentos(List<AtividadeDisponivel> lista) throws Exception{
		for(AtividadeDisponivel atividadeDisponivel: lista)
			deletarRelacionamentos(atividadeDisponivel.getId());
	}
	
	public List<Atividade> buscarAtividadesPorAtividadesDisponiveis(AtividadeDisponivel ad){
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("a.idAtividade.codigoAtividade","ad.id.codAtividade", true));
		condicoes.add(new Condicao("a.idAtividade.codigoDisciplina", "ad.id.codDisciplina", true));
		condicoes.add(new Condicao("a.idAtividade.areaAtividade", "ad.id.areaAtividade", true));
		condicoes.add(new Condicao("ad.id.codAtividade", ad.getId().getCodAtividade()));
		condicoes.add(new Condicao("ad.id.codDisciplina", ad.getId().getCodDisciplina()));
		condicoes.add(new Condicao("ad.id.areaAtividade", ad.getId().getAreaAtividade()));
		condicoes.add(new Condicao("ad.id.codTurma", ad.getId().getCodTurma()));
		condicoes.add(new Condicao("ad.id.anoTurma", ad.getId().getAnoTurma()));

		Where w = Where.criarWhere(condicoes);
		String jpql = "SELECT a FROM Atividade a, AtividadeDisponivel ad " + w.getStringWhere();
		EntityManager em = factory.createEntityManager();
		TypedQuery<Atividade> query = em.createQuery(jpql, Atividade.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<Atividade> lista = query.getResultList();
		em.close();
		return lista;
	}
	
	public List<AtividadeDisponivel> buscarAtividadesDisponiveis(AtividadeDisponivel ad){
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("ad.id.codAtividade", ad.getId().getCodAtividade()));
		condicoes.add(new Condicao("ad.id.codDisciplina", ad.getId().getCodDisciplina()));
		condicoes.add(new Condicao("ad.id.areaAtividade", ad.getId().getAreaAtividade()));
		condicoes.add(new Condicao("ad.id.codTurma", ad.getId().getCodTurma()));
		condicoes.add(new Condicao("ad.id.anoTurma", ad.getId().getAnoTurma()));
		condicoes.add(new Condicao("ad.id.isDisponivel", ad.getIsDisponivel()));
		condicoes.add(new Condicao("ad.id.valorAtividade", ad.getValorAtividade()));
		condicoes.add(new Condicao("ad.dataLiberacao", ad.getDataLiberacao()));
		condicoes.add(new Condicao("ad.dataEncerramento", ad.getDataEncerramento()));

		Where w = Where.criarWhere(condicoes);
		String jpql = "SELECT ad FROM AtividadeDisponivel ad " + w.getStringWhere();
		EntityManager em = factory.createEntityManager();
		TypedQuery<AtividadeDisponivel> query = em.createQuery(jpql, AtividadeDisponivel.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<AtividadeDisponivel> lista = query.getResultList();
		em.close();
		return lista;
	}
	
	public AtividadeDisponivel buscarSingleAtividadeDisponivel(AtividadeDisponivel ad) throws Exception{
		List<AtividadeDisponivel> lista = buscarAtividadesDisponiveis(ad);
		if(lista == null || lista.isEmpty())
			return null;
		return lista.get(0);
	}
	
	public List<AreaAtividade> buscarAreaAtividadeDaAtividadeDisponivel(AtividadeDisponivel ad){
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("ar.id", "ad.id.areaAtividade", true));
		condicoes.add(new Condicao("ad.id.codAtividade", ad.getId().getCodAtividade()));
		condicoes.add(new Condicao("ad.id.codDisciplina", ad.getId().getCodDisciplina()));
		condicoes.add(new Condicao("ad.id.areaAtividade", ad.getId().getAreaAtividade()));
		condicoes.add(new Condicao("ad.id.codTurma", ad.getId().getCodTurma()));
		condicoes.add(new Condicao("ad.id.anoTurma", ad.getId().getAnoTurma()));

		Where w = Where.criarWhere(condicoes);
		String jpql = "SELECT ar FROM AtividadeDisponivel ad, AreaAtividade ar " + w.getStringWhere();
		EntityManager em = factory.createEntityManager();
		TypedQuery<AreaAtividade> query = em.createQuery(jpql, AreaAtividade.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<AreaAtividade> lista = query.getResultList();
		em.close();
		return lista;
	}
}
