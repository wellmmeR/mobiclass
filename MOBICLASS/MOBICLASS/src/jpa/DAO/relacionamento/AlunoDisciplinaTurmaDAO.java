package jpa.DAO.relacionamento;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.DAO.DAO;
import jpa.academico.AlunoDisciplinaTurma;
import jpa.academico.AlunoDisciplinaTurmaID;
import jpa.academico.Disciplina;
import jpa.pessoa.Aluno;
import jpa.util.Condicao;
import jpa.util.CondicaoEnum;
import jpa.util.Where;

public class AlunoDisciplinaTurmaDAO extends DAO<AlunoDisciplinaTurma, AlunoDisciplinaTurmaID>{

	private static final long serialVersionUID = 2093979395109588213L;

	@Override
	public Class<AlunoDisciplinaTurma> getType() {
		return AlunoDisciplinaTurma.class;
	}
	
	public List<Disciplina> buscarDisciplinasDoAluno(AlunoDisciplinaTurma adt, Disciplina d) throws Exception{
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("d.codigoDisciplina", "adt.id.codDisciplina" ,true));
		condicoes.add(new Condicao("adt.id.RA", adt.getId().getRA()));
		condicoes.add(new Condicao("adt.id.codTurma", adt.getId().getCodTurma()));
		condicoes.add(new Condicao("adt.id.anoTurma", adt.getId().getAnoTurma()));
		condicoes.add(new Condicao("d.codigoDisciplina", d.getCodigoDisciplina()));
		condicoes.add(new Condicao("d.nome", d.getNome(), CondicaoEnum.LIKE));
		Where w = Where.criarWhere(condicoes);
		
		EntityManager em = factory.createEntityManager();
		String jqpl = "SELECT DISTINCT(d) FROM Disciplina d, AlunoDisciplinaTurma adt " + w.getStringWhere();
		TypedQuery<Disciplina> query = em.createQuery(jqpl, Disciplina.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<Disciplina> lista = query.getResultList();
		em.close();
		return lista;
	
		
	}
	
	public List<AlunoDisciplinaTurma> buscarAlunoDisciplinaTurma(AlunoDisciplinaTurma adt) throws Exception{
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("adt.id.codDisciplina", adt.getId().getCodDisciplina()));
		condicoes.add(new Condicao("adt.id.codTurma", adt.getId().getCodTurma()));
		condicoes.add(new Condicao("adt.id.anoTurma", adt.getId().getAnoTurma()));
		condicoes.add(new Condicao("adt.id.RA", adt.getId().getRA()));
		Where w = Where.criarWhere(condicoes);
		
		EntityManager em = factory.createEntityManager();
		String jpql = "SELECT adt FROM AlunoDisciplinaTurma adt " + w.getStringWhere();
		TypedQuery<AlunoDisciplinaTurma> query = em.createQuery(jpql, AlunoDisciplinaTurma.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<AlunoDisciplinaTurma> lista = query.getResultList();
		em.close();
		return lista;
		
	}
	
	public List<Aluno> buscarAlunosDaDisciplinaETurma(AlunoDisciplinaTurma adt){
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("adt.id.codDisciplina", adt.getId().getCodDisciplina()));
		condicoes.add(new Condicao("adt.id.codTurma", adt.getId().getCodTurma()));
		condicoes.add(new Condicao("adt.id.anoTurma", adt.getId().getAnoTurma()));
		condicoes.add(new Condicao("adt.id.RA", adt.getId().getRA()));
		condicoes.add(new Condicao("adt.statusDis", adt.getStatusDis()));
		condicoes.add(new Condicao("a.RA", "adt.id.RA", true));
		Where w = Where.criarWhere(condicoes);
		
		EntityManager em = factory.createEntityManager();
		String jpql = "SELECT a FROM Aluno a, AlunoDisciplinaTurma adt " + w.getStringWhere();
		TypedQuery<Aluno> query = em.createQuery(jpql, Aluno.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<Aluno> lista = query.getResultList();
		em.close();
		return lista;
	}

}
