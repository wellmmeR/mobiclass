package jpa.DAO.relacionamento;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.DAO.DAO;
import jpa.academico.AlunoTurma;
import jpa.academico.AlunoTurmaID;
import jpa.academico.Turma;
import jpa.pessoa.Aluno;
import jpa.util.Condicao;
import jpa.util.CondicaoEnum;
import jpa.util.Where;

public class AlunoTurmaDAO extends DAO<AlunoTurma, AlunoTurmaID>{
	
	private static final long serialVersionUID = 6941858324646826537L;

	@Override
	public Class<AlunoTurma> getType() {
		return AlunoTurma.class;
	}
	
	public List<Turma> consultarTurmasDoAluno(Long raAluno, Turma t) throws Exception{
		EntityManager em = factory.createEntityManager();
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("at.id.codTurma", "t.id.codigoTurma", true));
		condicoes.add(new Condicao("at.id.anoTurma", "t.id.anoTurma", true));
		condicoes.add(new Condicao("at.id.RA", raAluno));
		condicoes.add(new Condicao("t.nomeTurma", t.getNomeTurma(), CondicaoEnum.LIKE));
		condicoes.add(new Condicao("t.id.codigoTurma", t.getId().getCodigoTurma(), CondicaoEnum.EQUALS));
		condicoes.add(new Condicao("t.id.anoTurma", t.getId().getAnoTurma(), CondicaoEnum.EQUALS));
		condicoes.add(new Condicao("t.codCurso", t.getCodCurso(), CondicaoEnum.EQUALS));
		Where w = Where.criarWhere(condicoes);
		String jpql = "SELECT t FROM Turma t, AlunoTurma at " + w.getStringWhere();
		TypedQuery<Turma> query = em.createQuery(jpql, Turma.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<Turma> turmas = query.getResultList();
		em.close();
		return turmas;
	}

	public List<Aluno> consultarAlunosDaTurma(String codTurma, Integer anoTurma, Aluno a) throws Exception{
		EntityManager em = factory.createEntityManager();
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("at.id.RA", "a.RA", true));
		condicoes.add(new Condicao("at.id.codTurma", codTurma));
		condicoes.add(new Condicao("at.id.anoTurma", anoTurma));
		Where w = Where.criarWhere(condicoes);
		String jpql = "SELECT a FROM Aluno a, AlunoTurma at " + w.getStringWhere();
		TypedQuery<Aluno> query = em.createQuery(jpql, Aluno.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<Aluno> alunos = query.getResultList();
		em.close();
		return alunos;
	}
	
	public List<AlunoTurma> consultarAlunoTurma(AlunoTurma at) throws Exception{
		EntityManager em = factory.createEntityManager();
		List<Condicao> condicoes = new ArrayList<Condicao>();
		if(at!=null)
		{
			condicoes.add(new Condicao("at.id.codTurma", at.getId().getCodTurma()));
			condicoes.add(new Condicao("at.id.anoTurma", at.getId().getAnoTurma()));
			condicoes.add(new Condicao("at.id.RA", at.getId().getRA()));
		}
		Where w = Where.criarWhere(condicoes);
		String jpql = "SELECT at FROM AlunoTurma at " + w.getStringWhere();
		TypedQuery<AlunoTurma> query = em.createQuery(jpql, AlunoTurma.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<AlunoTurma> turmas = query.getResultList();
		em.close();
		return turmas;
	}
	
}
