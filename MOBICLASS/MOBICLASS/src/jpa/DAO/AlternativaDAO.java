package jpa.DAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;

import jpa.atividade.AlternativaQuestao;
import jpa.atividade.Questao;
import jpa.util.Condicao;
import jpa.util.Where;

public class AlternativaDAO extends DAO<AlternativaQuestao, Long>{

	private static final long serialVersionUID = -4662259154384270059L;

	@Override
	public Class<AlternativaQuestao> getType() {
		return AlternativaQuestao.class;
	}
	
	public void deletarTodasAlternativasDaQuestao(Questao q){
		EntityManager em = Persistence.createEntityManagerFactory("ServerAndroid").createEntityManager();
		em.getTransaction().begin();
		
		TypedQuery<AlternativaQuestao> query = em.createQuery("delete from AlternativaQuestao a where a.codPergunta = :codPergunta", AlternativaQuestao.class);
		query.setParameter("codPergunta", q.getCodPergunta());
		query.executeUpdate();
		em.getTransaction().commit();
	}
	
	public void deletarTodasAlternativasDaQuestoes(List<Questao> questoes){
		List<Long> listaCodigos = new ArrayList<>();
		for(Questao q: questoes){
			listaCodigos.add(q.getCodPergunta());
		}
		EntityManager em = Persistence.createEntityManagerFactory("ServerAndroid").createEntityManager();
		em.getTransaction().begin();
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaDelete<AlternativaQuestao> criteria = cb.createCriteriaDelete(AlternativaQuestao.class);
		Root<AlternativaQuestao> e =  criteria.from(AlternativaQuestao.class);
		
		
		criteria.where(e.get("codPergunta").in(listaCodigos));
		
		Query q = em.createQuery(criteria);
		q.executeUpdate();
		
		em.getTransaction().commit();

	}
	
	public List<AlternativaQuestao> buscarAlternativas(AlternativaQuestao aq) throws Exception{
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("aq.ID",aq.getID()));
		condicoes.add(new Condicao("aq.codPergunta",aq.getCodPergunta()));
		condicoes.add(new Condicao("aq.nomeAlternativa",aq.getNomeAlternativa()));
		condicoes.add(new Condicao("aq.isCorreta",aq.getIsCorreta()));
		Where w = Where.criarWhere(condicoes);
		String jpql = "SELECT aq FROM AlternativaQuestao aq " + w.getStringWhere();
		EntityManager em = factory.createEntityManager();
		TypedQuery<AlternativaQuestao> query = em.createQuery(jpql,AlternativaQuestao.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<AlternativaQuestao> lista = query.getResultList();
		em.close();
		return lista;
	}

}
