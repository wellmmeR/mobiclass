package jpa.DAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.DAO.relacionamento.AlunoDisciplinaTurmaDAO;
import jpa.DAO.relacionamento.AlunoTurmaDAO;
import jpa.DAO.relacionamento.AtividadeDisponivelDAO;
import jpa.DAO.relacionamento.ProfessorTurmaDAO;
import jpa.DAO.relacionamento.TurmaDisciplinaDAO;
import jpa.academico.Curso;
import jpa.academico.Turma;
import jpa.academico.TurmaID;
import jpa.util.Condicao;
import jpa.util.CondicaoEnum;
import jpa.util.Where;

public class TurmaDAO extends DAO<Turma, TurmaID>{
	
	private static final long serialVersionUID = 1L;

	@Override
	public Class<Turma> getType() {
		return Turma.class;
	}
	
	@Override
	public void deletarRelacionamentos(TurmaID id) throws Exception{
		AtividadeDisponivelDAO adDao = new AtividadeDisponivelDAO();
		AlunoTurmaDAO atDao = new AlunoTurmaDAO();
		AlunoDisciplinaTurmaDAO adtDao = new AlunoDisciplinaTurmaDAO();
		ProfessorTurmaDAO ptDao = new ProfessorTurmaDAO();
		TurmaDisciplinaDAO tdDao = new TurmaDisciplinaDAO();
		PaginaSlideDAO psDao = new PaginaSlideDAO();
		
		Map<String, Object> mapaDeAtributos = new HashMap<>();
		Map<String, Object> subMapa= new HashMap<>();
		subMapa.put("codTurma", id.getCodigoTurma());
		subMapa.put("anoTurma", id.getAnoTurma());
		mapaDeAtributos.put("id", subMapa);
		
		adDao.deletarPorId(adDao.buscarPorAtributos(mapaDeAtributos));
		atDao.deletarPorId(atDao.buscarPorAtributos(mapaDeAtributos));
		adtDao.deletarPorId(adtDao.buscarPorAtributos(mapaDeAtributos));
		ptDao.deletarPorId(ptDao.buscarPorAtributos(mapaDeAtributos));
		tdDao.deletarPorId(tdDao.buscarPorAtributos(mapaDeAtributos));
		
		mapaDeAtributos.clear();
		
		mapaDeAtributos.put("codTurma", id.getCodigoTurma());
		mapaDeAtributos.put("anoTurma", id.getAnoTurma());
		psDao.deletarPorId(psDao.buscarPorAtributos(mapaDeAtributos));
		mapaDeAtributos.clear();		
	}
	
	@Override
	public void deletarRelacionamentos(List<Turma> lista) throws Exception{
		for(Turma t :lista)
			deletarRelacionamentos(t.getId());
	}
	
	public List<Integer> anoDasTurmasDoCurso(Curso c) throws Exception{
		EntityManager em = factory.createEntityManager();
		String jpql = "SELECT distinct(t.id.anoTurma) from Turma t where t.codCurso = ?1";
		List<Integer> lista = em.createQuery(jpql, Integer.class).setParameter(1, c.getCodCurso()).getResultList();
		em.close();
		return lista;
	}
	
	public List<Integer> todosAnosExistentes() throws Exception{
		EntityManager em = factory.createEntityManager();
		String jpql = "SELECT distinct(t.id.anoTurma) from Turma t";
		List<Integer> lista = em.createQuery(jpql, Integer.class).getResultList();
		em.close();
		return lista;
	}
	
	public List<Turma> consultarTurmas(Turma t) throws Exception{
		EntityManager em = factory.createEntityManager();
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("t.nomeTurma", t.getNomeTurma(), CondicaoEnum.LIKE));
		condicoes.add(new Condicao("t.id.codigoTurma", t.getId().getCodigoTurma(), CondicaoEnum.EQUALS));
		condicoes.add(new Condicao("t.id.anoTurma", t.getId().getAnoTurma(), CondicaoEnum.EQUALS));
		condicoes.add(new Condicao("t.codCurso", t.getCodCurso(), CondicaoEnum.EQUALS));
		Where w = Where.criarWhere(condicoes);
		String jpql = "SELECT t FROM Turma t " + w.getStringWhere();
		TypedQuery<Turma> query = em.createQuery(jpql, Turma.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<Turma> turmas = query.getResultList();
		em.close();
		return turmas;
	}

}
