package jpa.pessoa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ALUNO", schema = "mobiclass")
public class Aluno extends Pessoa implements Serializable {
	private static final long serialVersionUID = 1L;

	public Aluno() {
	}

	public Aluno(long RA, String nome, String dataNasc, String email,
			String senha, String sexo) {
		setRA(RA);
		setNome(nome);
		setDataNasc(dataNasc);
		setEmail(email);
		setSenha(senha);
		setLogado(0);
		setSexo(sexo);
	}

	public Aluno(long RA, String nome, Date dataNasc, String email,
			String senha, String sexo) {
		setRA(RA);
		setNome(nome);
		setDataNasc(dataNasc);
		setEmail(email);
		setSenha(senha);
		setLogado(0);
		setSexo(sexo);
	}

	public String toString() {
		return "||" + getRA() + '|' + getNome() + '|' + getEmail() + '|'
				+ getLogado() + '|' + getDataNasc() + '|' + getSenha() + "||";
	}
}
