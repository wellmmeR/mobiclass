package jpa.academico;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="TURMA_DISCIPLINA", schema = "mobiclass")
public class TurmaDisciplina implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TurmaDisciplinaID id;

	public TurmaDisciplina() {
		setId(new TurmaDisciplinaID());
	}
	public TurmaDisciplina(String codDisciplina, String codTurma, int anoTurma) {
		setId(new TurmaDisciplinaID(codDisciplina, codTurma, anoTurma));
	}	
	
	public TurmaDisciplinaID getId() {
		return id;
	}
	public void setId(TurmaDisciplinaID id) {
		this.id = id;
	}
	public String toString(){
		return "||"+id.getCodDisciplina()+'|'+id.getCodTurma()+'|'+id.getAnoTurma()+"||";
	}
	
}
