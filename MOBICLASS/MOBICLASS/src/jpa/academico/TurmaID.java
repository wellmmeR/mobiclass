package jpa.academico;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TurmaID implements Serializable, Comparable<TurmaID>{

	private static final long serialVersionUID = -3591303718387106415L;
	
	public TurmaID(){
		
	}
	
	public TurmaID(String codigoTurma, int anoTurma){
		this.codigoTurma = codigoTurma;
		this.anoTurma = anoTurma;
	}
	
	@Column(name = "COD_TURMA", nullable = false)
	private String codigoTurma;
	@Column(name = "ANO_TURMA", nullable = false)
	private int anoTurma;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + anoTurma;
		result = prime * result
				+ ((codigoTurma == null) ? 0 : codigoTurma.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TurmaID other = (TurmaID) obj;
		if (anoTurma != other.anoTurma)
			return false;
		if (codigoTurma == null) {
			if (other.codigoTurma != null)
				return false;
		} else if (!codigoTurma.equals(other.codigoTurma))
			return false;
		return true;
	}
	
	public int compareTo(TurmaID o) {
		int stringComparator = this.getCodigoTurma().compareTo(o.getCodigoTurma());
		if(stringComparator == 0)
			return Integer.compare(this.getAnoTurma(), o.getAnoTurma());
		else
			return stringComparator;
			
	}

	public String getCodigoTurma() {
		return codigoTurma;
	}

	public void setCodigoTurma(String codigoTurma) {
		this.codigoTurma = codigoTurma;
	}

	public int getAnoTurma() {
		return anoTurma;
	}

	public void setAnoTurma(int anoTurma) {
		this.anoTurma = anoTurma;
	}

}
