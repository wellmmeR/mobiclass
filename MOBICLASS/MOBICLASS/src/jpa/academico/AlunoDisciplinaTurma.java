package jpa.academico;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "ALUNO_DISCIPLINA_TURMA", schema = "mobiclass")
public class AlunoDisciplinaTurma implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private AlunoDisciplinaTurmaID id;
	
	@Column(name = "STATUS", nullable = false)
	private int statusDis;

	public int getStatusDis() {
		return statusDis;
	}

	public void setStatusDis(int statusDis) {
		this.statusDis = statusDis;
	}

	public AlunoDisciplinaTurma() {
	}

	public AlunoDisciplinaTurma(long raAluno, String codDisciplina,
			String codTurma, int anoTurma, int Status) {
		AlunoDisciplinaTurmaID id = new AlunoDisciplinaTurmaID();
		id.setAnoTurma(anoTurma);
		id.setCodDisciplina(codDisciplina);
		id.setCodTurma(codTurma);
		id.setRA(raAluno);
		setId(id);
		setStatusDis(Status);
	}

	public AlunoDisciplinaTurmaID getId() {
		return id;
	}

	public void setId(AlunoDisciplinaTurmaID id) {
		this.id = id;
	}

	public String toString() {
		return "||" + id.getRA() + "|" + id.getCodDisciplina() + "|" + getStatusDis()
				+ "||";
	}
}