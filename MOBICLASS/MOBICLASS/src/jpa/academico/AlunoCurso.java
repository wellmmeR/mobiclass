package jpa.academico;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ALUNO_CURSO", schema = "mobiclass")
public class AlunoCurso implements Serializable {
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	private AlunoCursoID id;
	
	@Column(name = "ANO_ENTRADA", nullable = false)
	private int anoEntrada;
	

	public AlunoCurso() {
		id = new AlunoCursoID();
	}
	
	public AlunoCurso(String codCurso, int anoEntrada, long raAluno) {
		
		setId(new AlunoCursoID(codCurso, raAluno));
		setAnoEntrada(anoEntrada);
	}

	public int getAnoEntrada() {
		return anoEntrada;
	}

	public void setAnoEntrada(int anoEntrada) {
		this.anoEntrada = anoEntrada;
	}

	public AlunoCursoID getId() {
		return id;
	}

	public void setId(AlunoCursoID id) {
		this.id = id;
	}

	public String toString() {
		return "||" + id.getRaAluno() + '|' + id.getCodCurso() + '|' + getAnoEntrada()
				+ "||";
	}
}
