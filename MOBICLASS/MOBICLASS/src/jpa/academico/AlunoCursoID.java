package jpa.academico;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AlunoCursoID implements Serializable {
	
	private static final long serialVersionUID = 7307952270555141190L;
	@Column(name = "COD_CURSO", nullable = false)
	private String codCurso;
	@Column(name = "COD_ALUNO", nullable = false)
	private long raAluno;
	
	public AlunoCursoID() {
	
	}
	
	public AlunoCursoID(String codCurso, long raAluno) {
		this.codCurso = codCurso;
		this.raAluno = raAluno;
	}
	
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public long getRaAluno() {
		return raAluno;
	}
	public void setRaAluno(long raAluno) {
		this.raAluno = raAluno;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codCurso == null) ? 0 : codCurso.hashCode());
		result = prime * result + (int) (raAluno ^ (raAluno >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlunoCursoID other = (AlunoCursoID) obj;
		if (codCurso == null) {
			if (other.codCurso != null)
				return false;
		} else if (!codCurso.equals(other.codCurso))
			return false;
		if (raAluno != other.raAluno)
			return false;
		return true;
	}
	
	

}
