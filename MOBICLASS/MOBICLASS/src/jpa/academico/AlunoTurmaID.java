package jpa.academico;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AlunoTurmaID implements Serializable{

	private static final long serialVersionUID = 3998638206863018596L;
	@Column(name="COD_ALUNO", nullable = false)
	private long RA;	
	@Column(name="ANO_TURMA", nullable = false)
	private int anoTurma;
	@Column(name="COD_TURMA", nullable = false)
	private String codTurma;
	
	public AlunoTurmaID(long RA,int anoTurma, String codTurma) {
		this.RA = RA;
		this.anoTurma = anoTurma;
		this.codTurma = codTurma;
	}
	
	public AlunoTurmaID() {}
	
	public long getRA() {
		return RA;
	}
	public void setRA(long rA) {
		RA = rA;
	}
	public int getAnoTurma() {
		return anoTurma;
	}
	public void setAnoTurma(int anoTurma) {
		this.anoTurma = anoTurma;
	}
	public String getCodTurma() {
		return codTurma;
	}
	public void setCodTurma(String codTurma) {
		this.codTurma = codTurma;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (RA ^ (RA >>> 32));
		result = prime * result + anoTurma;
		result = prime * result
				+ ((codTurma == null) ? 0 : codTurma.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlunoTurmaID other = (AlunoTurmaID) obj;
		if (RA != other.RA)
			return false;
		if (anoTurma != other.anoTurma)
			return false;
		if (codTurma == null) {
			if (other.codTurma != null)
				return false;
		} else if (!codTurma.equals(other.codTurma))
			return false;
		return true;
	}
		
}
