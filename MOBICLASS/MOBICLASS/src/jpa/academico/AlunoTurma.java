package jpa.academico;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ALUNO_TURMA", schema = "mobiclass")
public class AlunoTurma implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private AlunoTurmaID id;
	
	public AlunoTurma() {
		setId(new AlunoTurmaID());
	}
	public AlunoTurma(long rA, String codTurma, int anoTurma) {
		setId(new AlunoTurmaID(rA,anoTurma, codTurma));
	}	

	public String toString(){
		return "||"+id.getRA()+"|"+id.getCodTurma()+"|"+id.getAnoTurma()+"||";
	}
	public AlunoTurmaID getId() {
		return id;
	}
	public void setId(AlunoTurmaID id) {
		this.id = id;
	}
}
