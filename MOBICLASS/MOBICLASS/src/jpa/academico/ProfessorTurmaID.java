package jpa.academico;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ProfessorTurmaID implements Serializable{

	private static final long serialVersionUID = 8354681396751050576L;
	@Column(name = "COD_PROFESSOR", nullable = false)
	private long codProfessor;
	@Column(name = "COD_TURMA", nullable = false)
	private String codTurma;
	@Column(name = "ANO_TURMA", nullable = false)
	private int anoTurma;
	
	public ProfessorTurmaID() {
	}

	
	public ProfessorTurmaID(long codProfessor, String codTurma, int anoTurma) {
		super();
		this.codProfessor = codProfessor;
		this.codTurma = codTurma;
		this.anoTurma = anoTurma;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + anoTurma;
		result = prime * result + (int) (codProfessor ^ (codProfessor >>> 32));
		result = prime * result
				+ ((codTurma == null) ? 0 : codTurma.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfessorTurmaID other = (ProfessorTurmaID) obj;
		if (anoTurma != other.anoTurma)
			return false;
		if (codProfessor != other.codProfessor)
			return false;
		if (codTurma == null) {
			if (other.codTurma != null)
				return false;
		} else if (!codTurma.equals(other.codTurma))
			return false;
		return true;
	}


	public long getCodProfessor() {
		return codProfessor;
	}
	public void setCodProfessor(long codProfessor) {
		this.codProfessor = codProfessor;
	}
	public String getCodTurma() {
		return codTurma;
	}
	public void setCodTurma(String codTurma) {
		this.codTurma = codTurma;
	}
	public int getAnoTurma() {
		return anoTurma;
	}
	public void setAnoTurma(int anoTurma) {
		this.anoTurma = anoTurma;
	}

}
