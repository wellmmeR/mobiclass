package jpa.academico;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "PROFESSOR_TURMA", schema = "mobiclass")
public class ProfessorTurma {
	
	@EmbeddedId
	private ProfessorTurmaID id;
	@Column(name = "COD_CURSO", nullable = false)
	private String codCurso;

	public ProfessorTurma() {
		setId(new ProfessorTurmaID());
	}

	public ProfessorTurma(long codProfessor, String codTurma, int anoTurma,
			String codigoCurso) {
		setId(new ProfessorTurmaID(codProfessor, codTurma, anoTurma));
		setCodCurso(codigoCurso);
	}

	public String getCodCurso() {
		return codCurso;
	}

	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}

	public ProfessorTurmaID getId() {
		return id;
	}

	public void setId(ProfessorTurmaID id) {
		this.id = id;
	}

	public String toString() {
		return "||" +id.getCodTurma() + "|" + id.getAnoTurma() + "|" + getCodCurso()
				+ "|" + id.getCodProfessor() + "||";
	}
}
