package jpa.academico;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "TURMA", schema = "mobiclass")
public class Turma implements Serializable, Comparable<Turma> {
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TurmaID id;
	
	@Column(name = "NOM_TURMA", nullable = false)
	private String nomeTurma;
	
	@Column(name = "COD_CURSO", nullable = false)
	private String codCurso;

	public Turma() {
		setId(new TurmaID());
	}

	public Turma(String codigoTurma, String nomeTurma, int anoTurma,
			String codigoCurso) {
		setId(new TurmaID(codigoTurma, anoTurma));
		setNomeTurma(nomeTurma);
		setCodCurso(codigoCurso);
	}

	public String getCodCurso() {
		return codCurso;
	}

	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}

	public String getNomeTurma() {
		return nomeTurma;
	}

	public void setNomeTurma(String nomeTurma) {
		this.nomeTurma = nomeTurma;
	}
	
	public TurmaID getId() {
		return id;
	}

	public void setId(TurmaID id) {
		this.id = id;
	}

	public String toString() {
		// Codigo + nome + ano;
		return "||" + id.getCodigoTurma() + '|' + getNomeTurma() + '|'
				+ id.getAnoTurma() + "||";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codCurso == null) ? 0 : codCurso.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((nomeTurma == null) ? 0 : nomeTurma.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Turma other = (Turma) obj;
		if (codCurso == null) {
			if (other.codCurso != null)
	
				return false;
		} else if (!codCurso.equals(other.codCurso))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nomeTurma == null) {
			if (other.nomeTurma != null)
				return false;
		} else if (!nomeTurma.equals(other.nomeTurma))
			return false;
		return true;
	}
	
	public int compareTo(Turma o) {
		return this.getId().compareTo(o.getId());
	}

	
}
