package jpa.testes;

import java.util.ArrayList;
import java.util.List;

import jpa.DAO.AlunoDAO;
import jpa.DAO.CursoDAO;
import jpa.DAO.TurmaDAO;
import jpa.DAO.relacionamento.AlunoTurmaDAO;
import jpa.academico.AlunoTurma;
import jpa.academico.Curso;
import jpa.academico.Turma;
import jpa.academico.TurmaID;
import jpa.pessoa.Aluno;

import org.junit.Test;

public class AlunoTurmaDAOTester {


	@Test
	public void testDeletarPorIdListOfT() throws Exception{
		AlunoDAO aDao = new AlunoDAO();
		TurmaDAO tDao = new TurmaDAO();
		CursoDAO cDao = new CursoDAO();
		AlunoTurmaDAO atDao = new AlunoTurmaDAO();
		
		
		List<Aluno> alunos = new ArrayList<Aluno>();
		List<Turma> turmas = new ArrayList<Turma>();
		List<AlunoTurma> alunosETurmas = new ArrayList<AlunoTurma>();
		
		for(int i=0; i<5;i++){
			long RA = aDao.primeiroRADisponivel();
			Aluno a = new Aluno(RA, "Teste"+i, "09/04/1992", "teste@hotmail.com", "123", "M");
			aDao.adicionar(a);
			alunos.add(a);
		}
		
		String codCurso = "cursoTeste";
		while(cDao.consultarPorId(codCurso) != null)
			codCurso = codCurso + "x";
		Curso c = new Curso(codCurso, "Curso Teste do Unasp", 0);
		cDao.adicionar(c);
		
		for(int i=0; i<5;i++){
			String codTurma = "turmaTeste";
			while(tDao.consultarPorId(new TurmaID(codTurma, 2015)) != null)
				codTurma = codTurma + "x";
			Turma turma = new Turma(codTurma, "Turma para Teste", 2015, c.getCodCurso());
			tDao.adicionar(turma);
			turmas.add(turma);
		}
		
		for(Aluno a: alunos){
			for(Turma t: turmas){
				AlunoTurma at = new AlunoTurma(a.getRA(), t.getId().getCodigoTurma(), t.getId().getAnoTurma());
				atDao.adicionar(at);
				alunosETurmas.add(at);
			}
		}
		
		atDao.deletarPorId(alunosETurmas);
		tDao.deletarPorId(turmas);
		cDao.deletarPorId(c.getCodCurso());
		aDao.deletarPorId(alunos);
	}

}
