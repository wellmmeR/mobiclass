package jpa.testes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jpa.DAO.AlunoDAO;
import jpa.DAO.CursoDAO;
import jpa.DAO.relacionamento.AlunoCursoDAO;
import jpa.academico.AlunoCurso;
import jpa.academico.Curso;
import jpa.pessoa.Aluno;
import static org.junit.Assert.fail;

import org.junit.Test;


public class AlunoCursoDAOTester {

	@Test
	public void testConsultarPorChaveDoAluno() throws Exception{
		AlunoDAO dao = new AlunoDAO();
		CursoDAO cDao = new CursoDAO();
		AlunoCursoDAO acDao = new AlunoCursoDAO();
		
		
		
		long RA = dao.primeiroRADisponivel();
		Aluno a = new Aluno(RA, "Teste", "09/04/1992", "teste@hotmail.com", "123", "M");
		dao.adicionar(a);
		
		String codigoCurso = "CursoTeste";
		while(cDao.consultarPorId(codigoCurso) != null)
			codigoCurso = codigoCurso + "x";
		Curso c = new Curso(codigoCurso, "Curso Teste do Unasp", 0);
		cDao.adicionar(c);
		
		AlunoCurso ac = new AlunoCurso(c.getCodCurso(), 2015, a.getRA());
		acDao.adicionar(ac);
		
		Map<String, Object> mapaDeAtributos = new HashMap<>();
		mapaDeAtributos.put("RA", a.getRA());
		
		if(acDao.buscarPorAtributos(mapaDeAtributos) == null)
			fail("Aluno n�o encontrado");
		
		acDao.deletarPorId(ac.getId());
		dao.deletarPorId(a.getRA());
		cDao.deletarPorId(c.getCodCurso());
	}

	@Test
	public void testDeletarPorIdListOfT() throws Exception{
		AlunoDAO dao = new AlunoDAO();
		CursoDAO cDao = new CursoDAO();
		AlunoCursoDAO acDao = new AlunoCursoDAO();
		List<Aluno> alunos = new ArrayList<Aluno>();
		List<Curso> cursos = new ArrayList<Curso>();
		List<AlunoCurso> alunosECursos= new ArrayList<AlunoCurso>();
		
		for(int i=0; i<5;i++){
			long RA = dao.primeiroRADisponivel();
			Aluno a = new Aluno(RA, "Teste"+i, "09/04/1992", "teste@hotmail.com", "123", "M");
			dao.adicionar(a);
			alunos.add(a);
		}
		
		for(int i=0; i<5;i++){
			String codigoCurso = "CursoTeste";
			while(cDao.consultarPorId(codigoCurso) != null)
				codigoCurso = codigoCurso + "x";
			Curso c = new Curso(codigoCurso, "Curso Teste do Unasp"+i, 0);
			cDao.adicionar(c);
			cursos.add(c);
		}
		
		for(Aluno aluno: alunos){
			for(Curso curso: cursos){
				AlunoCurso ac = new AlunoCurso(curso.getCodCurso(), 2015, aluno.getRA());
				acDao.adicionar(ac);
				alunosECursos.add(ac);
			}
		}
	
		acDao.deletarPorId(alunosECursos);
		cDao.deletarPorId(cursos);
		dao.deletarPorId(alunos);
		
		for(AlunoCurso ac : alunosECursos){
			if(acDao.consultarPorId(ac.getId()) != null)
				fail("AlunoCurso n�o foi deletado");
		}
	}

}
