package jpa.testes;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import jpa.DAO.CursoDAO;
import jpa.academico.Curso;

import org.junit.Test;

public class CursoDAOTester {

	@Test
	public void testGetType() {
		CursoDAO dao = new CursoDAO();
		if(dao.getType() != Curso.class)
			fail("Tipos diferentes");
	}

	@Test
	public void testAdicionarT() throws Exception{
		CursoDAO dao = new CursoDAO();
		String codigoCurso = "CursoTeste";
		while(dao.consultarPorId(codigoCurso) != null)
			codigoCurso = codigoCurso + "x";
		
		Curso c = new Curso(codigoCurso, "Curso Teste do Unasp", 0);
		dao.adicionar(c);
		if(dao.consultarPorId(c.getCodCurso())==null)
			fail("N�o adicionou o curso");
		
		dao.deletarPorId(c.getCodCurso());
	}

	@Test
	public void testAtualizarT() throws Exception{
		CursoDAO dao = new CursoDAO();
		String codigoCurso = "CursoTeste";
		while(dao.consultarPorId(codigoCurso) != null)
			codigoCurso = codigoCurso + "x";
		Curso c = new Curso(codigoCurso, "Curso Teste do Unasp", 0);
		dao.adicionar(c);
		
		Curso c2 = dao.consultarPorId(codigoCurso);
		c2.setIsDisponivel(1);
		c2.setNome("Curso Teste do Unasp1234");
		
		dao.atualizar(c2);
		c2 = dao.consultarPorId(codigoCurso);
		
		dao.deletarPorId(c.getCodCurso());
		if(c2.getIsDisponivel() == c.getIsDisponivel() || c2.getNome().equals(c.getNome()))
			fail("N�o Alterou o curso");
		
		
	}

	@Test
	public void testAtualizarListOfT() throws Exception{
		CursoDAO dao = new CursoDAO();
		List<Curso> cursos = new ArrayList<Curso>();
		cursos.add(new Curso("Curso1", "Curso para Teste", 0));
		cursos.add(new Curso("Curso2", "Curso para Teste2", 0));
		dao.adicionar(cursos);
		
		Curso c3 = dao.consultarPorId("Curso1");
		Curso c4 = dao.consultarPorId("Curso2");
		c3.setIsDisponivel(1);
		c4.setIsDisponivel(1);
		c3.setNome("Curso para Testeeeee");
		c4.setNome("Curso para Testeeeee");
		List<Curso> cursos2 = new ArrayList<Curso>();
		cursos2.add(c3);
		cursos2.add(c4);
		
		dao.atualizar(cursos2);
		
		for(Curso curso : cursos){
			Curso c = dao.consultarPorId(curso.getCodCurso());
			if(c.getNome().equals(curso.getNome())|| c.getIsDisponivel()==curso.getIsDisponivel())
				fail("N�o houve mudan�a em algum atributo");
		}
		
		dao.deletarPorId("Curso1");
		dao.deletarPorId("Curso2");
	}

	@Test
	public void testConsultarPorId() throws Exception{
		CursoDAO dao = new CursoDAO();
		String codigoCurso = "CursoTeste";
		while(dao.consultarPorId(codigoCurso) != null)
			codigoCurso = codigoCurso + "x";
		Curso c = new Curso(codigoCurso, "Curso Teste do Unasp", 0);
		dao.adicionar(c);
		
		if(dao.consultarPorId(c.getCodCurso()) == null)
			fail("N�o salvou no banco de dados");
		
		dao.deletarPorId(c.getCodCurso());
	}

	@Test
	public void testDeletarPorIdID() throws Exception{
		CursoDAO dao = new CursoDAO();
		String codigoCurso = "CursoTeste";
		while(dao.consultarPorId(codigoCurso) != null)
			codigoCurso = codigoCurso + "x";
		Curso c = new Curso(codigoCurso, "Curso Teste do Unasp", 0);
		dao.adicionar(c);
		dao.deletarPorId(c.getCodCurso());
		if(dao.consultarPorId(c.getCodCurso()) != null)
			fail("Curso n�o foi removido");
	}
	
	@Test
	public void testDeletarPorIdListOfID() throws Exception{
		CursoDAO dao = new CursoDAO();
		List<Curso> cursos = new ArrayList<Curso>();
		for(int i=0; i<10;i++){
			String codigoCurso = "CursoTeste";
			while(dao.consultarPorId(codigoCurso) != null)
				codigoCurso = codigoCurso + "x";
			Curso c = new Curso(codigoCurso, "Curso Teste do Unasp"+i, 0);
			dao.adicionar(c);
			cursos.add(c);
		}
		
		dao.deletarPorId(cursos);
		for(Curso curso : cursos){
			if(dao.consultarPorId(curso.getCodCurso()) != null)
				fail("Curso n�o foi removido");
		}
		
	}

}
