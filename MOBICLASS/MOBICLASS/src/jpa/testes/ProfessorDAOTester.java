package jpa.testes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import jpa.DAO.ProfessorDAO;
import jpa.pessoa.Professor;

import org.junit.Assert;
import org.junit.Test;

public class ProfessorDAOTester {

	@Test
	public void testGetType() {
		ProfessorDAO dao = new ProfessorDAO();
		assertEquals(dao.getType(), Professor.class);
		
	}

	@Test
	public void testPrimeiroRADisponivel() throws Exception{
		ProfessorDAO dao = new ProfessorDAO();
		Long primeiroRA = dao.primeiroRADisponivel();
		
		Assert.assertTrue(primeiroRA!=null);
	}

	@Test
	public void testAdicionar() throws Exception{
		ProfessorDAO dao = new ProfessorDAO();
		long RA = dao.primeiroRADisponivel();
		Professor a = new Professor(RA, "Teste", "09/04/1992", "teste@hotmail.com", "123", "M");
		dao.adicionar(a);
		if(dao.consultarPorId(RA) == null)
			fail("Professor criado n�o foi encontrado no banco");
		
		dao.deletarPorId(RA);
	}

	@Test
	public void testAdicionarListOfProfessor() throws Exception{
		ProfessorDAO dao = new ProfessorDAO();
		
		List<Professor> Professors = new ArrayList<Professor>();
		List<Long> codigos = new ArrayList<Long>();
		for(int i=0;i<4;i++){
			long RA = dao.primeiroRADisponivel()+i;
			Professor a = new Professor(RA, "Teste", "09/04/1992", "teste@hotmail.com", "123", "M");
			codigos.add(RA);
			Professors.add(a);
			
		}
		dao.adicionar(Professors);
		
		for(Long codigo : codigos){
			if(dao.consultarPorId(codigo)==null)
				fail("Professor n�o foi inserido");
		}	
		
		for(Long codigo:codigos){
			dao.deletarPorId(codigo);	
		}
		
	}

	@Test
	public void testAtualizar() throws Exception{
		ProfessorDAO dao = new ProfessorDAO();
		long RA = dao.primeiroRADisponivel();
		String nome = "Teste"; 
		String dataNascimento = "09/04/1992";
		String email = "teste@hotmail.com";
		String senha = "123";
		String sexo = "M";
		Professor a = new Professor(RA, nome, dataNascimento, email, senha, sexo);
		dao.adicionar(a);
		
		a = new Professor(RA, "Testee", "10/04/1992","testee@hotmail.com", "1234", "F"); 
		dao.atualizar(a);
		a = dao.consultarPorId(RA);
		if(a.getNome().equals(nome) || a.getDataNasc().equals(dataNascimento) || a.getEmail().equals(email) || a.getSenha().equals(senha) || a.getSexo().equals(sexo))
			fail("Alguma coisa n�o mudou");
		dao.deletarPorId(RA);
	}

	@Test
	public void testAtualizarListOfProfessors() throws Exception{
		ProfessorDAO dao = new ProfessorDAO();
		
		List<Professor> Professors = new ArrayList<Professor>();
		List<Long> codigos = new ArrayList<Long>();
		for(int i=0;i<4;i++){
			long RA = dao.primeiroRADisponivel() + i;
			String nome = "Teste"; 
			String dataNascimento = "09/04/1992";
			String email = "teste@hotmail.com";
			String senha = "123";
			String sexo = "M";
			
			Professors.add(new Professor(RA, nome, dataNascimento, email, senha, sexo));
			codigos.add(RA);
		}
		dao.adicionar(Professors);
		List<Professor> Professors2 = new ArrayList<Professor>();
		
		for(Long codigo: codigos){
			Professor a = dao.consultarPorId(codigo);
			a.setNome("Testee");
			a.setDataNasc("10/04/1992");
			a.setEmail("testee@hotmail.com");
			a.setSenha("1234");
			a.setSexo("F");
			Professors2.add(a);
		}
		dao.atualizar(Professors2);
		
		Professors2 = new ArrayList<Professor>();
		
		for(Long codigo : codigos){
			Professors2.add(dao.consultarPorId(codigo));
		}
		
		for(int i=0;i<4;i++){
			Professor a1 = Professors.get(i);
			Professor a2 = Professors2.get(i);
			
			if(a1.getNome().equals(a2.getNome()) || a1.getDataNasc().equals(a2.getDataNasc()) || a1.getEmail().equals(a2.getEmail()) || a1.getSenha().equals(a2.getSenha()) || a1.getSexo().equals(a2.getSexo()))
				fail("Alguma coisa n�o mudou");
		}
		
		for(Long codigo:codigos){
			dao.deletarPorId(codigo);
		}
		
	}

	@Test
	public void testConsultarPorId() throws Exception{
		ProfessorDAO dao = new ProfessorDAO();
		
		long RA = dao.primeiroRADisponivel();
		Professor a = new Professor(RA, "Teste", "09/04/1992", "teste@hotmail.com", "123", "M");
		dao.adicionar(a);
		a = dao.consultarPorId(RA);
		if(a == null)
			fail("Consulta n�o trouxe o Professor esperado");
		
		dao.deletarPorId(RA);
		
		
		
	}

	@Test
	public void testDeletarPorIdID() throws Exception{
		ProfessorDAO dao = new ProfessorDAO();
		long RA = dao.primeiroRADisponivel();
		Professor a = new Professor(RA, "Teste", "09/04/1992", "teste@hotmail.com", "123", "M");
		dao.adicionar(a);
		dao.deletarPorId(RA);
		if(dao.consultarPorId(RA) != null)
			fail("Professor n�o foi deletado");
		
		
		
	}

}
