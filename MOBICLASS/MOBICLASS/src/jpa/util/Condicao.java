package jpa.util;

public class Condicao{
	private String atributo;
	private Object value;
	private CondicaoEnum operador;
	private boolean literal;
	
	public String getAtributo() {
		return atributo;
	}

	public void setAtributo(String atributo) {
		this.atributo = atributo;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public CondicaoEnum getOperador() {
		return operador;
	}

	public void setOperador(CondicaoEnum operador) {
		this.operador = operador;
	}
	
	public Condicao(String atributo, Object value){
		this.atributo = atributo;
		this.value = value;
		this.operador = CondicaoEnum.EQUALS;
		literal = false;
	}
	
	public Condicao(String atributo, Object value, boolean literal){
		this.atributo = atributo;
		this.value = value;
		this.operador = CondicaoEnum.EQUALS;
		this.literal = literal;
	}
	
	public Condicao(String atributo, Object value, CondicaoEnum operador){
		this.atributo = atributo;
		this.value = value;
		literal = false;
		if(operador != null)
			this.operador = operador;
		else
			this.operador = CondicaoEnum.EQUALS;
	}
	
	public Condicao(String atributo, Object value, boolean literal, CondicaoEnum operador){
		this.atributo = atributo;
		this.value = value;
		this.literal = literal;
		if(operador != null)
			this.operador = operador;
		else
			this.operador = CondicaoEnum.EQUALS;
	}

	public boolean isLiteral() {
		return literal;
	}

	public void setLiteral(boolean literal) {
		this.literal = literal;
	}
	
}