package jpa.util;

public enum CondicaoEnum {

	EQUALS("="),
	LIKE(" like "),
	LESS_THEN("<"),
	GREATER_THEN(">"),
	LESS_EQUALS_THEN("<="),
	GREATER_EQUALS_THEN(">=");
	
	private String valor;
	private CondicaoEnum(String valor) {
		this.valor = valor;
	}
	
	public String getValor(){
		return valor;
	}
}
