package jpa.atividade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="ATIVIDADE", schema = "mobiclass")
public class Atividade implements Serializable{
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	private AtividadeID idAtividade;
	@Column(name="NOM_ATIVIDADE", nullable = false)
	private String nome;
	@Column(name="FLG_DISPONIVEL", nullable = false)
	private int disponivel;
	
	/*
	 * ATRIBUTOS ADICIONAIS QUE N�O EST�O NO BANCO DE DADOS
	 */
	@Transient
	private int quantidadeAtividadesDisponiveis;
	
	//Construtores;
	public Atividade(){
		setIdAtividade(new AtividadeID());
	}
	public Atividade(String codigoAtividade, String nome, String codigoDisciplina, int isDisponivel, long areaAtividade){
		setIdAtividade(new AtividadeID(codigoAtividade, codigoDisciplina, areaAtividade));
		setNome(nome);
		setDisponivel(isDisponivel);
		
	}
	
	public int isDisponivel() {
		return disponivel;
	}
	public void setDisponivel(int disponivel) {
		this.disponivel = disponivel;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return "||"+idAtividade.getCodigoAtividade()+'|'+idAtividade.getAreaAtividade()+'|'+getNome()+'|'+idAtividade.getCodigoDisciplina()+"||";
	}
	public AtividadeID getIdAtividade() {
		return idAtividade;
	}
	public void setIdAtividade(AtividadeID idAtividade) {
		this.idAtividade = idAtividade;
	}
	public int getQuantidadeAtividadesDisponiveis() {
		return quantidadeAtividadesDisponiveis;
	}
	public void setQuantidadeAtividadesDisponiveis(
			int quantidadeAtividadesDisponiveis) {
		this.quantidadeAtividadesDisponiveis = quantidadeAtividadesDisponiveis;
	}	
}
