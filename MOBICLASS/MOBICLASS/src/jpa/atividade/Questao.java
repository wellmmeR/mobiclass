package jpa.atividade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PERGUNTA", schema = "mobiclass")
public class Questao implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "COD_PERGUNTA", nullable = false)
	private long codPergunta;
	@Column(name = "NOM_PERGUNTA", nullable = false)
	private String nomeQuestao;
	@Column(name = "COD_DISCIPLINA", nullable = false)
	private String codDisciplina;
	@Column(name = "FLG_DISPONIVEL", nullable = false)
	private int isDisponivel;
	@Column(name = "VAL_PERGUNTA", nullable = false)
	private double valorPergunta;
	@Column(name = "CATEGORIA", nullable = false)
	private int categoria;
	@Column(name = "COD_ATIVIDADE", nullable = false)
	private String codAtividade;
	@Column(name = "AREA_ATIVIDADE", nullable = false)
	private long areaAtividade;

	public Questao() {

	}

	public Questao(String questao, String codDisciplina, int isDisponivel,
			String codAtividade, int categoria, double valorPergunta,
			long areaAtividade) {
		setCodDisciplina(codDisciplina);
		setIsDisponivel(isDisponivel);
		setNomeQuestao(questao);
		setValorPergunta(valorPergunta);
		setCodAtividade(codAtividade);
		setCategoria(categoria);
		setAreaAtividade(areaAtividade);
	}

	public int getCategoria() {
		return categoria;
	}

	public void setCategoria(int categoria) {
		this.categoria = categoria;
	}

	public String getCodAtividade() {
		return codAtividade;
	}

	public void setCodAtividade(String codAtividade) {
		this.codAtividade = codAtividade;
	}

	public long getCodPergunta() {
		return codPergunta;
	}

	public void setCodPergunta(long codPergunta) {
		this.codPergunta = codPergunta;
	}

	public String getNomeQuestao() {
		return nomeQuestao;
	}

	public void setNomeQuestao(String nomeQuestao) {
		this.nomeQuestao = nomeQuestao;
	}

	public String getCodDisciplina() {
		return codDisciplina;
	}

	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}

	public int getIsDisponivel() {
		return isDisponivel;
	}

	public void setIsDisponivel(int isDisponivel) {
		this.isDisponivel = isDisponivel;
	}

	public double getValorPergunta() {
		return valorPergunta;
	}

	public void setValorPergunta(double valorPergunta) {
		this.valorPergunta = valorPergunta;
	}

	public long getAreaAtividade() {
		return areaAtividade;
	}

	public void setAreaAtividade(long areaAtividade) {
		this.areaAtividade = areaAtividade;
	}

	public String toString() {
		return "||" + getCodPergunta() + '|' + getAreaAtividade() + '|'
				+ getNomeQuestao() + '|' + getValorPergunta() + '|'
				+ getCodDisciplina() + '|' + getIsDisponivel() + "||";
	}
}
