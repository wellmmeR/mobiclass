package jpa.atividade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PontuacaoID implements Serializable{

	private static final long serialVersionUID = -5886656562289910585L;
	
	
	@Column(name="COD_ALUNO", nullable = false)
	private long raAluno;
	@Column(name="COD_ATIVIDADE", nullable = false)
	private String codAtividade;
	@Column(name="COD_DISCIPLINA", nullable = false)
	private String codDisciplina;
	@Column(name="ANO_TURMA", nullable = false)
	private int anoTurma;
	@Column(name="COD_TURMA", nullable = false)
	private String codTurma;
	@Column(name="AREA_ATIVIDADE", nullable = false)
	private long areaAtividade;
	
	public PontuacaoID() {
	}

	public PontuacaoID(long raAluno, String codAtividade,
			String codDisciplina, int anoTurma, String codTurma,
			long areaAtividade) {
		super();
		this.raAluno = raAluno;
		this.codAtividade = codAtividade;
		this.codDisciplina = codDisciplina;
		this.anoTurma = anoTurma;
		this.codTurma = codTurma;
		this.areaAtividade = areaAtividade;
	}



	

	

	public long getRaAluno() {
		return raAluno;
	}

	public void setRaAluno(long raAluno) {
		this.raAluno = raAluno;
	}

	public String getCodAtividade() {
		return codAtividade;
	}

	public void setCodAtividade(String codAtividade) {
		this.codAtividade = codAtividade;
	}

	public String getCodDisciplina() {
		return codDisciplina;
	}

	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}

	public int getAnoTurma() {
		return anoTurma;
	}

	public void setAnoTurma(int anoTurma) {
		this.anoTurma = anoTurma;
	}

	public String getCodTurma() {
		return codTurma;
	}

	public void setCodTurma(String codTurma) {
		this.codTurma = codTurma;
	}

	public long getAreaAtividade() {
		return areaAtividade;
	}

	public void setAreaAtividade(long areaAtividade) {
		this.areaAtividade = areaAtividade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + anoTurma;
		result = prime * result
				+ (int) (areaAtividade ^ (areaAtividade >>> 32));
		result = prime * result
				+ ((codAtividade == null) ? 0 : codAtividade.hashCode());
		result = prime * result
				+ ((codDisciplina == null) ? 0 : codDisciplina.hashCode());
		result = prime * result
				+ ((codTurma == null) ? 0 : codTurma.hashCode());
		result = prime * result + (int) (raAluno ^ (raAluno >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PontuacaoID other = (PontuacaoID) obj;
		if (anoTurma != other.anoTurma)
			return false;
		if (areaAtividade != other.areaAtividade)
			return false;
		if (codAtividade == null) {
			if (other.codAtividade != null)
				return false;
		} else if (!codAtividade.equals(other.codAtividade))
			return false;
		if (codDisciplina == null) {
			if (other.codDisciplina != null)
				return false;
		} else if (!codDisciplina.equals(other.codDisciplina))
			return false;
		if (codTurma == null) {
			if (other.codTurma != null)
				return false;
		} else if (!codTurma.equals(other.codTurma))
			return false;
		if (raAluno != other.raAluno)
			return false;
		return true;
	}
	
	
	
}
