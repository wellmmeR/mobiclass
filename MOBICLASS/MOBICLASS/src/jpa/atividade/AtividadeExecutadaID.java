package jpa.atividade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AtividadeExecutadaID implements Serializable{

	private static final long serialVersionUID = -5643687196831461890L;
	
	@Column(name="COD_ALUNO", nullable = false)
	private Long codAluno;
	@Column(name="COD_ATIVIDADE", nullable = false)
	private String codAtividade;
	@Column(name="COD_DISCIPLINA", nullable = false)
	private String codDisciplina;
	@Column(name="COD_TURMA", nullable = false)
	private String codTurma;
	@Column(name="ANO_TURMA", nullable = false)
	private int anoTurma;
	@Column(name="AREA_ATIVIDADE", nullable = false)
	private long areaAtividade;
	
	public AtividadeExecutadaID() {
	
	}
	public AtividadeExecutadaID(long codAluno, String codAtividade, String codDisciplina, String codTurma, int anoTurma, long areaAtividade) {
		this.codAluno = codAluno;
		this.codAtividade = codAtividade;
		this.codDisciplina = codDisciplina;
		this.codTurma = codTurma;
		this.anoTurma = anoTurma;
		this.areaAtividade = areaAtividade;
	}
	
	public Long getCodAluno() {
		return codAluno;
	}
	public void setCodAluno(Long codAluno) {
		this.codAluno = codAluno;
	}
	public String getCodAtividade() {
		return codAtividade;
	}
	public void setCodAtividade(String codAtividade) {
		this.codAtividade = codAtividade;
	}
	public String getCodDisciplina() {
		return codDisciplina;
	}
	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}
	public String getCodTurma() {
		return codTurma;
	}
	public void setCodTurma(String codTurma) {
		this.codTurma = codTurma;
	}
	public int getAnoTurma() {
		return anoTurma;
	}
	public void setAnoTurma(int anoTurma) {
		this.anoTurma = anoTurma;
	}
	public long getAreaAtividade() {
		return areaAtividade;
	}
	public void setAreaAtividade(long areaAtividade) {
		this.areaAtividade = areaAtividade;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + anoTurma;
		result = prime * result + (int) (areaAtividade ^ (areaAtividade >>> 32));
		result = prime * result + ((codAluno == null) ? 0 : codAluno.hashCode());
		result = prime * result + ((codAtividade == null) ? 0 : codAtividade.hashCode());
		result = prime * result + ((codDisciplina == null) ? 0 : codDisciplina.hashCode());
		result = prime * result + ((codTurma == null) ? 0 : codTurma.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtividadeExecutadaID other = (AtividadeExecutadaID) obj;
		if (anoTurma != other.anoTurma)
			return false;
		if (areaAtividade != other.areaAtividade)
			return false;
		if (codAluno == null) {
			if (other.codAluno != null)
				return false;
		} else if (!codAluno.equals(other.codAluno))
			return false;
		if (codAtividade == null) {
			if (other.codAtividade != null)
				return false;
		} else if (!codAtividade.equals(other.codAtividade))
			return false;
		if (codDisciplina == null) {
			if (other.codDisciplina != null)
				return false;
		} else if (!codDisciplina.equals(other.codDisciplina))
			return false;
		if (codTurma == null) {
			if (other.codTurma != null)
				return false;
		} else if (!codTurma.equals(other.codTurma))
			return false;
		return true;
	}
	
	

}
