package jpa.atividade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ALTERNATIVA_PERGUNTA", schema = "mobiclass")
public class AlternativaQuestao implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long ID;
	@Column(name="COD_PERGUNTA", nullable = false)
	private long codPergunta;
	@Column(name="ALT_PERGUNTA", nullable = false)
	private String nomeAlternativa;
	@Column(name="FLG_CORRETA", nullable = false)
	private int isCorreta;
	
	public AlternativaQuestao() {
	}
	public AlternativaQuestao(String alternativa, int isCorreta, long codQuestao) {
		setNomeAlternativa(alternativa);
		setCodPergunta(codQuestao);
		setIsCorreta(isCorreta);
	}
	public long getCodPergunta() {
		return codPergunta;
	}
	public void setCodPergunta(long codPergunta) {
		this.codPergunta = codPergunta;
	}
	public String getNomeAlternativa() {
		return nomeAlternativa;
	}
	public void setNomeAlternativa(String nomeAlternativa) {
		this.nomeAlternativa = nomeAlternativa;
	}
	public int getIsCorreta() {
		return isCorreta;
	}
	public void setIsCorreta(int isCorreta) {
		this.isCorreta = isCorreta;
	}
	public long getID() {
		return ID;
	}
	public String toString(){
		return "||"+getCodPergunta()+'|'+getNomeAlternativa()+'|'+getIsCorreta()+"||";
	}
}
