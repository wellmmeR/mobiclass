package jpa.atividade;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "PAGINA_ATIVIDADE", schema = "mobiclass")
public class PaginaSlide implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private long id;
	@Column(name = "COD_ATIVIDADE", nullable=false)
	private String codAtividade;
	@Column(name = "COD_TURMA", nullable=false)
	private String codTurma;
	@Column(name = "COD_DISCIPLINA", nullable=false)
	private String codDisciplina;
	@Column(name = "ANO_TURMA", nullable=false)
	private int anoTurma;
	@Column(name = "DATA", nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date data;
	@Column(name = "TOT_PAGINA", nullable=false)
	private int totalPagina;
	@Column(name = "PAG_ATUAL", nullable=false)
	private int paginaAtual;
	@Column(name = "AREA_ATIVIDADE", nullable=false)
	private long areaAtividade;
	
	public PaginaSlide(String codAtividade, String codDisciplina,
			String codTurma, int anoTurma, Date data, int totalPagina,
			int paginaAtual, long areaAtividade) {
		setAnoTurma(anoTurma);
		setCodAtividade(codAtividade);
		setCodDisciplina(codDisciplina);
		setCodTurma(codTurma);
		setData(data);
		setPaginaAtual(paginaAtual);
		setTotalPagina(totalPagina);
		setAreaAtividade(areaAtividade);
	}

	public String getCodAtividade() {
		return codAtividade;
	}

	public void setCodAtividade(String codAtividade) {
		this.codAtividade = codAtividade;
	}

	public String getCodTurma() {
		return codTurma;
	}

	public void setCodTurma(String codTurma) {
		this.codTurma = codTurma;
	}

	public String getCodDisciplina() {
		return codDisciplina;
	}

	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}

	public int getAnoTurma() {
		return anoTurma;
	}

	public void setAnoTurma(int anoTurma) {
		this.anoTurma = anoTurma;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public int getTotalPagina() {
		return totalPagina;
	}

	public void setTotalPagina(int totalPagina) {
		this.totalPagina = totalPagina;
	}

	public int getPaginaAtual() {
		return paginaAtual;
	}

	public void setPaginaAtual(int paginaAtual) {
		this.paginaAtual = paginaAtual;
	}

	public long getId() {
		return id;
	}
	public long getAreaAtividade() {
		return areaAtividade;
	}
	public void setAreaAtividade(long areaAtividade) {
		this.areaAtividade = areaAtividade;
	}
	public PaginaSlide() {
	}
}
