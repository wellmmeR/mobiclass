
package jpa.atividade;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ALUNO_ATIVIDADE", schema = "mobiclass")
public class AtividadeExecutada implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AtividadeExecutadaID id;

	@Column(name = "DAT_INICIO", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataInicio;

	@Column(name = "DAT_FIM", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFim;

	@Column(name = "FLG_FINALIZADO", nullable = false)
	private int isFinalizado;

	public AtividadeExecutada(String codTurma, int anoTurma, long codAluno, String codAtividade, String codDisciplina,
			long areaAtividade, int isFinalizado) {
		setId(new AtividadeExecutadaID(codAluno, codAtividade, codDisciplina, codTurma, anoTurma, areaAtividade));
		setIsFinalizado(isFinalizado);
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public int getIsFinalizado() {
		return isFinalizado;
	}

	public void setIsFinalizado(int isFinalizado) {
		this.isFinalizado = isFinalizado;
	}

	public AtividadeExecutada() {
		setId(new AtividadeExecutadaID());
	}

	public AtividadeExecutadaID getId() {
		return id;
	}

	public void setId(AtividadeExecutadaID id) {
		this.id = id;
	}
	
}
