package jpa.principal;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import jpa.DAO.CursoDAO;
import jpa.DAO.TurmaDAO;
import jpa.academico.AlunoCurso;
import jpa.academico.AlunoCursoID;
import jpa.academico.AlunoTurmaID;
import jpa.academico.Curso;
import jpa.academico.TurmaID;
import jpa.atividade.AlternativaQuestao;
import jpa.atividade.Questao;
import jpa.pessoa.Pessoa;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class Tester{

	public static final String path1 = "c:\\testePOI\\teste1.xlsx";
	public static final String path2 = "c:\\testePOI\\teste2.xls";
	public static final String path3 = "c:\\testePOI\\teste3.xls";
	
	public static void main(String[] args) throws Exception{
		String regex = "[12][0-9]{3}";
		String ano = "1001";
		if(ano.matches(regex))
			System.out.println("sim");
		else
			System.out.println("não");
	}
	
	public static void importar() throws Exception{
		List<Pessoa> alunos = new ArrayList<Pessoa>();
		List<Pessoa> professores = new ArrayList<Pessoa>();
		
		FileInputStream fis3 = new FileInputStream(new File(path3));
		
		Workbook workbook = WorkbookFactory.create(fis3);
		
		Sheet sheet = workbook.getSheetAt(0);
		
		Iterator<Row> rowInterator = sheet.rowIterator();
		int colunaAtual= 0;
		int linhaAtual = 0;
		try{
			
			if(rowInterator.hasNext())
				rowInterator.next();
			while(rowInterator.hasNext()){
				Row row = rowInterator.next();
				Iterator<Cell> cellIterator = row.iterator();
				Pessoa p= new Pessoa();
				while(cellIterator.hasNext()){
					Cell celula = cellIterator.next();
					linhaAtual = celula.getRowIndex();
					colunaAtual = celula.getColumnIndex();
					switch(celula.getColumnIndex()){
						case 0:
							p.setNome(extrairValorCelula(celula));
							break;
						case 1:
							p.setEmail(extrairValorCelula(celula));
							break;
						case 2:
							p.setDataNasc(celula.getDateCellValue());
							break;
						case 3:
							String sexo = extrairValorCelula(celula);
							if(!"M".equals(sexo)  && !"F".equals(sexo))
								throw new Exception("Sexo diferente de 'F' ou 'M'");
							p.setSexo(sexo);
							break;
						case 4:
							int tipo = Integer.parseInt(extrairValorCelula(celula));
							if(tipo == 1)
								alunos.add(p);
							else if(tipo == 2)
								professores.add(p);
							else throw new Exception(tipo +" não é um tipo valido");
							break;
						default:
							throw new Exception("Quantidade de colunas Invalidas");
					}
				}
			}
			workbook.close();
		}
		catch(IllegalStateException iSE){
			throw new Exception("Formatação incorreta na linha " + ++linhaAtual + " coluna " + ++colunaAtual);
		}
		catch(NumberFormatException nFE){
			throw new Exception("Erro ao tentar converter numero na linha " + ++linhaAtual + " coluna " + ++colunaAtual);
		}
		catch(Exception ex){
			throw new Exception("Erro na linha " + ++linhaAtual + " coluna " + ++colunaAtual + ": " + ex.getMessage());
		}
	}
	
	private static String extrairValorCelula(Cell celula){
		if(celula==null)
			return null;
		
		switch (celula.getCellType()) {
		case Cell.CELL_TYPE_BLANK:
			return "";
		case Cell.CELL_TYPE_BOOLEAN:
			return celula.getBooleanCellValue()+"";
		case Cell.CELL_TYPE_ERROR:
			return celula.getErrorCellValue()+"";
		case Cell.CELL_TYPE_FORMULA:
			return celula.getCellFormula();
		case Cell.CELL_TYPE_NUMERIC:
			return  ((int)celula.getNumericCellValue()) + "";
		case Cell.CELL_TYPE_STRING:
			return celula.getStringCellValue();
		default:
			return null;
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<AlunoCurso> findByAttributes(Map<String, Object> attributes) {
		EntityManager em = Persistence.createEntityManagerFactory("ServerAndroid").createEntityManager();
		List<AlunoCurso> results;
        //set up the Criteria query
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AlunoCurso> cq = cb.createQuery(AlunoCurso.class);
        Root<AlunoCurso> foo = cq.from(AlunoCurso.class);
 
        List<Predicate> predicates = new ArrayList<Predicate>();
        for(String s : attributes.keySet())
        {
            if(foo.get(s) != null){
                if(attributes.get(s) instanceof Map){
                	
					Map<String, Object> subMapa =(Map) attributes.get(s);
                	for(String subString: subMapa.keySet()){
                		predicates.add(cb.equal(foo.get(s).get(subString), subMapa.get(subString)));
                	}
                }
                else{
                	predicates.add(cb.equal(foo.get(s), attributes.get(s)));
                }
            	
            }
        }
        cq.where(predicates.toArray(new Predicate[]{}));
        TypedQuery<AlunoCurso> q = em.createQuery(cq);
 
        results = q.getResultList();
        return results;
    }
	
	public static void testarJPA2() throws Exception{
		AlunoTurmaID id = new AlunoTurmaID(46341L, 2013, "CI13A");
		//AlunoTurma alunoTurma = em.find(AlunoTurma.class, id);
		
		TurmaDAO turmaDao = new TurmaDAO();
		turmaDao.deletarPorId(new TurmaID(id.getCodTurma(), id.getAnoTurma()));
		 
	}
	
	public static void testarJPA() throws Exception{
		EntityManager em = Persistence.createEntityManagerFactory("ServerAndroid").createEntityManager();
		AlunoCursoID id = new AlunoCursoID("CComp", 66158L);
		AlunoCurso a = em.find(AlunoCurso.class, id);
		CursoDAO dao = new CursoDAO();
		dao.deletarPorId(a.getId().getCodCurso());
	}
	
	public static void vetor(int ...a ){
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<Curso> buscarCursos(String codigo, String nome, Integer disponivel){
		EntityManager em = Persistence.createEntityManagerFactory("ServerAndroid").createEntityManager();
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Curso> cq = builder.createQuery(Curso.class);
		Root<Curso> root = cq.from(Curso.class);
		List<Predicate> where = new ArrayList<Predicate>();
		if(codigo!=null)
			where.add(builder.equal(root.get("codCurso"), codigo));
		if(nome!=null)
			where.add(builder.like((Expression)root.get("nome"), '%'+nome+'%'));
		if(disponivel!=null)
			where.add(builder.equal(root.get("isDisponivel"),disponivel));
		
		cq.where(where.toArray(new Predicate[]{}));
		
		List<Curso> cursos = em.createQuery(cq).getResultList();
		em.close();
		return cursos;	
	}
	
	public static List<Curso> buscarTodosCursosDisponiveis(){
		EntityManager em = Persistence.createEntityManagerFactory("ServerAndroid").createEntityManager();
		//em.getTransaction().begin();
		String jpql = "SELECT c from Curso c WHERE c.isDisponivel=:disp";
		TypedQuery<Curso> query = em.createQuery(jpql, Curso.class);
		query.setParameter("disp", 1);
		return query.getResultList();
		//em.getTransaction().commit();
		
	}
	
	public static boolean inteiroParaBoolean(int i)
	{
		if(i == 1)
			return true;
		else
			return false;
	}
	
	public static void consultarAtividade(){
		String jpql = "SELECT at from Atividade at";// where a.codigoAtividade = :codAti and a.codigoDisciplina = :codDis and a.areaAtividade = :area";
		
		EntityManager em = Persistence.createEntityManagerFactory("ServerAndroid").createEntityManager();
		Query q = em.createQuery(jpql);
//		q.setParameter("codAti", "at01");
//		q.setParameter("codDis", "CAL1");
//		q.setParameter("area", 1);
		q.getResultList();
	}
	
	public static void testarInsercaoDaTabelaPergunta(){
		EntityManager em = Persistence.createEntityManagerFactory("ServerAndroid").createEntityManager();
		
		Questao q = new Questao();
		
		q.setCategoria(1);
		q.setCodAtividade("AtTeste3");
		q.setCodDisciplina("CAL1");
		q.setIsDisponivel(1);
		q.setNomeQuestao("Atividade para teste");
		q.setValorPergunta(0.1);
		q.setAreaAtividade(1);
		
		em.getTransaction().begin();
		em.persist(q);
		em.getTransaction().commit();
	}
	
	public static void deletarQuestao(Questao q){
		EntityManager em = Persistence.createEntityManagerFactory("ServerAndroid").createEntityManager();
		em.getTransaction().begin();
		
		Query query = em.createQuery("delete from AlternativaQuestao a where a.codPergunta = :codPergunta");
		query.setParameter("codPergunta", q.getCodPergunta());
		query.executeUpdate();
		q = em.find(Questao.class, 73L);
		em.remove(q);
		em.getTransaction().commit();
		
	}
	
	public static void deletarVariasAlternativas(List<AlternativaQuestao> alternativas, EntityManager em){

		
		em.getTransaction().begin();
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaDelete<AlternativaQuestao> criteria = cb.createCriteriaDelete(AlternativaQuestao.class);
		Root<AlternativaQuestao> e =  criteria.from(AlternativaQuestao.class);
		
		
		criteria.where(e.in(alternativas));
		
		Query q = em.createQuery(criteria);
		q.executeUpdate();
		
		em.getTransaction().commit();
	}

}
