package jpa.constante.log;

public class Funcao {
	public static String removeAnd(String string){
		int tamanho = string.length(), i = 0;
		String s = "";
		while(i < tamanho && !s.equals("WHERE") && i+5 <= tamanho)
		{
			s = string.substring(i, i+5);
			if (s.equals("WHERE")){
				s = string.substring(i+6, i+9);
				if (s.equals("AND")){
				    string = string.substring(0, i+5)+string.substring(i+9, tamanho);
					return string;
				}
			}
			i++;
		}
		return string;
	}
}
