package jpa.constante.log;

public class DadosLog {
	//Mensagens geral;
	public final String _erroCadastro = "Erro ao cadastrar dados no Banco de Dados: ";
	public final String _erroLogin = "Erro ao logar: ";
	public final String _erroQuery = "Erro ao buscar dados (Query)";
	public final String _erroQueryVazia = "A busca retornou vazio";
	public final String _erroRemocao = "Erro ao tentar remover dados";
	public final String _atribuicaoAdd = "Atribuição realizada por: ";
	public final String _masterLogin = "Usuário Master efetuou loggin";
	public final String _masterLogout = "Usuário Master efetuou logoff";
	public final String _alunoLogin = "Aluno efetuou loggin: ";
	public final String _professorLogin = "Professor efetuou loggin: ";
	public final String _alunoLogout = "Aluno efetuou logoff: ";
	public final String _proessorLogout = "Professor efetuou logoff: ";
	public final String _gravaPontos = "Pontuacao gravado com sucesso";
	public final String _appStart = "Aplicação iniciada";
	public final String _appEnd = "Aplicação finalizada";
	public final String _regAdd = "Informação cadastrada por: ";
	public final String _atividadeDeletada = "Atividade apagada";
	public final String _atividadeEditada = "Atividade editada por: ";
	public final String _atividadeLiberada = "Atividade liberada por: ";
	// RETURN MSG - Pessoa n�o Logada, False, True;
	public final String _alunoNaoLogado = "false-Aluno não logado";
	public final String _professorNaoLogado = "false-Professor não logado";
	public final String _masterNaoLogado = "false-Master não logado";
	public final String _AlunoCursoAdded = "Curso já cadastrado a este aluno";
	public final String _false = "false-";
	public final String _true = "true";
	
	// RETURN MSG - N�o Encontrada;
	public final String _atvNotFound = "false-Atividade não Encontrada";
	public final String _turmaNotFound = "false-Turma não Encontrada";
	public final String _disciplinaNotFound = "false-Disciplina não Encontrada";
	public final String _alunoNotFound = "false-Aluno não Encontrado";
	public final String _professorNotFound = "false-Professor no Encontrada";
	
	//Mensagens de metodos;
	public final String _erroMetodo = "Erro ao executar metodo: ";
	//Tipos de logs;
	public final int _tipoCadastro = 1;
	public final int _tipoExcecao = 2;
	public final int _tipoQuery = 3;
	public final int _tipoRemocao = 4;
	public final int _tipoLogin = 5;
	public final int _tipoEdicao = 6;
	//Detalhes;
	public final String _alunoAdd = "Aluno Cadastrado";
	public final String _perguntaAdd = "Pergunta Cadastrada";
	public final String _professorAdd = "Professor Cadastrado";
	public final String _turmaAdd = "Turma Cadastrada";
	public final String _disciplinaAdd = "Disciplina Cadastrada";
	public final String _respostaAdd = "Resposta de Pergunta Cadastrada";
	public final String _palavraAdd = "Palavra Cadastrada";
	public final String _dicaAdd = "Dica Cadastrada";
	public final String _cursoAdd = "Curso Cadastrado";
	public final String _atividadeAdd = "Atividade Cadastrada";
	
	//Querys
	
}
