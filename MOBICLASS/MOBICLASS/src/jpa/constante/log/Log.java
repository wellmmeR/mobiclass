package jpa.constante.log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="LOGGER", schema = "mobiclass")
public class Log {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long ID;
	@Column(name="LOGGER")
	private String logger;
	@Column(name="DATA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date data;
	@Column(name="TIPO")
	private int tipo;
	@Column(name="DETALHE")
	private String detalhe;
	
	public Log(String logger, String detalhe, int tipo){
		Date hj = new Date();
		SimpleDateFormat formatData = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		setData(formatData.format(hj));
		setLogger(logger);
		setTipo(tipo);
		setDetalhe(detalhe);
	}
	
	public String getLogger() {
		return logger;
	}

	public void setLogger(String logger) {
		this.logger = logger;
	}

	public String getDetalhe() {
		return detalhe;
	}

	public void setDetalhe(String detalhe) {
		this.detalhe = detalhe;
	}

	public Date getData() {
		return data;
	}

	public void setData(String data) {
		this.data = converteData(data);
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public long getID() {
		return ID;
	}
	private Date converteData(String data) {  
	    Date date = null;  
	    try {  
	        SimpleDateFormat dtOutput = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
	        date = dtOutput.parse(data);
	    }
	    catch (ParseException e) {  
	        e.printStackTrace();  
	    }  
	    return date;  
	}
	public Log() {
	}
}
